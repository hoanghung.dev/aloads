<?php
namespace Soul;


use Soul\Mvc\Controller;
use Soul\Mvc\Request;
use Soul\Mvc\Response;
use Soul\Mvc\Router;
use Soul\Mvc\View;
use Soul\Pattern\Singleton;


class Application extends Singleton
{
    /**
     * @var View $_view
     */
    protected $_view = null;
    /**
     * @var Request $request
     */
    protected $_request = null;
    /**
     * @var Router $_router ;
     */
    protected $_router = null;
    /**
     * @var Response $_response ;
     */
    protected $_response = null;
    /**
     * @var string $_moduleName
     */
    protected $_moduleName = null;
    /**
     * @var array $_modules
     */
    protected $_modules = array();
    /**
     * @var string $_applicationDir
     */
    protected $_applicationDir = null;

    /**
     * @var string $_applicationName
     */
    protected $_applicationName = 'Application';

    /**
     * @var string Default Action
     */
    protected $_defaultAction = 'index';
    /**
     * @var string Default Controllers
     */
    protected $_defaultController = 'index';

    /**
     * @var string Module
     */
    protected $_defaultModule = null;

    /**
     *
     */

    public function __construct()
    {

    }

    /**
     * @param $name
     * @return mixed
     */
    public function setApplicationName($name)
    {
        return $this->_applicationName = $name;
    }

    /**
     * @return string
     */
    public function getApplicationName()
    {
        return $this->_applicationName;
    }

    /**
     * @param string $dir
     * @return $this
     */
    public function setApplicationDir($dir)
    {
        $this->_applicationDir = $dir;
        return $this;
    }

    /**
     * @return string Application Dir
     */
    public function getApplicationDir()
    {
        return $this->_applicationDir;
    }


    /**
     * @param $module
     * @return $this
     */
    public function setModuleName($module)
    {
        $this->_moduleName = $module;
        return $this;
    }

    /**
     * @param $modules
     * @return $this
     */
    public function setModules($modules)
    {
        $this->_modules = $modules;
        return $this;
    }

    /**
     * @return array
     */
    public function getModules()
    {
        return $this->_modules;
    }

    /**
     * @param Router $router
     * @return $this
     */
    public function setRouter(Router $router)
    {
        $this->_router = $router;
        return $this;
    }

    /**
     * @return Router
     */
    public function getRouter()
    {
        if (null == $this->_router) {
            $this->_router = new Router\Rewrite();

        }
        return $this->_router;
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->_request = $request;
        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        if (null == $this->_request) {
            $this->_request = new Request();
        }
        return $this->_request;
    }

    /**
     * @param Response $response
     * @return $this
     */
    public function setResponse(Response $response)
    {
        $this->_response = $response;
        return $this;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        if (null === $this->_response) {
            $this->_response = new Response();
        }
        return $this->_response;
    }

    /**
     * @param $unFormatted
     * @return string
     */
    public function formatModuleName($unFormatted)
    {
        return ucfirst($unFormatted);
    }


    /**
     * @param $unFormatted
     * @return string
     */
    public function formatControllerName($unFormatted)
    {
        return ucfirst($unFormatted);
    }

    /**
     * @param $unFormatted
     * @return string
     */
    public function formatActionName($unFormatted)
    {
        return $unFormatted;
    }

    /**
     * @param Controller $controller
     * @return $this
     */
    public function setDefaultController($controller)
    {
        $this->_defaultController = $controller;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultController()
    {
        return $this->_defaultController;
    }

    /**
     * @param $action
     * @return $this
     */
    public function setDefaultAction($action)
    {
        $this->_defaultAction = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultAction()
    {
        return $this->_defaultAction;
    }

    /**
     * @param $view
     */
    public function setView($view)
    {
        $this->_view = $view;
    }

    /**
     * @return View
     */
    public function getView()
    {
        if (null === $this->_view) {
            $this->_view = new View($this->getModuleDir() . DIRECTORY_SEPARATOR . 'Views');
        }
        return $this->_view;
    }


    /**
     * @return string
     */
    public function getControllerDir()
    {
        $directory = $this->getModuleDir() . DIRECTORY_SEPARATOR . 'Controllers';
        return $directory;
    }


    /**
     * @param Request $request
     * @return string action name
     */
    public function getActionMethod(Request $request)
    {
        $action = $request->getActionName();
        if (empty($action)) {
            $action = $this->getDefaultAction();
            $request->setActionName($action);
        }
        return $this->formatActionName($action);
    }

    /**
     * @param Request $request
     * @return string
     * @throws Exception
     */
    public function getControllerClass(Request $request)
    {
        $controller = $request->getControllerName();
        if (empty($controller)) {
            $controller = $this->getDefaultController();
            $request->setControllerName($controller);
        }
        $module = $this->getModuleName();
        if (!empty($module)) {
            $module = $module . '\\';
        }
        $finalClass = $this->getApplicationName() . '\\' . $module . 'Controllers\\' . $this->formatControllerName($controller);
        if (!class_exists($finalClass)) {
            throw new Exception("Controllers : $finalClass not found!");
        }
        return $finalClass;
    }

    /**
     * @return string
     */
    public function getModuleName()
    {
        $module = $this->getRequest()->getModuleName();
        $module = $this->formatModuleName($module);
        return $module;
    }

    public function getModuleDir()
    {
        $module = $this->getModuleName();
        if (empty($module)) {
            return $this->getApplicationDir();
        }
        return $this->getApplicationDir() . DIRECTORY_SEPARATOR . $this->getModuleName();
    }

    /**
     * Dispatch to a controller/action
     */
    public function dispatch()
    {
        $request = $this->getRequest();
        $module = $this->getModuleName($request);
        if (!empty($module)) {
            if (!in_array($module, $this->getModules())) {
                throw new Exception('Module  ' . $module . ' not found');
            }
        }
        if (null === $request->getActionName()) {
            $router = $this->getRouter();
            $router->route($request);
        }

        $this->setModuleName($module);
        $controllerClass = $this->getControllerClass($request);
        /* @var Controller $controller */
        $controller = new $controllerClass($this);
        $response = $this->getResponse();
        $controller->setResponse($response);
        $action = $this->getActionMethod($request);
        try {

            $controller->$action();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function run()
    {
        self::getInstance()->dispatch();
    }
}