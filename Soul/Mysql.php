<?php
namespace Soul;

/*
 * Class Factory Mysql
 */

use Soul\Mysql\Driver;

class Mysql
{
    /*
     * @var array $connections store all connections
     */
    private static $connections = array();

    /*
     * @param string $host
     * @param array $params
     * @return object \Soul\Db\Mysql
     */
    public static function factory($host = 'master', array $params)
    {
        if (!isset(self::$connections[$host])) {
            try {
                $con = new Driver("mysql:host={$params['host']};dbname={$params['dbname']};charset=utf8", $params['username'], $params['password'], $params['options']);
                $con->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                $con->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
                $con->query("set names 'utf8'");
            } catch (\PDOException $e) {
                throw new Exception($e->getMessage());
            }
            self::$connections[$host] = $con;
        }
        return self::$connections[$host];
    }

}
