<?php
namespace Soul;

use MongoClient;

class Mongo
{
    /*
     * @var array $connections store all connections
     */
    protected static $connections;

    /**
     * Factory MongoDb
     * @param string $host
     * @param array $params
     * @return MongoClient
     */
    public static function factory($host = 'master', array $params)
    {
        try {
            $connectionString = sprintf('mongodb://%s:%s@%s:%d', $params['username'], $params['password'], $params['host'], $params['port']);
            $mongo = new MongoClient($connectionString, array("db" => $params['dbname']));


          //      exit;
            //$mongo = new MongoClient($connectionString,array("username" => $params['username'], "password" => $params['password']) );
            self::$connections[$host] = $mongo->{$params['dbname']};
            //self::$connections[$host] = $mongo;


        } catch (\MongoException $e) {
            echo $e->getMessage();
        }
        return self::$connections[$host];
    }

}
