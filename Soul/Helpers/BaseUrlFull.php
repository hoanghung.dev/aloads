<?php
namespace Soul\Helpers;

use Soul\Application;

class BaseUrlFull
{
    public function baseUrlFull()
    {
        $pageURL = 'http';
        if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}}
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
        $reg = Application::getInstance()->getRequest();

        return $reg->getBaseUrlFull();
    }
}