<?php
class Soul_Helper_Thumbnail {
    public function thumbnail($file, $width = 100, $height = 100, $pathCache = null) {
        $config = Soul_Registry::get('Graphic');
        if ($pathCache == null) {
            $pathCache = $config['pathCache'];
        }
        $filecache = $pathCache . '/' . $width . 'x' . $height . '_' . basename($file);
        if (file_exists($filecache)) {
            return $filecache;
        }
        if ((strpos(strtolower($file), "http")) !== false) return null;
        if (!file_exists($file) || !is_file($file)) {
            return null;
        }

        if (! class_exists('Soul_Graphic_Thumbnail')) {
            require_once 'Soul/Graphic/Thumbnail.php';
        }

        $thumb = new Soul_Graphic_Thumbnail($file, $pathCache);
        $thumb->resize($width, $height);
        $newfile = $thumb->save();
        return $newfile;


    }
}
