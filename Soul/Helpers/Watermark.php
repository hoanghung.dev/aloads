<?php
class Soul_Helper_Watermark
{
    public function watermark($file, $width = 100, $height = 100, $pathCache = null)
    {
        $config = Soul_Registry::get('Graphic');
        if ($pathCache == null) {
            $pathCache = $config['pathCache'];
        }
        
        $filecache = $pathCache . DIRECTORY_SEPARATOR . 'w_'. $width . 'x' . $height . '_' . basename($file);
        if (file_exists($filecache)) {
            return $filecache;
        }
        if (! class_exists('Soul_Graphic_Watermark')) {
            require_once 'Soul/Graphic/Watermark.php';
        }
        $thumb = new Soul_Graphic_Watermark($file,$pathCache);
        $thumb->waterMark($config['logoWatermark']);      
        $thumb->save();
		$newfile = $pathCache . '/' . 'w_'. $width . 'x' . $height . '_' . basename($file);
        return $newfile;
    }
}
?>