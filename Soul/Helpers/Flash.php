<?php
namespace Soul\Helpers;
class Flash
{
    public function flash($message = null, $status = null)
    {
        if (isset($message) && null === $message) {
            $this->addMessage($message);
            $this->addStatus($status);
        }

        return $this;
    }

    public function getMessages()
    {
        if (isset($_SESSION['_FM'])) {
            $message = $_SESSION['_FM'];
            unset($_SESSION['_FM']);
            return $message;
        }
        return null;
    }

    public function addMessage($message = null)
    {
        if (is_array($message)) {
            $_SESSION['_FM'] = $message;
            return $this;
        }
        $_SESSION['_FM'][] = $message;
        return $this;
    }

    /**
     * @param $message
     * @return $this
     */
    public function error($message)
    {
        $this->addMessage($message);
        $this->addStatus('error');
        return $this;
    }

    /**
     * @param $message
     * @return $this
     */
    public function warning($message)
    {
        $this->addMessage($message);
        $this->addStatus('warning');
        return $this;
    }

    /**
     * @param $message
     * @return $this
     */
    public function info($message)
    {
        $this->addMessage($message);
        $this->addStatus('info');
        return $this;
    }

    /**
     * @param $message
     * @return $this
     */
    public function danger($message)
    {
        $this->addMessage($message);
        $this->addStatus('danger');
        return $this;
    }

    /**
     * @param $message
     * @return $this
     */
    public function success($message)
    {
        $this->addMessage($message);
        $this->addStatus('success');
        return $this;
    }

    /**
     * @param null $status
     * @return $this
     */
    public function addStatus($status = null)
    {
        $_SESSION['_FM_ST'] = $status;
        return $this;
    }

    /**
     * @return null
     */
    public function getStatus()
    {
        if (isset($_SESSION['_FM_ST'])) {
            $status = $_SESSION['_FM_ST'];
            unset($_SESSION['_FM_ST']);
            return $status;
        }
        return null;
    }

    /**
     * display message
     */
    public function message()
    {
        $message = $this->getMessages();
        $style = $this->getStatus();
        $output = '';
        if (count($message) > 0) {
            $count = count($message);
            $inc = 0;
            $output .= "<div class=\"soul-flash alert alert-dismissable alert-$style\">";
            $output .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>";
            $output .= implode('<br />', $message);
            $output .= "</div>";

        }
        return $output;
    }
}