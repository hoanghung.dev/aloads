<?php
namespace Soul\Helpers;

class SanitizeTitle {
    public function sanitizeTitle($title) {
        return sanitize_title_with_dashes($title);
    }
}