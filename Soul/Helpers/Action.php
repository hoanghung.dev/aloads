<?php
namespace Soul\Helpers;

use Soul\Application;

class Action
{
    /**
     * @var Application $application
     */
    public $application;
    /**
     * @var Request $request
     */
    public $request;


    public function __construct()
    {
        $this->application = Application::getInstance();
        $this->request = clone $this->application->getRequest();

    }

    public function resetObjects()
    {
        $params = $this->request->getParams();
        foreach (array_keys($params) as $key) {
            $this->request->setParam($key, null);
        }

    }

    public function action($action = null, $controller = null, $module = null, $params = array())
    {

        $this->resetObjects();
        $this->request->setActionName($action);
        $this->request->setControllerName($controller);
        $this->request->setModuleName($module);
        $this->request->setParams($params);
        $this->application->setRequest($this->request);
        $this->application->dispatch();
    }
}
