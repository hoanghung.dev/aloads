<?php
namespace Soul\Helpers;
class GetPost
{
    public function getPost($name, $defaultValue = '', $array = null)
    {

        if ($array != null) {
            if (isset($_GET[$array][$name])) {
                return htmlspecialchars($_GET[$array][$name]);

            }
            if (isset($_POST[$array][$name])) {

                return htmlspecialchars($_POST[$array][$name]);

            }
            return htmlspecialchars($defaultValue);
        }
        if (isset($_GET[$name])) {
            return htmlspecialchars($_GET[$name]);
        }
        if (isset($_POST[$name])) {
            return htmlspecialchars($_POST[$name]);
        }
        return htmlspecialchars($defaultValue);
    }
}
