<?php
namespace Soul;

use Soul\Pattern\Singleton;

class Helper extends Singleton
{
    protected $_helper = array();
    protected $_module = array();

    /**
     * @param null $helper
     * @return mixed
     */
    public function getHelper($helper = null)
    {
        $name = ucfirst($helper);
        if (!isset($this->_helper[$name])) {
            if (!class_exists($name)) {

               $classHelper = $this->_loadHelper($name);
            }
            $this->_helper[$name] = new $classHelper();

        }
        return $this->_helper[$name];
    }

    /**
     * @param $name
     * @return string
     * @throws Exception
     */
    protected function _loadHelper($name)
    {
        $classHelper = 'Soul\Helpers\\' . $name;
        if (class_exists($classHelper)) {
            return $classHelper;
        } else {

            $module = Application::getInstance()->getModuleName();

            $app = Application::getInstance()->getApplicationName();

            if (!empty($module)) {
                $module = $module . '\\';
            }

            $classHelper =  $app . '\\' . $module . 'Helpers\\' . $name;

            if (class_exists($classHelper)) {
                return $classHelper;
            }
        }
        throw new Exception("Helpers : $classHelper not found!");
    }


    public function __call($name, $args)
    {
        // is the Helpers already loaded?
        $helper = $this->getHelper($name);
        // call the Helpers method
        return call_user_func_array(array($helper, $name), $args);
    }

    public function __get($name)
    {
        return $this->getHelper($name);
    }
}
