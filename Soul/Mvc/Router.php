<?php
namespace Soul\Mvc;


abstract class Router
{

    /**
     * Array of invocation parameters to use when instantiating action
     * Controllers
     * @var array
     */
    protected $_invokeParams = array();

    /**
     * @param array $params
     */
    public function __construct(array $params = array())
    {
        $this->setParams($params);
    }

    /**
     * Add or modify a parameter to use when instantiating an action Controllers
     *
     * @param string $name
     * @param mixed $value
     * @return Router
     */
    public function setParam($name, $value)
    {
        $name = (string)$name;
        $this->_invokeParams[$name] = $value;
        return $this;
    }

    /**
     * Set parameters to pass to action Controllers constructors
     *
     * @param array $params
     * @return Router
     */
    public function setParams(array $params)
    {
        $this->_invokeParams = array_merge($this->_invokeParams, $params);
        return $this;
    }

    abstract public function route(Request $request);

    /**
     * Retrieve a single parameter from the Controllers parameter stack
     *
     * @param string $name
     * @return mixed
     */
    public function getParam($name)
    {
        if (isset($this->_invokeParams[$name])) {
            return $this->_invokeParams[$name];
        }

        return null;
    }

    /**
     * Retrieve action Controllers instantiation parameters
     *
     * @return array
     */
    public function getParams()
    {
        return $this->_invokeParams;
    }

    /**
     * @param null $name
     * @return $this
     */
    public function clearParams($name = null)
    {
        if (null === $name) {
            $this->_invokeParams = array();
        } elseif (is_string($name) && isset($this->_invokeParams[$name])) {
            unset($this->_invokeParams[$name]);
        } elseif (is_array($name)) {
            foreach ($name as $key) {
                if (is_string($key) && isset($this->_invokeParams[$key])) {
                    unset($this->_invokeParams[$key]);
                }
            }
        }

        return $this;
    }


}
