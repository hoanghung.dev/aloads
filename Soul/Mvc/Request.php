<?php

namespace Soul\Mvc;

class Request
{
    /**
     * @var sting module name
     */
    protected $_moduleName = null;
    /*
     * @var string Controllers name
     */
    protected $_controllerName = null;
    /**
     * @var string action name
     */
    protected $_actionName = null;
    /**
     * @var array
     */
    protected $_params = array();
    /**
     * @var string
     */
    protected $_requestUri;
    /**
     * Base URL of request
     * @var string
     */
    protected $_baseUrl = null;
    /**
     * @var null
     */
    protected $_basePath = null;
    /**
     * @var string
     */
    protected $_pathInfo = '';

    /**
     * set name module
     * @param $module string
     */
    public function setModuleName($module)
    {
        $this->_moduleName = $module;
    }

    /**
     * get module name
     * @return string
     */
    public function getModuleName()
    {
        if (null === $this->_moduleName) {
            return $this->getParam('module');
        }
        return $this->_moduleName;
    }

    /**
     * set name Controllers
     * @param $controller string
     */
    public function setControllerName($controller)
    {
        $this->_controllerName = $controller;
    }

    /**
     * get Controllers name
     * @return string
     */
    public function getControllerName()
    {
        if (null === $this->_controllerName) {
            return $this->getParam('controller');
        }
        return $this->_controllerName;
    }

    /**
     * @param $action
     */
    public function setActionName($action)
    {
        $this->_actionName = $action;
    }

    /**
     * @return string action
     */
    public function getActionName()
    {
        if (null == $this->_actionName) {
            return $this->getParam('action');
        }
        return $this->_actionName;
    }

    /**
     * @return array
     */

    public function getParams()
    {
        $return = $this->_params;
        if (isset($_GET) && is_array($_GET)) {
            $return += $_GET;
        }
        if (isset($_POST) && is_array($_POST)) {
            $return += $_POST;
        }
        return $return;
    }
    public function getPosts()
    {
        $return = array();
        if (isset($_POST) && is_array($_POST)) {
            $return += $_POST;
        }
        return $return;
    }
    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setParam($key, $value)
    {
        $key = (string)$key;
        if ((null === $value) && isset($this->_params[$key])) {
            unset($this->_params[$key]);
        } elseif (null !== $value) {
            $this->_params[$key] = $value;
        }
        return $this;
    }

    /**
     * @param $array
     */
    public function setParams($array)
    {
        $this->_params = $this->_params + (array)$array;
        foreach ($this->_params as $key => $value) {
            if (null === $value) {
                unset($this->_params[$key]);
            }
        }
    }

    /**
     * @param $key
     * @param null $default
     * @return null
     */
    public function getParam($key, $default = null)
    {
        //echo $_GET[$key];
        if (isset($this->_params[$key])) {
            return $this->_params[$key];
        } elseif ((isset($_GET[$key]))) {
            return $_GET[$key];
        } elseif ((isset($_POST[$key]))) {
            return $_POST[$key];
        }
        return $default;
    }
    public function getParamInt($key, $default = null)
    {
        if (isset($this->_params[$key])) {
            return intval($this->_params[$key]);
        } elseif ((isset($_GET[$key]))) {
            return intval($_GET[$key]);
        } elseif ((isset($_POST[$key]))) {
            return intval($_POST[$key]);
        }
        return $default;
    }

    public function getParamStr($key, $default = null)
    {

        $match = '/[^a-z0-9][^à|^á|^ạ|^ả|^ã|^â|^ầ|^ấ|^ậ|^ẩ|^ẫ|^ă|^ằ|^ắ|^ặ|^ẳ|^ẵ][^è|^é|^ẹ|^ẻ|^ẽ|^ê|^ề|^ế|^ệ|^ể|^ễ][^ì|^í|^ị|^ỉ|^ĩ][^ò|^ó|^ọ|^ỏ|^õ|^ô|^ồ|^ố|^ộ|^ổ|^ỗ|^ơ|^ờ|^ớ|^ợ|^ở|^ỡ][^ù|^ú|^ụ|^ủ|^ũ|^ư|^ừ|^ứ|^ự|^ử|^ữ][^ỳ|^ý|^ỵ|^ỷ|^ỹ][^đ][^À|^Á|^Ạ|^Ả|^Ã|^Â|^Ầ|^Ấ|^Ậ|^Ẩ|^Ẫ|^Ă|^Ằ|^Ắ|^Ặ|^Ẳ|^Ẵ][^È|^É|^Ẹ|^Ẻ|^Ẽ|^Ê|^Ề|^Ế|^Ệ|^Ể|^Ễ][^Ì|^Í|^Ị|^Ỉ|^Ĩ][^Ò|^Ó|^Ọ|^Ỏ|^Õ|^Ô|^Ồ|^Ố|^Ộ|^Ổ|^Ỗ|^Ơ|^Ờ|^Ớ|^Ợ|^Ở|^Ỡ][^Ù|^Ú|^Ụ|^Ủ|^Ũ|^Ư|^Ừ|^Ứ|^Ự|^Ử|^Ữ][^Ỳ|^Ý|^Ỵ|^Ỷ|^Ỹ][^Đ]/i';
        if (isset($this->_params[$key])) {
            return preg_match($match,'',$this->_params[$key]);
        } elseif ((isset($_GET[$key]))) {
            return preg_match($match,'',urldecode($_GET[$key]));
        } elseif ((isset($_POST[$key]))) {
            return preg_match($match,'',$_POST[$key]);
        }
        return $default;
    }
    /**
     * @param $key
     * @param null $default
     * @return null
     */
    public function getPost($key, $default = null)
    {
        if (null === $key) {
            return $_POST;
        }
        return (isset($_POST[$key])) ? $_POST[$key] : $default;
    }

    /**
     * @param $key
     * @param null $default
     * @return null
     */
    public function getQuery($key, $default = null)
    {
        if (null === $key) {
            return $_GET;
        }
        return (isset($_GET[$key])) ? $_GET[$key] : $default;
    }

    /**
     * @param $key
     * @param null $default
     * @return null
     */
    public function getServer($key, $default = null)
    {
        if (null === $key) {
            return $_SERVER;
        }
        return (isset($_SERVER[$key])) ? $_SERVER[$key] : $default;
    }

    /**
     * Only Apache
     */
    public function getUri()
    {
        return $this->getServer('REQUEST_URI');
    }

    /**
     * Return the method by which the request was made
     * @return string
     */
    public function getMethod()
    {
        return $this->getServer('REQUEST_METHOD');
    }

    /**
     * Kiểm tra có phải là POST hay ko ?
     * @return boolean
     */
    public function isPost()
    {
        if ('POST' === $this->getMethod()) {
            return true;
        }
        return false;
    }


    /**
     * Was the request made by PUT?
     *
     * @return boolean
     */
    public function isPut()
    {
        if ('PUT' == $this->getMethod()) {
            return true;
        }

        return false;
    }

    /**
     * Was the request made by DELETE?
     *
     * @return boolean
     */
    public function isDelete()
    {
        if ('DELETE' == $this->getMethod()) {
            return true;
        }

        return false;
    }

    /**
     * Was the request made by HEAD?
     *
     * @return boolean
     */
    public function isHead()
    {
        if ('HEAD' == $this->getMethod()) {
            return true;
        }

        return false;
    }

    /**
     * Was the request made by OPTIONS?
     *
     * @return boolean
     */
    public function isOptions()
    {
        if ('OPTIONS' == $this->getMethod()) {
            return true;
        }

        return false;
    }

    /**
     * Is the request a Javascript XMLHttpRequest?
     *
     * Should work with Prototype/Script.aculo.us, possibly others.
     *
     * @return boolean
     */
    public function isXmlHttpRequest()
    {
        return ($this->getHeader('X_REQUESTED_WITH') == 'XMLHttpRequest');
    }

    /**
     * Is this a Flash request?
     *
     * @return boolean
     */
    public function isFlashRequest()
    {
        $header = strtolower($this->getHeader('USER_AGENT'));
        return (strstr($header, ' flash')) ? true : false;
    }


    public function getBaseUrl()
    {
        if (null === $this->_baseUrl) {
            $this->setBaseUrl();
        }
        return $this->_baseUrl;
    }


    public function setBaseUrl($baseUrl = null)
    {
        if ((null !== $baseUrl) && !is_string($baseUrl)) {
            return $this;
        }
        if ($baseUrl === null) {
            $filename = basename($_SERVER['SCRIPT_FILENAME']);
            if (basename($_SERVER['SCRIPT_NAME']) === $filename) {
                $baseUrl = $_SERVER['SCRIPT_NAME'];
            } elseif (basename($_SERVER['PHP_SELF']) === $filename) {
                $baseUrl = $_SERVER['PHP_SELF'];
            } elseif (isset($_SERVER['ORIG_SCRIPT_NAME']) && basename($_SERVER['ORIG_SCRIPT_NAME']) === $filename) {
                $baseUrl = $_SERVER['ORIG_SCRIPT_NAME']; // 1and1 shared hosting compatibility
            } else {
                // Backtrack up the script_filename to find the portion matching
                // php_self
                $path = $_SERVER['PHP_SELF'];
                $segs = explode('/', trim($_SERVER['SCRIPT_FILENAME'], '/'));
                $segs = array_reverse($segs);
                $index = 0;
                $last = count($segs);
                $baseUrl = '';
                do {
                    $seg = $segs[$index];
                    $baseUrl = '/' . $seg . $baseUrl;
                    ++$index;
                } while (($last > $index) && (false !== ($pos = strpos($path, $baseUrl))) && (0 != $pos));
            }
            // Does the baseUrl have anything in common with the request_uri?
            $requestUri = $this->getRequestUri();
            if (0 === strpos($requestUri, $baseUrl)) {
                // full $baseUrl matches
                $this->_baseUrl = $baseUrl;
                return $this;
            }
            if (0 === strpos($requestUri, dirname($baseUrl))) {
                // directory portion of $baseUrl matches
                $this->_baseUrl = rtrim(dirname($baseUrl), '/');
                return $this;
            }
            if (!strpos($requestUri, basename($baseUrl))) {
                // no match whatsoever; set it blank
                $this->_baseUrl = '';
                return $this;
            }
            // If using mod_rewrite or ISAPI_Rewrite strip the script filename
            // out of baseUrl. $pos !== 0 makes sure it is not matching a value
            // from PATH_INFO or QUERY_STRING
            if ((strlen($requestUri) >= strlen($baseUrl)) && ((false !== ($pos = strpos($requestUri, $baseUrl))) && ($pos !== 0))) {
                $baseUrl = substr($requestUri, 0, $pos + strlen($baseUrl));
            }
        }
        $this->_baseUrl = rtrim($baseUrl, '/');
        return $this;
    }


    public function setBasePath($basePath = null)
    {
        if ($basePath === null) {
            $filename = (isset($_SERVER['SCRIPT_FILENAME']))
                ? basename($_SERVER['SCRIPT_FILENAME'])
                : '';

            $baseUrl = $this->getBaseUrl();
            if (empty($baseUrl)) {
                $this->_basePath = '';
                return $this;
            }

            if (basename($baseUrl) === $filename) {
                $basePath = dirname($baseUrl);
            } else {
                $basePath = $baseUrl;
            }
        }

        if (substr(PHP_OS, 0, 3) === 'WIN') {
            $basePath = str_replace('\\', '/', $basePath);
        }

        $this->_basePath = rtrim($basePath, '/');
        return $this;
    }

    public function getBasePath()
    {
        if (null === $this->_basePath) {
            $this->setBasePath();
        }

        return $this->_basePath;
    }

    public function setPathInfo($pathInfo = null)
    {
        if ($pathInfo === null) {
            $baseUrl = $this->getBaseUrl();
            if (null === ($requestUri = $this->getRequestUri())) {
                return $this;
            }
            // Remove the query string from REQUEST_URI
            if ($pos = strpos($requestUri, '?')) {
                $requestUri = substr($requestUri, 0, $pos);
            }
            if ((null !== $baseUrl) && (false === ($pathInfo = substr($requestUri, strlen($baseUrl))))) {
                // If substr() returns false then PATH_INFO is set to an empty string
                $pathInfo = '';
            } elseif (null === $baseUrl) {
                $pathInfo = $requestUri;
            }
        }
        $this->_pathInfo = (string)$pathInfo;
        return $this;
    }

    /**
     * @return string
     */
    public function getPathInfo()
    {
        if (empty($this->_pathInfo)) {
            $this->setPathInfo();
        }

        return $this->_pathInfo;
    }

    /**
     * @param null $requestUri
     * @return $this
     */
    public function setRequestUri($requestUri = null) {
        if ($requestUri === null) {
            if (isset($_SERVER['HTTP_X_REWRITE_URL'])) { // check this first so IIS will catch
                $requestUri = $_SERVER['HTTP_X_REWRITE_URL'];
            } elseif (isset($_SERVER['REQUEST_URI'])) {
                $requestUri = $_SERVER['REQUEST_URI'];
            } elseif (isset($_SERVER['ORIG_PATH_INFO'])) { // IIS 5.0, PHP as CGI
                $requestUri = $_SERVER['ORIG_PATH_INFO'];
                if (! empty($_SERVER['QUERY_STRING'])) {
                    $requestUri .= '?' . $_SERVER['QUERY_STRING'];
                }
            } else {
                return $this;
            }
        } elseif (! is_string($requestUri)) {
            return $this;
        } else {
            // Set GET items, if available
            $_GET = array();
            $vars = null;
            if (false !== ($pos = strpos($requestUri, '?'))) {
                // Get key => value pairs and set $_GET
                $query = substr($requestUri, $pos + 1);
                parse_str($query, $vars);
                $_GET = $vars;
            }
        }
        $this->_requestUri = $requestUri;
        return $this;
    }

    public function getRequestUri()
    {
        if (empty($this->_requestUri)) {
            $this->setRequestUri();
        }
        return $this->_requestUri;
    }

    /**
     * Get the client's IP addres
     *
     * @param  boolean $checkProxy
     * @return string
     */
    public function getClientIp($checkProxy = true)
    {
        if ($checkProxy && $this->getServer('HTTP_CLIENT_IP') != null) {
            $ip = $this->getServer('HTTP_CLIENT_IP');
        } else if ($checkProxy && $this->getServer('HTTP_X_FORWARDED_FOR') != null) {
            $ip = $this->getServer('HTTP_X_FORWARDED_FOR');
        } else {
            $ip = $this->getServer('REMOTE_ADDR');
        }

        return $ip;
    }
}
