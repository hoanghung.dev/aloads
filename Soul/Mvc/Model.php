<?php
namespace Soul\Mvc;

use Soul\Registry;

abstract class Model
{
    /**
     * @var \Soul\Mysql\Driver $_mysql
     */
    protected $_mysql = null;

    protected $_mysql_sub = null;

    protected $_mysql_gw = null;

    protected $_mysql_log = null;

    protected $_mysql_report = null;

    /**
     * @var MongoClient $_mongodb
     */
    protected $_mongodb = null;
    /**
     * @var Redis $_redis
     */
    protected $_redis = null;
    /**
     * @var Memcache $_memcache
     */
    protected $_memcache = null;

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {

    }

    /**
     * @param $args
     * @param string $defaults
     * @return array
     */
    public function parseArgs($args, $defaults = '')
    {
        if (is_object($args))
            $r = get_object_vars($args);
        elseif (is_array($args))
            $r = & $args;
        else
            self::parseStr($args, $r);

        if (is_array($defaults))
            return array_merge($defaults, $r);
        return $r;
    }

    /**
     *
     * @param string $string The string to be parsed.
     * @param array $array Variables will be stored in this array.
     */
    public function parseStr($string, &$array)
    {
        parse_str($string, $array);
        if (get_magic_quotes_gpc())
            $array = stripslashes_deep($array);
    }

    public function bind($parameter, $value, $var_type = null){
        if (is_null($var_type)) {
            switch (true) {
                case is_bool($value):
                    $var_type = \PDO::PARAM_BOOL;
                    break;
                case is_int($value):
                    $var_type = \PDO::PARAM_INT;
                    break;
                case is_null($value):
                    $var_type = \PDO::PARAM_NULL;
                    break;
                default:
                    $var_type = \PDO::PARAM_STR;
            }
        }
        $this->bindValue($parameter, $value, $var_type);
    }
}