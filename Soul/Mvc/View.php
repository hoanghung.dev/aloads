<?php

namespace Soul\Mvc;

use Soul\Exception;
use Soul\Helper;

class View
{
    /**
     * @var Helper $_helper
     */
    protected $_helper;
    /**
     * @var string $_viewDir
     */
    protected $_viewDir = null;
    /**
     * @var Translate $_translate
     */
    protected $_translate = null;
    /**
     * @var \Soul\Session $_session ;
     */
    protected $_session = null;

    /**
     * @param null $viewDir
     */
    public function __construct($viewDir = null)
    {
        if (null != $viewDir) {
            $this->setViewDir($viewDir);
        }
    }

    /**
     * @param $dir
     * @return $this
     * @return $this
     */
    public function setViewDir($dir)
    {
        $this->_viewDir = $dir;
        return $this;
    }

    public function getViewDir()
    {
        if (null === $this->_viewDir) {
            echo $this->_viewDir = Application::getInstance()->getModuleDir() . DIRECTORY_SEPARATOR . 'Views';
        }
        return $this->_viewDir;
    }

    /**
     * @param string $key The variable name.
     * @param string $value The variable value.
     * @return void.
     */
    public function __set($key, $value)
    {
        if ($key[0] != '_') {
            $this->$key = $value;
        }
    }

    /**
     * @param Mix $var
     * @param Mix $val
     * @return bool
     */
    public function assign($var, $val = null)
    {
        if (is_array($var)) {
            foreach ($var as $key => $val) {
                $this->$key = $val;
            }
            return true;
        }
        if (is_object($var) && $var instanceof View) {
            foreach (get_object_vars($var) as $key => $val) {
                if ($key[0] != '_') {
                    $this->$key = $val;
                }
            }
            return true;
        }
        if (is_object($var)) {
            foreach ($var as $key => $val) {
                $this->$key = $val;
            }
            return true;
        }
        if (is_string($var)) {
            $this->$var = $val;
            return true;
        }
        return false;
    }


    /**
     * @param $name
     * @param $args
     * @return mixed
     */
    public function __call($name, $args)
    {
        $helper = Helper::getInstance();
        // call the Helpers method
        return call_user_func_array(array($helper, $name), $args);
    }

    /**
     * Render Views
     * @return string
     */
    public function render()
    {
        $file = $this->_getScript(func_get_arg(0));
        ob_start();
        require $file;
        return ob_get_clean();
    }

    /*
     * @param string $name name of view
     * @return string file name
     */
    public function _getScript($name)
    {
        $name = strtolower($name);
        $file = $this->getViewDir() . DIRECTORY_SEPARATOR . $name;
        if (!is_readable($file)) {
            throw new Exception('Views script \'' . $file . '\' not found');
        }
        return $file;
    }

    /**
     * Build-in helper for escapeing output.
     * @param scalar $input The value for escape.
     * @return string The escape value.
     */
    public function escape($input)
    {
        return htmlspecialchars($input);
    }

    /**
     * @param Session $session
     * @return $this
     */
    public function setSession($session)
    {
        $this->_session = $session;
        return $this;
    }

    /**
     * @param Translate $translate
     * @return $this
     */
    public function setTranslate($translate)
    {
        $this->_translate = $translate;
        return $this;
    }

    /**
     * @param $index
     * @param null $placeholders
     * @param null $domain
     */
    public function _e($index, $placeholders = null, $domain = null)
    {
        echo $this->_translate->_($index, $placeholders, $domain);
    }

    /**
     * @param $index
     * @param null $placeholders
     * @param null $domain
     * @return mixed
     */
    public function _t($index, $placeholders = null, $domain = null)
    {
        return $this->_translate->_($index, $placeholders, $domain);
    }
}