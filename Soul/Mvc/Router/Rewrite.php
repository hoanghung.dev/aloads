<?php
namespace Soul\Mvc\Router;


use Soul\Mvc\Request;
use Soul\Mvc\Router;


class Rewrite extends Router
{
    /**
     * @var string
     */
    protected $_urlDelimiter = '/';

    /**
     * @param Request $request
     * @return Request
     */
    public function route(Request $request)
    {

        $path = $request->getPathInfo();
        $this->assemble($path);
        $this->_setRequestParams($request, $this->getParams());
        return $request;
    }

    /**
     * http://localhost.com/index/
     * http://localhost.com/Admin/index/
     * http://localhost.com/Admin/main/Views/id/5
     *
     * @param string $path
     */
    public function assemble($path)
    {
        $path = trim($path, $this->_urlDelimiter);
        if ($path != '') {
            $parts = explode($this->_urlDelimiter, $path);
            //  print_r($parts);
            $numPart = count($parts);
            if ($numPart > 0) {
                $i = 0;
                if ($parts[0] != 'index' && file_exists($parts[0] . '.php')) {
                    $i = 3;
                    if (isset($parts[1])) {
                        $this->setParam('Controllers', $parts[1]);
                    }
                    if (isset($parts[2])) {
                        $this->setParam('action', $parts[2]);
                    }

                } else {
                    $i = 2;
                    if ($parts[0] != 'index') {
                        $this->setParam('Controllers', $parts[0]);
                    }
                    if (isset($parts[1])) {
                        $this->setParam('action', $parts[1]);
                    }

                }

                for ($i; $i < $numPart; $i++) {
                    $next = $i + 1;
                    if (isset($parts[$next])) {
                        $this->setParam($parts[$i], $parts[$next]);
                    }
                    $i++;
                }
            } else {
                $this->setParam('Controllers', 'index');
                $this->setParam('action', 'index');
            }
        }

    }

    /**
     * @param Request $request
     * @param array $params
     */
    protected function _setRequestParams(Request $request,array $params)
    {

        foreach ($params as $param => $value) {
            $request->setParam($param, $value);
            if ($param === 'module') {
                $request->setModuleName($value);
            }
            if ($param === 'Controllers') {
                $request->setControllerName($value);
            }
            if ($param === 'action') {
                $request->setActionName($value);
            }
        }
    }
}
