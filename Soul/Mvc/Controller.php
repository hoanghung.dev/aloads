<?php
namespace Soul\Mvc;

use Soul\Application;
use Soul\Helper;

abstract class Controller
{
    /**
     * @var Request $_request
     */
    protected $_request;
    /**
     * @var Helper $_helper
     */
    protected $_helper;
    /**
     * @var Response $_response ;
     */
    protected $_response;
    /**
     * @var View $_view ;
     */
    public $view;
    /**
     * @var Translate $_translate ;
     */
    protected $_translate;
    /**
     * @var \Soul\Session $_session ;
     */
    protected $_session;

    /**
     * @var bool $_noResponse ;
     */
    protected $_noResponse = false;

    public function __construct(Application $app = null)
    {

        $this->_helper = Helper::getInstance();
        if ($app == null) {
            $app = Application::getInstance();
        }
        $this->_request = $app->getRequest();
        $this->init();
    }

    public function init()
    {

    }

    /**
     * @return View
     */
    public function initView()
    {
        if (null === $this->view) {
            $this->view = Application::getInstance()->getView();
        }
        return $this->view;
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->_request = $request;
        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        if (null == $this->_request) {
            return $this->_request = Application::getInstance()->getRequest();
        }
        return $this->_request;
    }

    /**
     * @param $param
     * @param null $default
     * @return null
     */
    public function param($param, $default = null)
    {
        return $this->getRequest()->getParam($param, $default);
    }

    /**
     * @return mixed
     */
    public function params()
    {
        return $this->getRequest()->getParams();
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        $request = $this->getRequest();
        if ((null !== $request) && method_exists($request, 'getBaseUrl')) {
            return $request->getBaseUrl();
        }

        return $this->_baseUrl;
    }

    /**
     * @param $response
     * @return $this
     */
    public function setResponse($response)
    {
        if (is_string($response)) {
            $response = new $response();
        }
        $this->_response = $response;

        return $this;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->_response;
    }

    public function noResponse($status = null)
    {
        if (null !== $status) {
            $this->_noResponse = $status;
        }
        return $this->_noResponse;
    }

    /**
     * @param null $action
     * @return mixed
     */
    public function render($action = null)
    {
        $script = $this->getViewScript($action);
        return $this->view->render($script);
    }

    /**
     *
     */
    public function display($content = null)
    {

        $this->getResponse()->body($content)->send();

    }

    /**
     * @param null $action
     * @return string
     */
    public function getViewScript($action = null)
    {
        $request = $this->getRequest();
        if (null === $action) {
            $action = $request->getActionName();
        }
        $controller = $request->getControllerName();
        $script = $controller . DIRECTORY_SEPARATOR . $action . '.php';
        return $script;
    }

    /**
     * @param $index
     * @param null $placeholders
     * @param null $domain
     */
    public function _e($index, $placeholders = null, $domain = null)
    {
        echo $this->_translate->_($index, $placeholders, $domain);
    }

    /**
     * @param $index
     * @param null $placeholders
     * @param null $domain
     * @return mixed
     */
    public function _t($index, $placeholders = null, $domain = null)
    {
        return $this->_translate->_($index, $placeholders, $domain);
    }
}