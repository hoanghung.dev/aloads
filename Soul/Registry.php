<?php

namespace Soul;

class Registry
{
    /**
     * @var array
     */
    private static $_vars = array();

    /**
     * @param $key
     * @param $value
     */
    public function __set($key, $value)
    {
        self::$_vars[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        return self::$_vars[$key];
    }

    /**
     * @param $key
     * @param $value
     */
    public static function set($key, $value)
    {
        self::$_vars[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        return self::$_vars[$key];
    }

    /**
     * @param $key
     * @return bool
     */
    public static function exists($key)
    {
        return isset(self::$_vars[$key]);
    }

    /**
     * @param $key
     */
    public static function remove($key)
    {
        unset(self::$_vars[$key]);
    }
}
