<?php

namespace Soul;


class Validation
{


    /**
     * Checks whether string input is valid date format. When a format is passed
     * it will make sure the date will be in that specific format if validated
     *
     * @param   string
     * @param   string  The format used at the time of a validation
     * @param   bool    Whether validation checks strict
     * @return  bool
     */
    static public function date($val, $format = null, $strict = true)
    {
        if (self::_empty($val)) {
            return true;
        }

        if ($format) {
            $parsed = date_parse_from_format($format, $val);
        } else {
            $parsed = date_parse($val);
        }


        if ($format) {
            return date($format, mktime($parsed['hour'], $parsed['minute'], $parsed['second'], $parsed['month'], $parsed['day'], $parsed['year']));
        } else {
            return true;
        }

    }

    static public function required($val)
    {
        return !self::_empty($val);
    }

    /**
     * Validate email using PHP's filter_var()
     *
     * @param   string
     * @return  bool
     */
    static public function validEmail($val)
    {
        return self::_empty($val) || filter_var($val, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param $val
     * @param string $separator
     * @return bool
     */
    static public function validEmails($val, $separator = ',')
    {
        if (self::_empty($val)) {
            return true;
        }

        $emails = explode($separator, $val);

        foreach ($emails as $e) {
            if (!filter_var(trim($e), FILTER_VALIDATE_EMAIL)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validate URL using PHP's filter_var()
     *
     * @param   string
     * @return  bool
     */
    static public function validUrl($val)
    {
        return self::_empty($val) || filter_var($val, FILTER_VALIDATE_URL);
    }

    /**
     * Validate IP using PHP's filter_var()
     *
     * @param   string
     * @return  bool
     */
    static public function validIp($val)
    {
        return self::_empty($val) || filter_var($val, FILTER_VALIDATE_IP);
    }

    /**
     * Checks whether numeric input has a minimum value
     *
     * @param   string|float|int
     * @param   float|int
     * @return  bool
     */
    static public function numericMin($val, $min_val)
    {
        return self::_empty($val) || floatval($val) >= floatval($min_val);
    }

    /**
     * Checks whether numeric input is between a minimum and a maximum value
     *
     * @param   string|float|int
     * @param   float|int
     * @param   float|int
     * @return  bool
     */
    static public function numericBetween($val, $min_val, $max_val)
    {
        return self::_empty($val) or (floatval($val) >= floatval($min_val) and floatval($val) <= floatval($max_val));
    }

    /**
     * Special empty method because 0 and '0' are non-empty values
     *
     * @param   mixed
     * @return  bool
     */
    static public function _empty($val)
    {
        return ($val === false or $val === null or $val === '' or $val === array());
    }
}