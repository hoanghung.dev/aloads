<?php
namespace Soul;
class Memcache
{
    /**
     * @var array
     */
    private static $connections;

    /*
     * @param string $host
     * @param array $params
     * @return object \Memcache
     */
    public static function getConnection($host = 'master', array $params)
    {

        if (!isset(self::$connections[$host])) {
            try {
                $mc = new \Memcache();
                $mc->connect($params['host'], $params['port']);
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
            self::$connections[$host] = $mc;
        }
        return self::$connections[$host];
    }

}
