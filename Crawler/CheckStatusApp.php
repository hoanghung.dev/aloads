<?php

/**
 * Created by PhpStorm.
 * User: STEVEN
 * Date: 18/08/2016
 * Time: 12:29 SA
 */
namespace Crawl;

class CheckStatusApp
{
    function checkUrl($url)
    {
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);

        /* Get the HTML or whatever is linked in $url. */
        $response = curl_exec($handle);

        /* Check for 404 (file not found). */
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        if ($httpCode == 404 || $httpCode == 403 || $httpCode == 400) {
            return 0;
        } else {
            return 1;
        }
    }

    public function __construct()
    {

        $appModel = new \Application\Admin\Models\Apps();
        $apps = $appModel->getDataArr(['limit'=>999999999999999999]);
        $appOn = [];
        $appOff = [];

        foreach ($apps as $item) {
            if($this->checkUrl('https://play.google.com/store/apps/details?id=' . $item->packageName) == 0 && $item->appStatus == 1) {
                echo $item->packageName . " đã off";
                echo "\n";
                if($appModel->update(['appStatus'=>0], 'appId = :appId', [':appId' => $item->appId]) == true) {
                    echo $item->packageName . " đã cập nhật trạng thái";
                    echo "\n";
                }
                $appOff['http://admin.aloads.net/app-edit/' . $item->appId] = $item->packageName;
            } elseif($this->checkUrl('https://play.google.com/store/apps/details?id=' . $item->packageName) == 1 && $item->appStatus == 0) {
                echo $item->packageName . " đã on";
                echo "\n";
                if($appModel->update(['appStatus'=>1], 'appId = :appId', [':appId' => $item->appId]) == true) {
                    echo $item->packageName . " đã cập nhật trạng thái";
                    echo "\n";
                }
                $appOn['http://admin.aloads.net/app-edit/' . $item->appId] = $item->packageName;
            } 
        }

        $data = [
            'title'     => 'Aloads thông báo trạng thái app thay đổi',
            'data'      => [
                'on'    => $appOn,
                'off'   => $appOff,
                'date'  => date('d-m-Y H:m:s')
            ]
        ];

        $html = '<div style="width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 11px;">
    <h1>'.$data['title'].'</h1>
    <div align="center">
        <table style="width: 100%;font-size: 16px;">
            <thead>
            <tr>
                <td>Trạng thái</td>
                <td>Tên package</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Live</td>
                <td>';
        $data = $data['data'];
                    foreach ($data['on'] as $key => $item):
                        $html .= '<a href="' .$key .'" target="_blank">' . $item . '</a> | ';
                    endforeach;
            $html .='</td>
            </tr>
            <tr>
                <td>Die</td>
                <td>';
                    foreach ($data['off'] as $key => $item):
                        $html .= '<a href="' .$key . '" target="_blank">' . $item . '</a> | ';
                    endforeach;
            $html .= '</td>
            </tr>
            </tbody>
        </table>
    </div>
    <p style="font-size: 15px;">Xem chi tiết tại: <a href="http://admin.aloads.net/app" target="_blank">http://admin.aloads.net/app</a></p>
    <p style="font-size: 15px;">Mọi thay đổi đã được cập nhật vào database!</p>
    <p style="font-size: 15px;">Quét lúc: ' . $data['date'] . '</p>
</div>';
        if(!empty($appOn) && !empty($appOff))
            $this->sendMai($html);
        else
            print "\nKhông có gì thay đổi\n";
    }
    
    public function sendMai($data) {
        require '../Application/Library/PHPMailer/PHPMailerAutoload.php';

        $mail = new \PHPMailer();

        $mail->isSMTP();

        $mail->SMTPDebug = 2;

        $mail->CharSet = 'UTF-8';

        $mail->Debugoutput = 'html';

        $mail->Host = "ssl://smtp.gmail.com";

        $mail->Port = 465;

        $mail->SMTPAuth = true;

        $mail->Username = "alert@netvietgroups.com";

        $mail->Password = "Hbg!990@ooikjuYP;L";

        $mail->setFrom('alert@netvietgroups.com', '[aloads thông báo trạng thái app thay đổi]');

        $mail->addReplyTo('alert@netvietgroups.com', 'Aloads');

        $mail->addAddress('info@netvietgroups.com', 'alert@netvietgroups.com');

        $mail->Subject = 'Aloads thông báo trạng thái app thay đổi';
        
        $mail->msgHTML($data, dirname(__FILE__));

        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }

    }
}