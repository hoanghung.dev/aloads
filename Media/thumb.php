<?php
#error_reporting(E_ALL);
#ini_set('display_errors', 1);

include_once './Thumbnailer.php';
$file = $_GET['f'];

$path = dirname($file);
$name = basename($file);
$name = urlencode($name);
if (IsSafe($name) != true) {
    header('location: /no-image.png');
    exit;
}

if (is_file($file)) {
    header('location: ' . $file);
    exit;
}

$width = 1;
$height = 1;

//$out = sscanf($name, '-%dx%d.jpg', $width, $height);
//var_dump($width);
preg_match_all('#-(\d+)x(\d+).#i', $name, $math);

$width = isset($math[1][0]) ? $math[1][0] : 1;
$height = isset($math[2][0]) ? $math[2][0] : 1;


$origin = str_replace('-' . $width . 'x' . $height, '', $name);

$fileOrigin = $path . '/' . $origin;


if (!is_file($fileOrigin)) {
    header('location: /no-image.png');
    exit;
}

$root = '';
$cacheRelativePath = 'cache/'.$path;
$data['img'] = $fileOrigin;
$data['h'] = $height;
$data['w'] = $width;
$data['resize'] =false;
$data['quality'] = 90;
$data['fill'] =true;
$data['cachetime'] = 7;
if (strlen($path) > 50) exit;

/*if (!is_dir($cacheRelativePath)) {
    if (!mkdir($cacheRelativePath, 0777, true)) {
        die('Failed to create folders...');
    }
}*/

if (!is_dir($cacheRelativePath)) {
    mkdir($cacheRelativePath, 0777, true);
}



$thumb = new Thumbnailer($data, $root, $cacheRelativePath);
$thumb->show();


function IsSafe($string)
{
    if (preg_match('/[\x80-\xFF]/', $string)) {
        return false;
    } else {
        return true;
    }
}

