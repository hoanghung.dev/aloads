<?php
namespace Application\Tracking\Controllers;


use Application\Admin\Models\Apps;
use Application\Admin\Models\Campaigns;
use Application\Admin\Models\Trackings;
use Application\Admin\Models\Users;

class Index extends Base
{
    public function index(){
        //Exam: http://tracking.admobifun.com/links?campaign_id=6&partner_id=4&sub_partner_id=1
        $trackingModel = new Trackings();
        $campaignId = $this->_request->getParam('campaign_id');
        $userId = $this->_request->getParam('partner_id');
        $subPartnerId = $this->_request->getParam('sub_partner_id');
        $urlCurrent = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $params = sprintf('campaign_id=%d&partner_id=%d&sub_partner_id=%d',$campaignId,$userId,$subPartnerId);
        /*$uri = $this->decrypt($hash);
        parse_str($uri);
        $campaignId = isset($campaign_id)?$campaign_id:NULL;
        $userId = isset($user_id)?$user_id:NULL;
        $parentId = isset($parent_id)?$parent_id:NULL;*/
        $REQ = date('YmdHis').rand(100,999);
        $platform = array('Android','iOS','Window Phone');
        $oneCampaign = $this->getCampaign($campaignId);

        $statusUser = $this->statusUser($userId);
        $statusApp = $this->statusApp($oneCampaign->app_id);
        $statusCampaign = $this->statusCampaign($campaignId);
        $statusTracking = $this->statusTracking(md5($params));
        $linkAppsflyers = $oneCampaign->link.'?pid='.$userId.'&af_adset_id='.$subPartnerId.'&c='.$campaignId;
        //echo $linkAppsflyers;exit;

       /* echo "user:".$statusUser."<hr>";
        echo "app:".$statusApp."<hr>";
        echo "campaign:".$statusCampaign."<hr>";
        echo "tracking:".$statusTracking."<hr>";*/

        if($statusUser != 1) die('Link hiện tại không hoạt động do Partner chưa kích hoạt !');
        if($statusApp != 1) die('Link hiện tại không hoạt động do App chưa kích hoạt !');
        if($statusCampaign != 1) die('Link hiện tại không hoạt động do Campaign đang tạm dừng hoặc hoặc kết thúc hoặc chưa kích hoạt !');
        if($statusTracking != 1) die('Link hiện tại không hoạt động do Tracking link bị khóa !');

        $userAgent = get_browser();
        $this->writeLog('TRACKING_' . date('Y-m-d'), 'BEGIN_GET: ' . date('Y-m-d H:i:s') . '=> REQ: '.$REQ.'
        | PARTNER_ID: '.$userId.'
        | SUB_PARTNER_ID: '.$subPartnerId.'
        | APP_ID: '.$oneCampaign->app_id.'
        | CAMPAIGN_ID: '.$campaignId.'
        | URL: '.$urlCurrent.' | IP_CLIENT: '.$this->getIpClient().'
        | URL_REDIRECT: '.$linkAppsflyers.'
        | USER_AGENT: '.print_r($userAgent,true));

        if(in_array($userAgent->platform,$platform)){
            $data['campaign_id'] = $campaignId;
            $data['user_id'] = $userId;
            $data['sub_user'] = $subPartnerId;
            $data['platform'] = $userAgent->platform;
            $data['browser'] = $userAgent->browser;
            $data['created_time'] = date('Y-m-d H:i:s');
            $data['req'] = $REQ;
            $data['ip'] = $this->getIpClient();
            $data['source'] = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:$urlCurrent;

            if(!empty($data['campaign_id']) && !empty($data['user_id'])){
                $data['link'] = $params;
                $data['hash'] = md5($data['link']);
                //Check link và Insert Link
                $oneTracking = $trackingModel->getOne('hash = :hash',array(':hash'=>$data['hash']),'tracking_id');
                if(!isset($oneTracking->tracking_id)){
                    $trackingId = $trackingModel->insert($data);
                    $this->writeLog('TRACKING_' . date('Y-m-d'), '=> REQ: '.$REQ.' | INSERT_LINK: TRACKING_ID:'.$trackingId.' | HASH: '.$data['hash'].' default_tracking success');
                }else $trackingId = isset($oneTracking->tracking_id)?$oneTracking->tracking_id:'';
                if($trackingId != ''){
                    $data['tracking_id'] = $trackingId;
                    $clickId =  $trackingModel->insertClient($data);
                    $this->writeLog('TRACKING_' . date('Y-m-d'), '=> REQ: '.$REQ.' | INSERT_CLICK_ID: CLICK_ID:'.$clickId.' default_client success');
                    $trackingModel->updateClick($trackingId);
                }
                $this->redirect($linkAppsflyers);
            }
            unset($data);
        }else{
            echo "Ứng dụng này không hỗ trợ Platform này !";
            $this->writeLog('TRACKING_' . date('Y-m-d'), '=> REQ: '.$REQ.' | RETURN: Not support platform');
        }
        $this->writeLog('TRACKING_' . date('Y-m-d'), '=> REQ: '.$REQ." | END_GET \n---------------------------------------");
        exit;
    }
    private function statusUser($id){
        $model = new Users();
        $oneItem = $model->getOne('user_id = :id',array(':id'=>$id),'status');
        return $oneItem->status;
    }
    private function statusApp($id){
        $model = new Apps();
        $oneItem = $model->getOne('app_id = :id',array(':id'=>$id),'status');
        return $oneItem->status;
    }
    private function statusCampaign($id){
        $model = new Campaigns();
        $oneItem = $model->getOne('campaign_id = :id',array(':id'=>$id),'status');
        return $oneItem->status;
    }
    private function statusTracking($id){
        $model = new Trackings();
        $oneItem = $model->getOne('hash = :hash',array(':hash'=>$id),'status');
        return isset($oneItem->status)?$oneItem->status:1;
    }
    public function getCampaign($id,$select = '*'){
        $campaignsModel = new Campaigns();
        $data = $campaignsModel->getOne('campaign_id = :id',array(':id'=> $id),$select);
        return $data;
    }
    public function csvToJson(){
        header('Content-type: application/json');
        // Set your CSV feed
        $feed = 'https://docs.google.com/spreadsheet/pub?hl=en_US&hl=en_US&key=0Akse3y5kCOR8dEh6cWRYWDVlWmN0TEdfRkZ3dkkzdGc&single=true&gid=0&output=csv';
        // Arrays we'll use later
        $keys = array();
        $newArray = array();
        // Function to convert CSV into associative array
        function csvToArray($file, $delimiter) {
            if (($handle = fopen($file, 'r')) !== FALSE) {
                $i = 0;
                while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) {
                    for ($j = 0; $j < count($lineArray); $j++) {
                        $arr[$i][$j] = $lineArray[$j];
                    }
                    $i++;
                }
                fclose($handle);
            }
            return $arr;
        }
        // Do it
        $data = csvToArray($feed, ',');
        // Set number of elements (minus 1 because we shift off the first row)
        $count = count($data) - 1;

        //Use first row for names
        $labels = array_shift($data);
        foreach ($labels as $label) {
            $keys[] = $label;
        }
        // Add Ids, just in case we want them later
        $keys[] = 'id';
        for ($i = 0; $i < $count; $i++) {
            $data[$i][] = $i;
        }

        // Bring it all together
        for ($j = 0; $j < $count; $j++) {
            $d = array_combine($keys, $data[$j]);
            $newArray[$j] = $d;
        }
        // Print it out as JSON
        echo json_encode($newArray);
    }

    public function notFound(){
        $this->displayLayout('notfound',$this->render());
    }

}