<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS - CPI | Admobifun</title>

    <link href="/css/style.default.css" rel="stylesheet">
    <link href="/css/morris.css" rel="stylesheet">
    <link href="/css/select2.css" rel="stylesheet" />
    <link href="/css/jquery.tagsinput.css" rel="stylesheet" />
    <link href="/css/toggles.css" rel="stylesheet" />
    <link href="/css/bootstrap-timepicker.min.css" rel="stylesheet" />
    <link href="/css/daterangepicker.css" rel="stylesheet" />
    <link href="/css/colorpicker.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
    <script src="/js/jquery-1.11.1.min.js"></script>
</head>

<body>
<?php echo $this->action('boxHeader','block','admin',$this->params); ?>
<section>
    <div class="mainwrapper">
        <?php echo $this->action('sidebar','block','admin',$this->params); ?>

        <div class="mainpanel">
            <?php echo $this->layoutContent; ?>
        </div>
    </div>
</section>
<?php if(isset($_SESSION['_FM']) && $_SESSION['_FM'] != ''): ?>
    <div class="message-box alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong><?php echo $_SESSION['_FM']; ?></strong>
    </div>
<?php endif; ?>
<script src="/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery-ui-1.10.3.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src="/js/modernizr.min.js"></script>
<script src="/js/pace.min.js"></script>
<script src="/js/retina.min.js"></script>
<script src="/js/jquery.cookies.js"></script>
<script src="/js/bootstrap-timepicker.min.js"></script>
<script src="/js/daterangepicker.js"></script>


<!--form-->
<script src="/js/jquery.autogrow-textarea.js"></script>
<script src="/js/jquery.mousewheel.js"></script>
<script src="/js/jquery.tagsinput.min.js"></script>
<script src="/js/toggles.min.js"></script>
<script src="/js/jquery.maskedinput.min.js"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/colorpicker.js"></script>
<script src="/js/dropzone.min.js"></script>

<script src="/js/custom.js"></script>
<script>
    jQuery(document).ready(function() {

        if($(".message-box a.close").length){
            $(function(){
                setTimeout(closeMessage, 5000);
            });

            function closeMessage(){
                $(".message-box a.close").click();
                $.ajax("/unset-message", {"cache":false});
            }
        }
        <?php if($_SESSION['Default']['auth']->user_id !=0): ?>
        $.get("/user/permissionJson", function(data, status){
            $('button').each(function(index){
                var action = $(this).attr('data-access');
                if(action != undefined && jQuery.inArray(action,data) === -1){
                    $(this).remove();
                }
            });
        });

        <?php endif; ?>
        // Form Toggles
        // Date Picker
        jQuery('input.datepicker').datepicker({
            dateFormat: 'yy/mm/dd',
            endDate: "+1 days"
        });

        function cb(start, end) {
            $('#reportrange input').val(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
        }
        cb(moment().subtract(29, 'days'), moment());

        $('#reportrange').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);



        jQuery("#select-app").select2();
        jQuery("#select-user").select2();
        jQuery("#select-user-parent").select2();
        jQuery("#select-status").select2();

        jQuery('.toggle').toggles({on: true});
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    jQuery(input).parent().find('.preview-result').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".preview").change(function(){
            readURL(this);
        });
        $.getJSON("<?php echo _ROOT_CMS; ?>/files/countries.json", function(data) {
            var option = "";
            $.each(data, function(k, v) {
                option += "<option value='" + v.code + "'>" + v.name + "</option>";
            });
            $("select[name='country']").html(option);
        });
    });
</script>

</body>
</html>