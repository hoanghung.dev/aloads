<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-Imeis"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý Imei (Total: <?php echo $this->totalAll; ?>)</h4>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center">Id</th>
                    <th class="text-center">Tên</th>
                    <th class="text-center" colspan="2">Hành động</th>
                </tr>
                </thead>
                <tbody>
                <?php if($this->data) foreach($this->data as $key=>$item): ?>
                    <tr data-id="<?php echo $item->id; ?>">
                        <td><?php echo $item->id; ?></td>
                        <td><?php echo $item->imei; ?></td>
                        <td>
                            <a href="/imei-edit/<?php echo $item->id; ?>" data-toggle="tooltip" title="Edit"
                               class="tooltips"><i class="fa fa-pencil"></i></a>
                        </td>
                        <td>
                            <a href="javascript:;" data-toggle="tooltip" title="Delete"
                               class="delete-row tooltips btnDelete"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div>
            <ul class="pagination pagination-sm no-margin center-block">
                <?php echo isset($this->paging)?$this->paging:''; ?>
            </ul>
        </div>
    </div>
</div>
<!--
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-1.12.3.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>-->
<script>

    jQuery(document).ready(function() {
        $.getJSON("<?php echo _ROOT_CMS; ?>/app-api", function (data) {
            var option = "<option value=''>All App</option>";
            var element = jQuery("select[name='package']");
            var getSelected = element.data('selected');
            $.each(data, function (k, v) {
                option += "<option value='" + v.packageName + "'>" + v.packageName + "</option>";
            });
            element.html(option);
            element.select2().select2('val', getSelected);
        });
    });
</script>