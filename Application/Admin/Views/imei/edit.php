<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-users"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý imei</h4>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Sửa imei
            </div>
            <?php if($this->data): $oneItem = $this->data; ?>
                <div class="panel-body">
                    <div class="row">
                        <form id="myForm" class="form-horizontal form-bordered" data-toggle="validator" role="form" method="POST" encname_="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">IMEI</label>
                                <div class="col-sm-10">
                                    <input type="text" name="imei" class="form-control" placeholder="IMEI" value="<?php echo $oneItem->imei; ?>" required>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group text-center clearfix">
                                <button name="submit" class="btn btn-primary">Cập nhật</button>
                                <button name="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

