<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 08:03 CH
 */
?>
<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-users"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý Module </h4>
        </div>
    </div>
</div>


<div class="contentpanel">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Thêm mới module
            </div>
            <div class="panel-body">
                <div class="row">
                    <form id="myForm" data-toggle="validator"  role="form" action="/permission/add" method="POST">

                        <div class="form-group">
                            <label>Tên module</label>
                            <input type="text" name="title" class="form-control" placeholder="Tên module" required>
                            <span class="help-block with-errors"></span>
                        </div>

                        <div class="form-group">
                            <label>Mã module</label>
                            <input type="text" name="module_key" class="form-control" placeholder="Mã module" required>
                            <span class="help-block with-errors"></span>
                        </div>
                        <div class="form-group">
                            <label>Module cha</label>
                            <select name="parent_id" class="form-control">
                                <?php echo $this->selectPermission(30); ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Trạng thái</label>
                            <input class="switch-check" type="checkbox" name="status" checked>
                        </div>

                        <button type="submit" class="btn btn-default">Thêm mới</button>
                        <button type="reset" class="btn btn-default">Reset Button</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

