<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-users"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý Module </h4>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="row">
        <div class="table-responsive">
            <table class="table table-hidaction table-hover mb30">
                <thead>
                <tr>
                    <th>ID</th>
                    <th class="text-center">Tên module</th>
                    <th class="text-center">Mã module</th>
                    <th class="text-center">Trạng thái</th>
                    <th class="text-center">ACTION</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($this->data as $item):
                    $child = $this->getPermission($item->permission_id);
                    ?>
                    <tr data-id="<?php echo $item->permission_id; ?>">
                        <td><?php echo $item->permission_id; ?></td>
                        <td><strong><?php echo $item->title; ?></strong></td>
                        <td><?php echo $item->module; ?></td>
                        <td><?php if($item->status == 1) echo "Đã kích hoạt"; else echo "Chưa kích hoạt"; ?></td>
                        <td class="text-center">
                            <button type="button" class="btn btn-success btn-circle" onclick="javascript: window.open('/permission/edit/<?php echo $item->permission_id; ?>','_blank');"><i class="fa fa-pencil"></i></button>
                            <button type="button" class="btn btn-danger btn-circle btnDelete"><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                    <?php if(!empty($child)) foreach($child as $items): ?>
                    <tr data-id="<?php echo $items->permission_id; ?>" >
                        <td><?php echo $items->permission_id; ?></td>
                        <td><i class="fa fa-long-arrow-right fa-w">&nbsp;</i><?php echo $items->title; ?></td>
                        <td><?php echo $items->module; ?></td>
                        <td><?php if($items->status == 1) echo "Đã kích hoạt"; else echo "Chưa kích hoạt"; ?></td>
                        <td class="text-center">
                            <button type="button" class="btn btn-success btn-circle" onclick="javascript: window.open('/permission/edit/<?php echo $items->permission_id; ?>','_blank');"><i class="fa fa-pencil"></i></button>
                            <button type="button" class="btn btn-danger btn-circle btnDelete"><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('.btnTrash').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa tạm bản ghi này không ?") == true){
                $.ajax({
                    url: '/permission/actTrash',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
        $('.btnUnTrash').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn khôi phục bản ghi này không ?") == true){
                $.ajax({
                    url: '/permission/actUnTrash',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
        $('.btnDelete').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa bản ghi này khỏi hệ thống không ?") == true){
                $.ajax({
                    url: '/permission/actDelete',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
    })
</script>