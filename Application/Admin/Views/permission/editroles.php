<?php
$item = $this->data;
?>
<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-users"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý Roles </h4>
        </div>
    </div>
</div>

<?php if(!empty($item)): ?>
    <div class="contentpanel">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Thêm mới roles
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="myForm" role="form" action="" method="POST">

                            <div class="form-group">
                                <label>Tên nhóm</label>
                                <input name="title" class="form-control" placeholder="Tên nhóm" value="<?php echo $item->title; ?>">
                            </div>
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <input class="switch-check" type="checkbox" name="status" checked>
                            </div>
    
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
