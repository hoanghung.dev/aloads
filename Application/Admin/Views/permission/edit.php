<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 08:01 CH
 */
$item = $this->data;
?>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Quản lý chức năng</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
<?php if(!empty($item)): ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Sửa chức năng <?php echo $item->title; ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-5">
                            <form id="myForm" data-toggle="validator"  role="form" action="/permission/edit/<?php echo $item->permission_id; ?>" method="POST">

                                <div class="form-group">
                                    <label>Tên module</label>
                                    <input name="title" class="form-control" placeholder="Title" value="<?php echo $item->title; ?>" required>
                                    <span class="help-block with-errors"></span>
                                </div>
                                <div class="form-group">
                                    <label>Mã module</label>
                                    <input type="text" name="module_key" class="form-control" placeholder="Mã module"  value="<?php echo $item->module; ?>"  required>
                                    <span class="help-block with-errors"></span>
                                </div>
                                <div class="form-group">
                                    <label>Module cha</label>
                                    <select name="parent_id" class="form-control">
                                        <?php $listParent = $this->selectPermission($item->parent_id); ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Trạng thái</label>
                                    <input class="switch-check" type="checkbox" name="status" <?php if($item->status == 1) echo "checked"; ?>>
                                </div>

                                <button type="submit" class="btn btn-default">Sửa</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
<?php endif; ?>