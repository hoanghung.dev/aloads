<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 03:52 CH
 */

?>
<style type="text/css">
    th {
        color: #57378A!important;
    }
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#999;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}
    .tg .tg-5y5n{background-color:#ecf4ff}
    .tg .tg-uhkr{background-color:#efefef}
    .tg tr:hover td{background-color: rgb(147, 192, 139) !important;}
    .color-red{
        background-color: #FF0000 !important;}
    i.s25
    {
        font-size:25px;
    }
</style>
<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-users"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý Roles </h4>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Bảng phân quyền theo nhóm thành viên
                <button type="button" class="btn btn-primary btnUpdate">Cập nhật quyền</button>
                <span class="ajax-loader hidden"><img src="../images/ajax-loader-3.gif"> </span>
            </div>
            <?php if($this->listPermission && $this->listGroup): ?>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover tg">
                            <tr>
                                <th class="tg-5y5n"></th>
                                <?php $i = 2; $countGroup = count($this->listGroup); foreach($this->listGroup as $item):
                                    //$arrPerm = explode('|',$item->role_permission);
                                    //$listPerm = implode(',',$arrPerm);
                                    ?>
                                    <th class="tg-031e text-center group" data-perm="<?php echo $item->role_permission; ?>" data-col="<?php echo $i++; ?>" data-id="<?php echo $item->group_id; ?>">
                                        <strong><?php echo $item->title; ?></strong>
                                        <input type="checkbox" class="btnCheckAll"> Check All
                                    </th>
                                <?php endforeach; ?>
                            </tr>
                            <?php foreach($this->listPermission as $items):
                                $child = $this->getPermission($items->permission_id);
                                ?>
                                <tr data-id="<?php echo $items->permission_id; ?>" >
                                    <td class="tg-uhkr col-lg-3"><strong><?php echo $items->title; ?></strong></td>
                                    <?php if($countGroup) for($j = 1;$j <= $countGroup;$j++): ?>
                                        <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                    <?php endfor; ?>
                                    <!--<td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                    <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                    <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                    <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>-->
                                </tr>
                                    <?php if(!empty($child)) foreach($child as $item): ?>
                                        <tr data-id="<?php echo $item->permission_id; ?>" >
                                            <td class="tg-uhkr col-lg-3"><i class="fa fa-long-arrow-right fa-w">&nbsp;</i><?php echo $item->title; ?></td>
                                            <?php if($countGroup) for($j = 1;$j <= $countGroup;$j++): ?>
                                                <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                            <?php endfor; ?>
                                            <!--<td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                            <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                            <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>
                                            <td class="tg-031e text-center"><input type="checkbox" class="btn btn-default btn-sm"><i class=""></i></td>-->
                                        </tr>
                                    <?php endforeach; ?>
                            <?php endforeach; ?>

                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            <?php endif; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('table tr th.group').each(function(){
            var role = $(this).attr('data-perm');
            var allPermission=role.split('|');
            var col = $(this).attr('data-col');
            //console.log(allPermission);
            $('table tr td:nth-child('+col+')').each(function(){
                var permId = $(this).parents('tr').attr('data-id');
                if ($.inArray(permId,allPermission) > -1){
                    //console.log(permId+' Checked');
                    $(this).find('input[type="checkbox"]').attr("checked","checked");
                }
            });
        });
        $('input[type="checkbox"].btnCheckAll').click(function(){
            var col = $(this).parents('th').attr('data-col');
            if($(this).is(":checked")) {
                $('table tr td:nth-child('+col+') input').each(function(){
                    this.checked = true;
                });
            } else {
                $('table tr td:nth-child('+col+') input').each(function(){
                    this.checked = false;
                });
            }

        });


        $('.btnUpdate').click(function(){
            console.clear();
            $('table tr th.group').each(function(){
                var groupId = $(this).attr('data-id');
                var col = $(this).attr('data-col');
                var allPermission = '';
                $('table tr td:nth-child('+col+')').each(function(){

                    var permId = $(this).parents('tr').attr('data-id');
                    var checked = $(this).val();
                    if ($(this).find('input[type="checkbox"]').is(":checked")){
                        if(allPermission != ''){
                            allPermission += '|'+permId;
                        }else{
                            allPermission += permId;
                        }
                    }
                    $(this).find('i').addClass('s25 fa fa-smile-o pull-right');
                });
                $.ajax({
                    type:'POST',
                    url: '/permission/actUpdate',
                    data: {id: groupId,permission:allPermission},
                    beforeSend: function(){
                        $('.ajax-loader').removeClass('hidden');
                    },
                    success: function(response) {
                        /*alert(response);
                         location.reload();
                         }*/
                        console.log(response);
                        $('.ajax-loader').html('<div class="alert alert-success alert-dismissable col-lg-7 pull-right" style="margin: 0;padding: 5px;">'+response+'</div>');
                    }
                });

                console.log(allPermission);
                ///$('table tr td:nth-child('+col+') i').addClass('s25 fa fa-smile-o pull-right');
            });
            /*var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa tạm bản ghi này không ?") == true){
                $.ajax({
                    url: '/permission/actTrash',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }*/

        })
    })
</script>