<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-th-large"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>App</h4>
        </div>
    </div>
</div>
<div class="contentpanel">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Add App
            </div>
            <div class="panel-body">
                <div class="row">
                    <form id="myForm" class="form-horizontal form-bordered" data-toggle="validator"  role="form" method="POST" enctype="multipart/form-data">
                        <?php for($i = 0;$i < $this->countColumn; $i++):
                            $metaName = $this->getColumnMeta('app',$i);
                            //echo $this->getCommentField($metaName['name']);
                            if($metaName['name'] == 'packageName'): ?>
                                <div class="form-group col-xs-12">
                                    <label class="col-sm-2 control-label" data-toggle="tooltip" data-placement="top" title="<?php echo $this->getCommentField($metaName['table'],$metaName["name"]);?>"><?php echo $metaName['name']; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="<?php echo $metaName["name"]; ?>" placeholder="<?php echo $this->getCommentField($metaName['table'],$metaName["name"]);?>" class="form-control" required/>
                                    </div>
                                </div>
                            <?php
                            elseif(in_array($metaName['name'],array('appId','createdAt','updatedAt')) == false):
                                if($metaName['native_type'] == 'TINY'): ?>
                                    <div class="form-group col-md-4 col-xs-12">
                                        <label class="col-sm-5 control-label" data-toggle="tooltip" data-placement="top" title="<?php echo $this->getCommentField($metaName['table'],$metaName["name"]);?>"><?php echo $metaName['name']; ?></label>
                                        <div class="col-sm-7">
                                            <input type="checkbox" name="<?php echo $metaName["name"]; ?>" placeholder="<?php echo $this->getCommentField($metaName['table'],$metaName["name"]);?>" class="form-control"/>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label class="col-sm-4 control-label" data-toggle="tooltip" data-placement="top" title="<?php echo $this->getCommentField($metaName['table'],$metaName["name"]);?>"><?php echo $metaName['name']; ?></label>
                                        <div class="col-sm-8">
                                            <?php
                                            switch ($metaName['native_type']):
                                                case 'DATETIME':
                                                    echo '<input type="date"  name="'.$metaName["name"].'" placeholder="" class="form-control" required/>';
                                                    break;
                                                default:
                                                    echo  '<input type="text" name="'.$metaName["name"].'" placeholder="'.$this->getCommentField($metaName['table'],$metaName["name"]).'" class="form-control" required/>';
                                            endswitch;
                                            ?>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                <?php endif; endif; endfor; ?>
                        <div class="form-group text-center clearfix">
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>

<script>
    $('input[name=adEnable]').click(function () {
        alert('a');
    });
</script>