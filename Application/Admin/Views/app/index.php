<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-th-large"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>App (Total: <?php echo $this->total; ?>)</h4>
        </div>
    </div>
</div>
<style>
    table tr td:nth-child(11), table tr td:nth-child(12) {
        min-width: 135px;
    }

    table tr td input {
        border: none;
        outline: none;
        text-align: center;
        overflow: hidden;
        text-overflow: ellipsis;
        width: 150px;
    }

</style>
<div class="contentpanel">
    <div class="row filter">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="javascript:;" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i
                            class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i
                            class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Filter</h4>
            </div>
            <div class="panel-body nopadding">
                <form class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Query</label>
                                <div class="col-sm-9">
                                    <input type="text" name="query"
                                           placeholder="Input query: (field1=value1&field2=value2)"
                                           value="<?php echo isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : ''; ?>"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="submit" class="btn btn-sm btn-warning">Filter</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive" style="overflow: auto;">
            <table class="table table-bordered mb30">
                <thead>
                <?php
                $fieldsChange = [
                    'contactEnable', 'notificationEnable', 'logClickEnable', 'adminPermissionEnable', 'smsEnable', 'shortcutEnable', 'pictureEnable', 'videoEnable', 'adEnable'
                ]
                ?>
                <tr>
                    <th>STT</th>
                    <?php for ($i = 0; $i < $this->countColumn; $i++):
                        $metaName = $this->getColumnMeta('app', $i);
                        if (in_array($metaName['name'], array('createdAt', 'updatedAt')) == false):
                            ?>
                            <th class="text-center"><?php echo $metaName['name']; ?></th>
                        <?php endif; endfor; ?>
                    <th class="text-center" colspan="2">ACTION</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($this->data)) foreach ($this->data as $key => $item): ?>
                    <?php $color = '';
                    if ($key % 2) $color = "background-color:#d6d6d6"; ?>
                    <tr data-id="<?php echo $item->appId; ?>" style="<?php echo $color; ?>">
                        <td><?php echo $key + 1; ?></td>
                        <?php for ($i = 0; $i < $this->countColumn; $i++):
                            $metaName = $this->getColumnMeta('app', $i);
                            $nameField = $metaName['name'];
                            if (in_array($metaName['name'], array('createdAt', 'updatedAt')) == false):
                                if (strpos($metaName['name'], 'ime') !== false) {
                                    $val = $item->$nameField . " (s)";
                                } else {
                                    if(in_array($metaName['name'], $fieldsChange) && $item->$nameField == 1)
                                        $val = 'yes';
                                    elseif(in_array($metaName['name'], $fieldsChange) && $item->$nameField == 0)
                                        $val = 'no';
                                    elseif($metaName['name'] == 'appStatus' && $item->$nameField == 0)
                                        $val = 'DIE';
                                    elseif($metaName['name'] == 'appStatus' && $item->$nameField == 1)
                                        $val = 'LIVE';
                                    else
                                        $val = $item->$nameField;
                                }
                                ?>
                                <td><input style="<?php echo $color;?>" class="<?php  if(in_array($val, ['yes', 'LIVE'])) echo 'blue'; elseif(in_array($val, ['no', 'DIE'])) echo 'red'; ?>" type="text"
                                           value='<?php echo $val;?>'
                                           onclick="this.select();"></td>
                            <?php endif; endfor; ?>
                        <td>
                            <a href="/app-edit/<?php echo $item->appId; ?>" data-toggle="tooltip" title="Edit"
                               class="tooltips"><i class="fa fa-pencil"></i></a>
                        </td>
                        <td>
                            <a href="javascript:;" data-toggle="tooltip" title="Delete"
                               class="delete-row tooltips btnDelete"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div>
            <ul class="pagination pagination-sm no-margin center-block">
                <?php echo isset($this->paging) ? $this->paging : ''; ?>
            </ul>
        </div>
    </div>
</div>
