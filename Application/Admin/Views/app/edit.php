<?php
$item = $this->data;
?>
<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-th-large"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>App</h4>
        </div>
    </div>
</div>
<?php $oneItem = $this->data; ?>
<div class="contentpanel">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Edit App
            </div>
            <div class="panel-body">
                <div class="row">
                    <form id="myForm" class="form-horizontal form-bordered" data-toggle="validator"  role="form" method="POST" enctype="multipart/form-data">
                        <?php for($i = 0;$i < $this->countColumn; $i++):
                            $metaName = $this->getColumnMeta('app',$i);
                            if(in_array($metaName['name'],array('appId','createdAt','updatedAt')) == false):
                                if($metaName['native_type'] == 'TINY'): ?>
                                    <div class="form-group col-md-4 col-xs-12">
                                        <label class="col-sm-5 control-label" data-toggle="tooltip" data-placement="top" title="<?php echo $this->getCommentField($metaName['table'],$metaName["name"]);?>"><?php echo $metaName['name']; ?></label>
                                        <div class="col-sm-7">
                                            <?php
                                                $checked = '';
                                                if($oneItem->$metaName["name"] == 1) $checked = 'checked';
                                                echo '<input type="checkbox" name="'.$metaName["name"].'" '.$checked.' placeholder="" class="form-control"/>';
                                            ?>
                                        </div>
                                    </div>
                                    <?php else: ?>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label class="col-sm-4 control-label"  data-toggle="tooltip" data-placement="top" title="<?php echo $this->getCommentField($metaName['table'],$metaName["name"]);?>"><?php echo $metaName['name']; ?></label>
                                        <div class="col-sm-8">
                                            <?php
                                            switch ($metaName['native_type']):
                                                case 'DATETIME':
                                                    echo '<input type="date"  name="'.$metaName["name"].'" value="'.date('Y-m-d',strtotime($oneItem->$metaName["name"])).'" placeholder="" class="form-control" required/>';
                                                    break;
                                                default:
                                                    echo  '<input type="text" name="'.$metaName["name"].'" value="'.$oneItem->$metaName["name"].'" placeholder="" class="form-control" required/>';
                                            endswitch;
                                            ?>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                            <?php endif; endif; endfor; ?>
                        <div class="form-group text-center clearfix">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>