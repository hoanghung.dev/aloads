<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 22/07/2015
 * Time: 10:49 SA
 */
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Thống kê</h1>
    </div>
    <?php echo $this->render('/_layouts/_filter_date.php'); ?>
</div>

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            Thống kê điểm của thuê bao
        </div>
        <div class="panel-body">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Số thuê bao</th>
                                    <th>Ngày hoạt động</th>
                                    <th>Ngày hủy</th>
                                    <th>Điểm</th>
                                    <th>Trạng thái</th>
                                    <th>Gói dịch vụ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($this->data)): $i = 0; foreach($this->data as $oneItem): $i++;
                                    $status = '';
                                    if(isset($oneItem->STATUS) && $oneItem->STATUS == 1) $status = 'Đang hoạt động';
                                    else $status = 'Đang hủy';
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo isset($oneItem->MSISDN)?$oneItem->MSISDN:''; ?></td>
                                        <td><?php echo isset($oneItem->ACTIVE_TIME)?date('d/m/Y H:i:s',strtotime($oneItem->ACTIVE_TIME)):''; ?></td>
                                        <td><?php echo isset($oneItem->DEACTIVE_TIME)?date('d/m/Y H:i:s',strtotime($oneItem->DEACTIVE_TIME)):''; ?></td>
                                        <td><?php echo isset($oneItem->SCORE)?$oneItem->SCORE:''; ?></td>
                                        <td><?php echo $status; ?></td>
                                        <td><?php echo isset($oneItem->PACKAGE_NAME)?$oneItem->PACKAGE_NAME:''; ?></td>
                                    </tr>
                                <?php endforeach; else: ?>
                                    <tr><td colspan="7" class="text-center">Không có thông tin !</td></tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                            <div class="col-lg-5 text-center">
                                <?php echo isset($this->paging)?$this->paging:''; ?>
                            </div>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                </div>
                <!-- /.panel -->
            </div>
        </div>

    </div>
</div>