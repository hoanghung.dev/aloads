<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-users"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý thành viên</h4>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Thêm mới thành viên
            </div>
            <div class="panel-body">
                <div class="row">
                        <form id="myForm" class="form-horizontal form-bordered" data-toggle="validator" role="form" method="POST">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" data-toggle="tooltip" data-placement="top" title="Tên package">Package name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="package" class="form-control" required>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" data-toggle="tooltip" data-placement="top" title="IMEI">IMEI</label>
                                <div class="col-sm-10">
                                    <input type="text" name="imei" class="form-control" placeholder="IMEI" required>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" data-toggle="tooltip" data-placement="top" title="Quốc gia">Country</label>
                                <div class="col-sm-10">
                                    <input type="text" name="country" class="form-control" placeholder="Country" required>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Android Version</label>
                                <div class="col-sm-10">
                                    <input type="text" name="androidVersionCode" class="form-control" placeholder="Android Version" required>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" data-toggle="tooltip" data-placement="top" title="Thiết bị">Device</label>
                                <div class="col-sm-10">
                                    <input type="text" name="device" class="form-control" placeholder="Device" required>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group text-center clearfix">
                                <button type="submit" class="btn btn-primary">Thêm mới</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>

                        </form>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>

