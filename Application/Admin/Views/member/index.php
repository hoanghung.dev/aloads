<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-users"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý User (Total: <?php echo $this->total; ?>)</h4>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="row filter">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="javascript:;" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Lọc</h4>
            </div>
            <div class="panel-body nopadding">
                <form class="form-horizontal form-bordered" method="get" enctype="application/x-www-form-urlencoded">
                    <div class="col-xs-6">
                        <div class="form-group col-xs-12">
                            <label class="col-sm-4 control-label">package</label>
                            <div class="col-sm-8">
                                <select title="package" name="package" data-selected="<?php echo isset($_GET['package'])?$_GET['package']:''; ?>" data-placeholder="All" class="width300"></select>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label class="col-xs-4 control-label">androidVersion</label>
                            <div class="col-xs-8"> 
                                <div class="row">
                                    <?php
                                    $versions = [
                                        1     => '1.0',
                                        2     => '1.1',
                                        3     => '1.5',
                                        4     => '1.6',
                                        5     => '2.0',
                                        6     => '2.0.1',
                                        7     => '2.1',
                                        8     => '2.2',
                                        9     => '2.3',
                                        10    => '2.3.3',
                                        11    => '3.0',
                                        12    => '3.1',
                                        13    => '3.2',
                                        14    => '4.0',
                                        15    => '4.0.3',
                                        16    => '4.1',
                                        17    => '4.2',
                                        18    => '4.3',
                                        19    => '4.4',
                                        20    => '4.4W',
                                        21    => '5.0',
                                        22    => '5.1',
                                        23    => '6.0',
                                        24    => '7.0',
                                    ];
                                    ?>
                                    <div class="col-xs-6">
                                        <select name="version_start" class="form-control" data-selected="<?php echo isset($_GET['androidVersionstart'])?$_GET['androidVersionstart']:0; ?>">
                                            <option value="">Tất cả</option>
                                            <?php foreach($versions as $key => $item):?>
                                            <option value="<?php echo $key;?>" <?php if($this->filter['version_start'] == $key) echo 'selected';?>><?php echo $item;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div class="col-xs-6">
                                        <select name="version_end" class="form-control">
                                            <option value="">Tất cả</option>
                                            <?php foreach($versions as $key => $item):?>
                                                <option value="<?php echo $key;?>" <?php if($this->filter['version_end'] == $key) echo 'selected';?>><?php echo $item;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label class="col-sm-4 control-label">device</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" name="device" value="<?php echo isset($_GET['device'])?$_GET['device']:''; ?>" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group col-xs-12">
                            <label class="col-sm-3 control-label">appVersion</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" name="appVersion" value="<?php echo isset($_GET['appVersion'])?$_GET['appVersion']:''; ?>" placeholder="">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label class="col-sm-3 control-label">country</label>
                            <div class="col-sm-9">
                                <select title="country" name="country" data-selected="<?php echo isset($_GET['country'])?$_GET['country']:''; ?>" data-placeholder="All" class="width300"></select>
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label class="col-xs-3 control-label">Chọn ngày</label>
                            <div class="col-xs-9">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input name="start_date" type="date" class="form-control" value="<?php echo date('Y-m-d', strtotime($this->filter['start_date']));?>" data-date-format="d/m/Y">
                                    </div>
                                    <div class="col-xs-6">
                                        <input name="end_date" type="date" class="form-control" value="<?php echo date('Y-m-d', strtotime($this->filter['end_date']));?>" data-date-format="d/m/Y">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12" style="text-align: center;">
                        <button type="submit" class="btn btn-sm btn-warning" style="font-size: 17px;">Tìm kiếm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <p style="width: 100%; text-align: center;font-weight: bolder;font-size: 18px;">Tổng số user: <?php echo $this->totalFilter; ?></p>
    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <label class="col-xs-6" style="padding-left: 0;font-weight: bold;">Số dòng trên trang</label>
                <div class="col-xs-6">
                    <select name="paginate" class="form-control">
                        <option value="10">10</option>
                        <option value="15" <?php if($this->filter['limit']==15) echo 'selected';?>>15</option>
                        <option value="20" <?php if($this->filter['limit']==20) echo 'selected';?>>20</option>
                        <option value="25" <?php if($this->filter['limit']==25) echo 'selected';?>>25</option>
                        <option value="30" <?php if($this->filter['limit']==30) echo 'selected';?>>30</option>
                    </select>
                </div>
            </div>
        </div>
        <a href="/member-export" target="_blank" class="btn btn-info" style="margin-right: 0px;float: right;margin-bottom: 10px;">Xuất Excel</a>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th class="text-center">Package Name<i class="glyphicon glyphicon-chevron-down" aria-hidden="true"></i></th>
                    <th class="text-center">Phone</th>
                    <th class="text-center">Country</th>
                    <th class="text-center">Version</th>
                    <th class="text-center">App Version</th>
                    <th class="text-center">Device</th>
                    <th class="text-center">IMEI</th>
                    <th class="text-center">Tạo lúc</th>
                    <th class="text-center">Cập nhật</th>
                    <th class="text-center" colspan="2">Hành động</th>
                </tr>
                </thead>
                <tbody>
                <?php if($this->data) foreach($this->data as $key=>$item): ?>
                    <tr data-id="<?php echo $item->userId; ?>">
                        <td><?php echo $item->userId; ?></td>
                        <td><?php echo $item->package; ?></td>
                        <td><?php echo $item->phone; ?></td>
                        <td><?php echo $item->country; ?></td>
                        <td><?php echo $versions[$item->androidVersionCode]; ?></td>
                        <td><?php echo $item->appVersion; ?></td>
                        <td><?php echo $item->device; ?></td>
                        <td><?php echo $item->imei; ?></td>
                        <td><?php echo $item->createdAt; ?></td>
                        <td><?php echo $item->updatedAt; ?></td>
                        <td>
                            <a href="/member-edit/<?php echo $item->userId; ?>" data-toggle="tooltip" title="Edit" class="tooltips" style="float: left;margin-left: 4px;">
                                <i class="fa fa-pencil" style="font-size: 19px;"></i></a>
                        </td>
                        <td>
                            <a href="javascript:;" data-toggle="tooltip" title="Delete" class="delete-row tooltips btnDelete" style="float: right;margin-right: 2px;">
                                <i class="fa fa-trash-o" style="font-size: 19px;"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div>
            <ul class="pagination pagination-sm no-margin center-block">
                <?php echo isset($this->paging)?$this->paging:''; ?>
            </ul>
        </div>
    </div>



<!--Bieu do-->



    <script type="text/javascript">
        window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer",
                {
                    title:{
                        text: "Thống kê Country"
                    },
                    exportFileName: "Pie Chart",
                    exportEnabled: true,
                    animationEnabled: true,
                    legend:{
                        verticalAlign: "bottom",
                        horizontalAlign: "center"
                    },
                    data: [
                        {
                            type: "pie",
                            showInLegend: false,
                            toolTipContent: "{name}: <strong>{y}%</strong>",
                            indexLabel: "{name} {y}%",
                            dataPoints: [
                                <?php foreach ($this->countryCount as $key => $item):?>
                                {  y: <?php echo number_format(($item->count/$this->totalFilter) * 100, 2, '.', '.');?>, name:"<?php echo $item->country?>"},
                                <?php endforeach;?>
                            ]
                        }
                    ]
                });
            chart.render();

            var chart2 = new CanvasJS.Chart("chartContainer2",
                {
                    title:{
                        text: "Thống kê android version"
                    },
                    exportFileName: "Pie Chart",
                    exportEnabled: true,
                    animationEnabled: true,
                    legend:{
                        verticalAlign: "bottom",
                        horizontalAlign: "center"
                    },
                    data: [
                        {
                            type: "pie",
                            showInLegend: false,
                            toolTipContent: "{name}: <strong>{y}%</strong>",
                            indexLabel: "{name} {y}%",
                            dataPoints: [
                                <?php foreach ($this->countOSVersion as $key => $item):?>
                                {  y: <?php echo number_format(($item->count/$this->totalFilter) * 100, 2, '.', '.');?>, name:"<?php echo $versions[$item->androidVersionCode]?>-"},
                                <?php endforeach;?>
                            ]
                        }
                    ]
                });
            chart2.render();
        }
    </script>
    <script type="text/javascript" src="js/canvasjs.min.js"></script>
</div>
<div class="bieu-do">
<div id="chartContainer" style="height: 500px; width: 100%;"></div>
<hr>
<div id="chartContainer2" style="height: 500px; width: 100%;"></div>
</div>
<!--END bieu do-->



<!--
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-1.12.3.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>-->
<script>

    jQuery(document).ready(function() {
        $.getJSON("<?php echo _ROOT_CMS; ?>/files/countries.json", function (data) {
            var option = "<option value='' selected='selected'>All Country</option>";
            var element = jQuery("select[name='country']");
            var getSelected = element.data('selected')?element.data('selected'):'';
            $.each(data, function (k, v) {
                option += "<option value='" + v.code + "'>" + v.name + "</option>";
            });
            element.html(option);
            element.select2().select2('val', getSelected);
        });
        $.getJSON("<?php echo _ROOT_CMS; ?>/app-api", function (data) {
            var option = "<option value=''>All App</option>";
            var element = jQuery("select[name='package']");
            var getSelected = element.data('selected');
            $.each(data, function (k, v) {
                option += "<option value='" + v.packageName + "'>" + v.packageName + "</option>";
            });
            element.html(option);
            element.select2().select2('val', getSelected);
        });

        // Phan trang
        $('select[name=paginate]').change(function () {
            var paginate = $(this).val();
            var href = window.location.href;
            var find = href.indexOf('?');

            if(find >= 0) {
                window.location.href = href + '&paginate=' + paginate;
            } else {
                window.location.href = href + '?paginate=' + paginate;
            }
        });
    });
</script>

