<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Địa chỉ Google maps của các Partner </h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="gmap-marker" class="height500"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="/js/gmaps.js"></script>
<script>
    jQuery(document).ready(function(){

        /* new GMaps({
         div: '#gmap',
         lat: -12.043333,
         lng: -77.028333
         });
         */
        var map_marker = new GMaps({
            div: '#gmap-marker',
            lat: -12.043333,
            lng: -77.028333
        });

        map_marker.addMarker({
            lat: -12.043333,
            lng: -77.028333,
            click: function(e) {
                alert('You clicked in this marker');
            }
        });

    });
</script>