<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-users"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý thành viên</h4>
        </div>
    </div>
</div>
<?php
echo "<!--<pre>";
print_r($_GET);
echo "</pre>-->";
?>
<div class="contentpanel">
    <div class="row filter">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="javascript:;" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Filter</h4>
            </div>
            <div class="panel-body nopadding">
                <form class="form-horizontal form-bordered" method="get" enctype="application/x-www-form-urlencoded">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tên</label>
                            <div class="col-sm-9">
                                <input type="text" name="full_name" placeholder="Tên" class="form-control" value="<?php echo isset($_GET['full_name'])?$_GET['full_name']:''; ?>"/>
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Giới thiệu bởi</label>
                            <select id="select-user" name="referral_id" data-placeholder="Giới thiệu bởi" class="col-sm-9">
                                <option value="">Chọn người giới thiệu</option>
                                <?php $listAllUser = $this->getListUser(); if(!empty($listAllUser)) foreach($listAllUser as $item): ?>
                                    <option value="<?php echo $item->user_id; ?>" <?php if(isset($_GET['referral_id']) && $_GET['referral_id'] == $item->user_id) echo "selected"; ?>><?php echo $item->full_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-9">
                                <input type="text" name="user_name" placeholder="Username" class="form-control" value="<?php echo isset($_GET['user_name'])?$_GET['user_name']:''; ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" name="email" placeholder="Email" class="form-control" value="<?php echo isset($_GET['email'])?$_GET['email']:''; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số điện thoại</label>
                            <div class="col-sm-9">
                                <input type="text" name="tel" placeholder="Số điện thoại" class="form-control" value="<?php echo isset($_GET['tel'])?$_GET['tel']:''; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Trạng thái</label>
                            <div class="col-sm-9 control-label">
                                <div class="toggle toggle-primary"></div>
                                <input type="hidden" name="status" value="1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nhóm Roles</label>
                            <div class="col-sm-9 control-label">
                                <select id="select-search-hide" name="group_id" data-placeholder="Chọn nhóm" class="width300">
                                    <option value="">Tất cả</option>
                                    <?php echo $this->getUserGroup(isset($_GET['group_id'])?$_GET['group_id']:''); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số dư</label>
                            <div class="col-sm-9">
                                <div class="col-sm-4 control-label">
                                    <input type="text" name="balances[0]" class="form-control" value="<?php echo isset($_GET['balances']['0'])?$_GET['balances']['0']:''; ?>"/>
                                </div>
                                <label class="col-sm-2 control-label">Đến</label>
                                <div class="col-sm-4 control-label">
                                    <input type="text" name="balances[1]" class="form-control" value="<?php echo isset($_GET['balances']['1'])?$_GET['balances']['1']:''; ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Loại user</label>
                            <div class="col-sm-4">
                                <div class="ckbox ckbox-default">
                                    <input type="checkbox" name="type[0]" id="checkboxDefault" <?php if(isset($_GET['type'][0]) && $_GET['type'][0] == 'on') echo "checked"; ?>>
                                    <label for="checkboxDefault">Chuẩn</label>
                                </div>
                                <div class="ckbox ckbox-primary">
                                    <input type="checkbox" name="type[1]" id="checkboxPrimary" <?php if(isset($_GET['type'][1]) && $_GET['type'][1] == 'on') echo "checked"; ?>>
                                    <label for="checkboxPrimary">Wap Master</label>
                                </div>
                                <div class="ckbox ckbox-warning">
                                    <input type="checkbox" name="type[2]" id="checkboxWarning" <?php if(isset($_GET['type'][2]) && $_GET['type'][2] == 'on') echo "checked"; ?>>
                                    <label for="checkboxWarning">App Master</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="ckbox ckbox-success">
                                    <input type="checkbox" name="type[3]" id="checkboxSuccess" <?php if(isset($_GET['type'][3]) && $_GET['type'][3] == 'on') echo "checked"; ?>>
                                    <label for="checkboxSuccess">Phone Shop</label>
                                </div>
                                <div class="ckbox ckbox-danger">
                                    <input type="checkbox" name="type[4]" id="checkboxDanger" <?php if(isset($_GET['type'][4]) && $_GET['type'][4] == 'on') echo "checked"; ?>>
                                    <label for="checkboxDanger">Adnetwork</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Danh sách cài App</label>
                            <div class="col-sm-9">
                                <input name="tags" id="tags" class="form-control" value="<?php echo isset($_GET['tags'])?$_GET['tags']:''; ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="panel-body clearfix text-center">
                        <button class="btn btn-primary">Lọc thành viên</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>STT</th>
                    <th class="text-center">User Name</th>
                    <th class="text-center">Full Name</th>
                    <th class="text-center">Nhóm</th>
                    <th class="text-center">Telephone</th>
                    <th class="text-center">Trạng thái</th>
                    <th class="text-center">ACTION</th>
                </tr>
                </thead>
                <tbody>
                <?php if($this->data) foreach($this->data as $key=>$item):
                    $status = '';
                    if($item->status == 1){
                        $status = 'Kích hoạt';
                    }else{
                        $status = 'Chưa kích hoạt';
                    }
                    ?>
                    <tr data-id="<?php echo $item->user_id; ?>">
                        <td><?php echo $key; ?></td>
                        <td><?php echo $item->user_name; ?></td>
                        <td><?php echo $item->full_name; ?></td>
                        <td class="text-center">
                            <select disabled class="form-control">
                                <?php echo $this->getUserGroup($item->group_id); ?>
                            </select>
                        </td>
                        <td><?php echo $item->tel; ?></td>
                        <td><?php echo $status; ?></td>
                        <td>
                            <a href="/user-edit/<?php echo $item->user_id; ?>" data-toggle="tooltip" title="Edit" class="tooltips"><i class="fa fa-pencil"></i></a>
                            <?php if($item->user_id != 0): ?><a href="javascript:;" data-toggle="tooltip" title="Delete" class="delete-row tooltips btnDelete"><i class="fa fa-trash-o"></i></a><?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        // Select2
        jQuery("#select-basic, #select-multi").select2();
        jQuery('#select-search-hide').select2({
            minimumResultsForSearch: -1
        });
        // Tags Input
        jQuery('#tags').tagsInput({width:'auto'});

        $('.toggle').on('toggle', function(e, active) {
            if (active) {
                $('input[name="status"]').val(1);
            } else {
                $('input[name="status"]').val(0);
            }
        });
        $('.btnDelete').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa thành viên này khỏi hệ thống không ?") == true){
                $.ajax({
                    url: '/user/actDelete',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
    })
</script>