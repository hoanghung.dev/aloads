<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-money"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý hoa hồng giới thiệu</h4>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="javascript:;" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Filter</h4>
            </div>
            <div class="panel-body nopadding">
                <form class="form-horizontal form-bordered" method="POST" enctype="multipart/form-data">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tên</label>
                            <div class="col-sm-9">
                                <input type="text" name="full_name" placeholder="Tên" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Giới thiệu bởi</label>
                            <div class="col-sm-9">
                                <input type="text" name="referral" placeholder="Giới thiệu bởi" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-9">
                                <input type="text" name="username" placeholder="Username" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" name="email" placeholder="Email" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số điện thoại</label>
                            <div class="col-sm-9">
                                <input type="text" name="tel" placeholder="Số điện thoại" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nhóm</label>
                            <div class="col-sm-9 control-label">
                                <select id="select-search-hide" name="group_id" data-placeholder="Chọn nhóm" class="width300">
                                    <?php echo $this->getUserGroup(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Trạng thái</label>
                            <div class="col-sm-9 control-label">
                                <div class="toggle toggle-primary"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số dư</label>
                            <div class="col-sm-9">
                                <div class="col-sm-4">
                                    <input type="text" name="balances[]" class="form-control" />
                                </div>
                                <label class="col-sm-2">Đến</label>
                                <div class="col-sm-4">
                                    <input type="text" name="balances[]" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Loại user</label>
                            <div class="col-sm-4">
                                <div class="ckbox ckbox-default">
                                    <input type="checkbox" name="type[]" id="checkboxDefault">
                                    <label for="checkboxDefault">Chuẩn</label>
                                </div>
                                <div class="ckbox ckbox-primary">
                                    <input type="checkbox" name="type[]" id="checkboxPrimary">
                                    <label for="checkboxPrimary">Wap Master</label>
                                </div>
                                <div class="ckbox ckbox-warning">
                                    <input type="checkbox" name="type[]" id="checkboxWarning">
                                    <label for="checkboxWarning">App Master</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="ckbox ckbox-success">
                                    <input type="checkbox" name="type[]" id="checkboxSuccess">
                                    <label for="checkboxSuccess">Phone Shop</label>
                                </div>
                                <div class="ckbox ckbox-danger">
                                    <input type="checkbox" name="type[]" id="checkboxDanger">
                                    <label for="checkboxDanger">Adnetwork</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Danh sách cài App</label>
                            <div class="col-sm-9">
                                <input name="tags" id="tags" class="form-control" value="foo,bar,baz" />
                            </div>
                        </div>
                    </div>
                    <div class="panel-body clearfix text-center">
                        <button class="btn btn-primary">Lọc thành viên</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-hidaction table-hover mb30">
                <thead>
                <tr>
                    <th>STT</th>
                    <th class="text-center">User Name</th>
                    <th class="text-center">Full Name</th>
                    <th class="text-center">Nhóm</th>
                    <th class="text-center">Telephone</th>
                    <th class="text-center">ACTION</th>
                </tr>
                </thead>
                <tbody>
                <?php if($this->data) foreach($this->data as $key=>$item): ?>
                    <tr data-id="<?php echo $item->user_id; ?>">
                        <td><?php echo $key; ?></td>
                        <td><?php echo $item->user_name; ?></td>
                        <td><?php echo $item->full_name; ?></td>
                        <td class="text-center">
                            <select disabled class="form-control">
                                <?php echo $this->getUserGroup($item->group_id); ?>
                            </select>
                        </td>
                        <td><?php echo $item->tel; ?></td>
                        <td>
                            <a href="/user-edit/<?php echo $item->user_id; ?>" data-toggle="tooltip" title="Edit" class="tooltips"><i class="fa fa-pencil"></i></a>
                            <?php if($item->user_id != 1): ?><a href="javascript:;" data-toggle="tooltip" title="Delete" class="delete-row tooltips btnDelete"><i class="fa fa-trash-o"></i></a><?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        // Select2
        jQuery("#select-basic, #select-multi").select2();
        jQuery('#select-search-hide').select2({
            minimumResultsForSearch: -1
        });
        // Tags Input
        jQuery('#tags').tagsInput({width:'auto'});
        $('.btnTrash').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa tạm bản ghi này không ?") == true){
                $.ajax({
                    url: '/user/actTrash',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
        $('.btnUnTrash').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn khôi phục bản ghi này không ?") == true){
                $.ajax({
                    url: '/user/actUnTrash',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
        $('.btnDelete').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa bản ghi này khỏi hệ thống không ?") == true){
                $.ajax({
                    url: '/user/actDelete',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
    })
</script>