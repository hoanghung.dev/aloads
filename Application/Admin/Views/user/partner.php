<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-users"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý Partner</h4>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>STT</th>
                    <th class="text-center">User Name</th>
                    <th class="text-center">Full Name</th>
                    <th class="text-center">Nhóm</th>
                    <th class="text-center">Telephone</th>
                    <th class="text-center">Trạng thái</th>
                    <th class="text-center">ACTION</th>
                </tr>
                </thead>
                <tbody>
                <?php if($this->data) foreach($this->data as $key=>$item):
                    $status = '';
                    if($item->status == 1){
                        $status = 'Kích hoạt';
                    }else{
                        $status = 'Chưa kích hoạt';
                    }
                    ?>
                    <tr data-id="<?php echo $item->user_id; ?>">
                        <td><?php echo $key; ?></td>
                        <td><?php echo $item->user_name; ?></td>
                        <td><?php echo $item->full_name; ?></td>
                        <td class="text-center">
                            <select disabled class="form-control">
                                <?php echo $this->getUserGroup($item->group_id); ?>
                            </select>
                        </td>
                        <td><?php echo $item->tel; ?></td>
                        <td><?php echo $status; ?></td>
                        <td>
                            <a href="/user-edit/<?php echo $item->user_id; ?>" data-toggle="tooltip" title="Edit" class="tooltips"><i class="fa fa-pencil"></i></a>
                            <?php if($item->user_id != 1): ?><a href="javascript:;" data-toggle="tooltip" title="Delete" class="delete-row tooltips btnDelete"><i class="fa fa-trash-o"></i></a><?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        // Select2
        jQuery("#select-basic, #select-multi").select2();
        jQuery('#select-search-hide').select2({
            minimumResultsForSearch: -1
        });
        // Tags Input
        jQuery('#tags').tagsInput({width:'auto'});
        $('.btnTrash').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa tạm bản ghi này không ?") == true){
                $.ajax({
                    url: '/user/actTrash',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
        $('.btnUnTrash').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn khôi phục bản ghi này không ?") == true){
                $.ajax({
                    url: '/user/actUnTrash',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
        $('.btnDelete').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa bản ghi này khỏi hệ thống không ?") == true){
                $.ajax({
                    url: '/user/actDelete',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
    })
</script>