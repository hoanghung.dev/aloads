<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-users"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý thành viên</h4>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Thêm mới thành viên
            </div>
            <div class="panel-body">
                <div class="row">
                        <form id="myForm" class="form-horizontal form-bordered" data-toggle="validator" role="form" action="/user-add" method="POST">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">User Name</label>
                                    <div class="col-sm-8">
                                        <input name="user_name"  pattern="^([_A-z0-9]){3,}$" maxlength="20" data-error="Tối đa 20 ký tự bao gồm chữ số và chữ cái" class="form-control" placeholder="User Name" required>
                                        <span class="help-block with-errors"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-4 control-label">Password</label>
                                    <div class="col-sm-4">
                                        <input type="password" name="password" data-minlength="6" class="form-control" id="inputPassword" data-error="Tối thiểu 6 ký tự nhé !" placeholder="Password" required>
                                        <span class="help-block with-errors"></span>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="password" class="form-control" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Mật khẩu không trùng nhau" placeholder="Confirm" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Full Name</label>
                                    <div class="col-sm-8">
                                        <input name="full_name" type="text" class="form-control" placeholder="Full Name" required>
                                        <span class="help-block with-errors"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Số điện thoại</label>
                                    <div class="col-sm-8">
                                        <input name="tel" type="tel" class="form-control" placeholder="Số điện thoại" required>
                                        <span class="help-block with-errors"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-8">
                                        <input name="email" type="email" class="form-control" id="inputEmail" placeholder="Email" data-error="Định dạng email không đúng !" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Trạng thái</label>
                                    <div class="col-sm-8 control-label">
                                        <select name="status" class="width300">
                                            <option value="0" selected>Chưa kích hoạt</option>
                                            <option value="1">Kích hoạt</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Skype</label>
                                    <div class="col-sm-8">
                                        <input name="skype" type="text" class="form-control" placeholder="Skype">
                                        <span class="help-block with-errors"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Website</label>
                                    <div class="col-sm-8">
                                        <input name="website" type="text" class="form-control" placeholder="Website">
                                        <span class="help-block with-errors"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">VAT</label>
                                    <div class="col-sm-8">
                                        <div class="">
                                            <input type="radio" name="is_vat" value="0" checked>
                                            <label>Không VAT</label>
                                        </div>
                                        <div class="">
                                            <input type="radio" name="is_vat" value="1">
                                            <label>Có VAT</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Nhóm</label>
                                    <div class="col-sm-8">
                                        <select name="group_id" class="form-control">
                                            <?php echo $this->getUserGroup(); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Giới thiệu bởi</label>
                                    <select id="select-user" name="referral_id" data-placeholder="Giới thiệu bởi" class="col-sm-8">
                                        <option value="0">Chọn người giới thiệu</option>
                                        <?php $listAllUser = $this->getListUser(); if(!empty($listAllUser)) foreach($listAllUser as $item): ?>
                                            <option value="<?php echo $item->user_id; ?>"><?php echo $item->full_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Loại user</label>
                                    <div class="col-sm-4">
                                        <div class="rdio rdio-default">
                                            <input type="radio" name="type" value="0" id="radioDefault">
                                            <label for="radioDefault">Chuẩn</label>
                                        </div>
                                        <div class="rdio rdio-primary">
                                            <input type="radio" name="type" value="1" id="radioPrimary">
                                            <label for="radioPrimary">Wap Master</label>
                                        </div>
                                        <div class="rdio rdio-warning">
                                            <input type="radio" name="type" value="2" id="radioWarning">
                                            <label for="radioWarning">App Master</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="rdio rdio-success">
                                            <input type="radio" name="type" value="3" id="radioSuccess">
                                            <label for="radioSuccess">Phone Shop</label>
                                        </div>
                                        <div class="rdio rdio-danger">
                                            <input type="radio" name="type" value="4" id="radioDanger">
                                            <label for="radioDanger">Adnetwork</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center clearfix">
                                <button type="submit" class="btn btn-primary">Thêm mới</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>

                        </form>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>

