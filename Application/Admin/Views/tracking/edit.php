<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-th-large"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý Tracking</h4>
        </div>
    </div>
</div>
<?php if($this->data): $oneItem = $this->data; ?>
<div class="contentpanel">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Sửa tracking
            </div>

            <div class="panel-body">
                <div class="row">
                    <form id="myForm" class="form-horizontal form-bordered" data-toggle="validator"  role="form" action="" method="POST">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Link: </label>
                            <div class="col-sm-8 control-label">
                                <input type="text" class="form-control" value="<?php echo _ROOT_TRACKING.$oneItem->link;  ?>" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Trạng thái</label>
                            <div class="col-sm-8 control-label">
                                <select name="status" class="width300">
                                    <option value="0" <?php if($oneItem->status == 0) echo "selected"; ?>>Không hoạt động</option>
                                    <option value="1" <?php if($oneItem->status == 1) echo "selected"; ?>>Đang hoạt động</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group text-center clearfix">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>
<script>
    /*$("input[name='app_title']").keyup(function(){
     var keywords = $(this).val();
     $('ul.list-result').removeClass('hide');
     $.ajax({
     url: '/list-app/'+keywords,
     success: function(response){
     $('.list-result').html(response);
     }
     })
     });
     $(document).on('click','ul.list-result li',function (event) {
     var id = $(this).attr('data-id');
     var title = $(this).text();
     $('input[name="app_title"]').val(title);
     $('input[name="app_id"]').val(id);
     $('ul.list-result').addClass('hide');
     });*/
</script>
<?php endif; ?>