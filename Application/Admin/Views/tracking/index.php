<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-bullhorn"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý Tracking (Tổng: <?php echo isset($this->data['total'])?$this->data['total']:0; ?>)</h4>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-btns" style="display: none;">
                <a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
                <a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <form class="form-inline" method="GET">
                <div class="form-group">
                    <label class="control-label">Chiến dịch: </label>
                    <select id="select-app" name="campaign_id" data-placeholder="Chọn chiến dịch" class="width300">
                        <option value="">Chọn chiến dịch</option>
                        <?php $listAllCamp = $this->getListCampaign(); if(!empty($listAllCamp)) foreach($listAllCamp as $item): ?>
                            <option value="<?php echo $item->campaign_id; ?>" <?php if(isset($_GET['campaign_id']) && $_GET['campaign_id'] == $item->campaign_id) echo "selected"; ?>><?php echo $item->title; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Đối tác chạy: </label>
                    <select id="select-user" name="user_id" data-placeholder="Chọn đối tác" class="width300">
                        <option value="">Chọn đối tác</option>
                        <?php $listAllUser = $this->getListUser(); if(!empty($listAllUser)) foreach($listAllUser as $item): ?>
                            <option value="<?php echo $item->user_id; ?>" <?php if(isset($_GET['user_id']) && $_GET['user_id'] == $item->user_id) echo "selected"; ?>><?php echo $item->full_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary mr5">Lọc</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>STT</th>
                    <th class="text-center">Link</th>
                    <th class="text-center">Đối tác</th>
                    <th class="text-center">Click</th>
                    <th class="text-center">Install</th>
                    <th class="text-center">Giá</th>
                    <th class="text-center">Tổng tiền</th>
                    <th class="text-center">Trạng thái</th>
                    <th class="text-center">ACTION</th>
                </tr>
                </thead>
                <tbody>
                <?php if($this->data) foreach($this->data as $key=>$item):
                    $oneCampaign = $this->getCampaign($item->campaign_id);
                    $total_click = $this->getTrackingClick($item->tracking_id);
                    $total_install = $this->getTrackingInstall($item->tracking_id);
                    //$percent_complete = ($total_install/$item->quantity_install)*100;

                    $status = '';
                    if($item->status == 1){
                        $status = 'Đang hoạt động';
                    }else{
                        $status = 'Dừng hoạt động';
                    }

                    ?>
                    <tr data-id="<?php echo $item->tracking_id; ?>">
                        <td><?php echo $key; ?></td>
                        <td><input type="text" id="link" value="<?php echo _ROOT_TRACKING.$item->link; ?>" onClick="this.select();"/></td>
                        <td class="text-center">
                            <?php echo $this->getUser($item->user_id); ?>
                        </td>
                        <td><?php echo $total_click; ?></td>
                        <td><?php echo $total_install; ?></td>
                        <td><?php echo money_format('%i',$oneCampaign->price_out); ?></td>
                        <td><?php echo money_format('%i',$oneCampaign->price_out*$total_install); ?></td>
                        <td>
                            <?php echo $status; ?>
                        </td>
                        <td>
                            <a href="/tracking-edit/<?php echo $item->tracking_id; ?>" data-toggle="tooltip" title="Edit" class="tooltips"><i class="fa fa-pencil"></i></a>
                            <a href="javascript:;" data-toggle="tooltip" title="Delete" class="delete-row tooltips btnDelete"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){

        /*jQuery('.toggle').toggles({
            <?php if($item->status == 1): ?>
            on: true
            <?php endif; ?>
        });
        $('.toggle').on('toggle', function (e, active) {
            var id = $(this).parents().find('tr').data('tracking_id');
            alert(id);
            *//*if (active) {
             alert('Toggle is now ON!');
             } else {
             alert('Toggle is now OFF!');
             }*//*
        });
*/
        $('.btnDelete').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa bản ghi này khỏi hệ thống không ?") == true){
                $.ajax({
                    url: '/tracking/actDelete',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
    })
</script>