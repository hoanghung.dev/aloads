<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-th-large"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý chiến dịch</h4>
        </div>
    </div>
</div>
<div class="contentpanel">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Thêm mới chiến dịch
            </div>
            <div class="panel-body">
                <div class="row">
                    <form id="myForm" class="form-horizontal form-bordered" data-toggle="validator"  role="form" action="" method="POST">

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Chiến dịch: </label>
                            <div class="col-sm-8 control-label">
                                <select id="select-app" name="campaign_id" data-placeholder="Chọn chiến dịch" class="width300">
                                    <option value="">Chọn chiến dịch</option>
                                    <?php $listAllCamp = $this->getListCampaign(); if(!empty($listAllCamp)) foreach($listAllCamp as $item): ?>
                                        <option value="<?php echo $item->campaign_id; ?>"><?php echo $item->title; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs nav-danger">
                                    <li class="active"><a href="#tab_1" data-toggle="tab"><strong>Thêm link chính</strong></a></li>
                                    <li><a href="#tab_2" data-toggle="tab"><strong>Thêm link con</strong></a></li>
                                </ul>

                                <div class="tab-content tab-content-danger mb30">
                                    <div id="tab_1" class="form-group tab-pane active">
                                        <label class="col-sm-4 control-label">Đối tác chạy: </label>
                                        <div class="col-sm-8 control-label">
                                            <select id="select-user" name="user_id" data-placeholder="Chọn đối tác" class="width300">
                                                <option value="0">Chọn đối tác</option>
                                                <?php $listAllUser = $this->getListUser(); if(!empty($listAllUser)) foreach($listAllUser as $item): ?>
                                                    <option value="<?php echo $item->user_id; ?>"><?php echo $item->full_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div id="tab_2" class="form-group tab-pane ">
                                        <label class="col-sm-4 control-label">Đối tác chạy: </label>
                                        <div class="col-sm-8 control-label">
                                            <select id="select-user-parent" name="parent_id" data-placeholder="Chọn đối tác" class="width300">
                                                <option value="0">Chọn đối tác chính</option>
                                                <?php $listAllUser = $this->getListUser(); if(!empty($listAllUser)) foreach($listAllUser as $item): ?>
                                                    <option value="<?php echo $item->user_id; ?>"><?php echo $item->full_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Trạng thái</label>
                            <div class="col-sm-8 control-label">
                                <div class="toggle toggle-primary"></div>
                            </div>
                        </div>
                        <div class="form-group text-center clearfix">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>
<script>
    /*$("input[name='app_title']").keyup(function(){
        var keywords = $(this).val();
        $('ul.list-result').removeClass('hide');
        $.ajax({
            url: '/list-app/'+keywords,
            success: function(response){
                $('.list-result').html(response);
            }
        })
    });
    $(document).on('click','ul.list-result li',function (event) {
        var id = $(this).attr('data-id');
        var title = $(this).text();
        $('input[name="app_title"]').val(title);
        $('input[name="app_id"]').val(id);
        $('ul.list-result').addClass('hide');
    });*/
</script>