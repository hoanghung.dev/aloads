<div class="leftpanel">
    <div class="media profile-left">
        <a class="pull-left profile-thumb" href="/user/profile">
            <img class="img-circle" src="/images/anonymous.png" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">Administrator</h4>
            <small class="text-muted">Quản lý cấp cao</small>
        </div>
    </div>
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="/"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
        <!--<li class="permission parent">
            <a href="/permission"><i class="fa fa-sitemap"></i> <span>Quản lý phân quyền</span></a>
            <ul class="children">
                <li class="permission"><a href="/permission">Danh sách Module</a></li>
                <li class="permission-roles"><a href="/permission-roles">Danh sách Roles</a></li>
                <li class="permission-add-roles"><a href="/permission-add-roles">Thêm mới Roles</a></li>
                <li class="permission-detail"><a href="/permission-detail">Phân quyền chi tiết</a></li>
            </ul>
        </li>-->
        <!--<li class="user parent">
            <a href="/user"><i class="fa fa-users"></i> <span>Quản lý thành viên</span></a>
            <ul class="children">
                <li class="user"><a href="/user">Danh sách thành viên</a></li>
                <li class="user-add"><a href="/user-add">Thêm mới thành viên</a></li>
                <li class="user-customer"><a href="/user-customer">Danh sách Customer</a></li>
                <li class="user-partner"><a href="/user-partner">Danh sách Partner</a></li>
                <li class="user-add-maps"><a href="/user-add-maps">Địa chỉ Partner Google map</a></li>
                <li class="user-referral"><a href="/user-referral">Phần trăm hoa hồng giới thiệu</a></li>
            </ul>
        </li>-->
        <li class="member parent active">
            <a href="/member"><i class="fa fa-mobile-phone"></i> <span>Quản lý User</span></a>
            <ul class="children">
                <li class="member"><a href="/member">Danh sách User</a></li>
                <li class="member-add"><a href="/member-add">Thêm mới User</a></li>
            </ul>
        </li>
        <li class="app parent active"><a href="/app"><i class="fa fa-th-large"></i> <span>Config app</span></a>
            <ul class="children">
                <li class="app"><a href="/app">Danh sách App</a></li>
                <li class="app-add"><a href="/app-add">Thêm mới App</a></li>
                <!--<li class="app-embed"><a href="/app-embed">Traffic mã nhúng</a></li>-->
            </ul>
        </li>
        <li class="campaign parent active"><a href="/campaign"><i class="fa fa-bullhorn"></i> <span>Quản lý Campaign</span></a>
            <ul class="children">
                <li class="campaign"><a href="/campaign">Danh sách Campaign</a></li>
                <li class="campaign-add"><a href="/campaign-add">Thêm Campaign</a></li>
            </ul>
        </li>
        <li class="imei parent active">
            <a href="/imei"><i class="fa fa-mobile-phone"></i> <span>Blacklist Imei</span></a>
            <ul class="children">
                <li class="imei"><a href="/imei">Danh sách</a></li>
                <li class="imei-add"><a href="/imei-add">Thêm mới</a></li>
            </ul>
        </li>
        <!--<li class="tracking parent"><a href="/tracking"><i class="glyphicon glyphicon-filter"></i> <span>Quản lý Tracking</span></a>
            <ul class="children">
                <li class="tracking"><a href="/tracking">Danh sách Tracking</a></li>
                <li class="tracking-add"><a href="/tracking-add">Thêm Tracking</a></li>
            </ul>
        </li>-->
        <!--<li class="report parent"><a href="/report"><i class="fa fa-bar-chart-o"></i> <span>Thống kê</span></a>
            <ul class="children">
                <li class="user"><a href="/report/list-click">Danh sách Click</a></li>
                <li class="user"><a href="/report/list-campaign">Thống kê mở app</a></li>
            </ul>
        </li>
        <li class="parent"><a href="/report"><i class="fa fa-money"></i> <span>Thanh toán & Đối soát</span></a>
            <ul class="children">
                <li class="user"><a href="/report/list-click">Tổng kết doanh thu</a></li>
                <li class="user"><a href="/report/list-campaign">Dữ liệu đối soát</a></li>
                <li class="user"><a href="/report/list-campaign">Danh sách thanh toán</a></li>
                <li class="user"><a href="/report/list-campaign">Lịch sử thanh toán</a></li>
                <li class="user"><a href="/report/list-campaign">Lịch sử email đối soát</a></li>
            </ul>
        </li>
        <li class="parent"><a href="/report"><i class="fa fa-filter"></i> <span>Quản lý Cheat</span></a>
            <ul class="children">
                <li class="user"><a href="/report/list-campaign">Danh sách các điều kiện lọc</a></li>
                <li class="user"><a href="/report/list-click">Tạo điều kiện lọc</a></li>
                <li class="user"><a href="/report/list-campaign">Click/Install wrap app</a></li>
                <li class="user"><a href="/report/list-campaign">Retention rate</a></li>
            </ul>
        </li>
        <li class="parent"><a href="/setting"><i class="fa fa-cogs"></i> <span>Cấu hình chung</span></a>
            <ul class="children">
                <li class="setting"><a href="/setting-email">Cấu hình email</a></li>
            </ul>
        </li>-->
    </ul>
</div>
<script type="text/javascript">
    jQuery('ul.nav > li.<?php echo $this->params['router']['controller'];?>').addClass('active');
    jQuery('li.<?php echo str_replace('/','',parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)); ?>').addClass('active');
</script>