<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-home"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Dashboard</h4>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row row-stat">
                <div class="col-md-6">
                    <?php echo $this->action('listAppInstall','index','admin'); ?>
                </div>

                <div class="col-md-6">
                    <?php echo $this->action('clickInstallChart','index','admin'); ?>
                </div>
            </div>

            <div class="row row-stat">
                <div class="col-md-12">
                    <h5 class="lg-title mb10">Doanh thu/Chi phí</h5>
                    <div id="barchart" class="flotGraph"></div>
                </div>
            </div><!-- row -->
            <div class="row row-stat">
                <div class="col-md-12">
                    <h5 class="lg-title mb10">KPI</h5>
                    <div id="line-chart" class="height300"></div>
                </div>
            </div>
        </div>
    </div><!-- panel -->
</div>
<!--Chart-->
<script src="/js/flot/jquery.flot.min.js"></script>
<script src="/js/flot/jquery.flot.resize.min.js"></script>
<script src="/js/flot/jquery.flot.symbol.min.js"></script>
<script src="/js/flot/jquery.flot.crosshair.min.js"></script>
<script src="/js/flot/jquery.flot.categories.min.js"></script>
<script src="/js/flot/jquery.flot.pie.min.js"></script>
<script src="/js/morris.min.js"></script>
<script src="/js/raphael-2.1.0.min.js"></script>
<script src="/js/jquery.sparkline.min.js"></script>
<script src="/js/charts.js"></script>