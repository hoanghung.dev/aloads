<h5 class="lg-title mb10">Thống kê Click/Install</h5>
<div id="piechart" class="flotGraph"></div>
<script>
    var piedata = [
        { label: "Install", data: [[1,20]], color: '#4E5154'},
        { label: "Click", data: [[1,80]], color: '#5BC0DE'}
    ];
</script>