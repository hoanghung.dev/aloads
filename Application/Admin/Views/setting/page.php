<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="trang đơn-header">Quản lý trang đơn</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">

    <div class="panel panel-default col-lg-6">
        <div class="panel-heading">
            Danh sách trang đơn
        </div>
        <?php if($this->data): ?>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th class="text-center">Vị trí</th>
                            <th class="text-center">Tiêu đề trang đơn</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        <?php foreach($this->data as $item): ?>
                            <tr data-id="<?php echo $item->setting_id; ?>">
                                <td><?php echo $item->setting_id; ?></td>
                                <td><?php echo $item->position; ?></td>
                                <td><?php echo $item->title; ?></td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-success btn-circle btnShowEdit" onclick="javascript: window.location.href = '/page?id=<?php echo $item->setting_id; ?>';"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-danger btn-circle btnDelete"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>

                    </table>
                    <button type="button" class="btn btn-primary btn-xs btnShowAdd">Thêm trang đơn</button>

                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        <?php endif; ?>
    </div>
    <div id="add-page" class="panel panel-default col-lg-6 hide">
        <div class="panel-heading">
            Thêm trang mới
        </div>
        <div class="panel-body">
            <form id="myForm" data-toggle="validator"  role="form" action="/page/add" method="POST">

                <div class="form-group">
                    <label>Tiêu đề trang đơn</label>
                    <input type="text" name="title" class="form-control" placeholder="Tiêu đề trang đơn" required>
                    <span class="help-block with-errors"></span>
                </div>
                <div class="form-group">
                    <label>Tiêu đề trang đơn SEO</label>
                    <input type="text" name="title_page" class="form-control" placeholder="Tiêu đề trang đơn SEO" required>
                    <span class="help-block with-errors"></span>
                </div>
                <div class="form-group">
                    <label>Mô tả (Description)</label>
                    <textarea name="intro" class="form-control" rows="3" required></textarea>
                    <span class="help-block with-errors"></span>
                </div>
                <div class="form-group">
                    <label>Keywords</label>
                    <input name="keywords" class="form-control" required>
                    <span class="help-block with-errors"></span>
                </div>
                <div class="form-group">
                    <label>Content</label>
                    <textarea name="content" class="form-control tinymce" rows="10"></textarea>
                </div>
                <button type="submit" class="btn btn-default">Thêm mới</button>
                <button type="reset" class="btn btn-default">Reset</button>
            </form>

        </div>
    </div>
    <?php
    if($this->item):

    $items = $this->item;
    ?>
    <div id="edit-page" class="panel panel-default col-lg-6">
        <div class="panel-heading">
            Sửa trang
        </div>
        <div class="panel-body">
            <form id="myForm" data-toggle="validator"  role="form" action="/page/edit" method="POST">

                <div class="form-group">
                    <label>Tiêu đề trang đơn</label>
                    <input type="text" name="title" class="form-control" placeholder="Tiêu đề trang đơn" value="<?php echo $items->title; ?>" required>
                    <span class="help-block with-errors"></span>
                </div>
                <div class="form-group">
                    <label>Tiêu đề trang đơn SEO</label>
                    <input type="text" name="title_page" class="form-control" placeholder="Tiêu đề trang đơn SEO" value="<?php echo $items->title_page; ?>" required>
                    <span class="help-block with-errors"></span>
                </div>
                <div class="form-group">
                    <label>Mô tả (Description)</label>
                    <textarea name="intro" class="form-control" rows="3" required><?php echo $items->intro; ?></textarea>
                    <span class="help-block with-errors"></span>
                </div>
                <div class="form-group">
                    <label>Keywords</label>
                    <input name="keywords" class="form-control" value="<?php echo $items->keywords; ?>" required>
                    <span class="help-block with-errors"></span>
                </div>
                <div class="form-group">
                    <label>Vị trí</label>
                    <input name="position" class="form-control" value="<?php echo $items->position; ?>" required>
                    <span class="help-block with-errors"></span>
                </div>
                <div class="form-group">
                    <label>Content</label>
                    <textarea name="content" class="form-control tinymce" rows="10"><?php echo $items->content; ?></textarea>
                </div>
                <input type="hidden" name="setting_id" value="<?php echo $items->setting_id; ?>">
                <button type="submit" class="btn btn-default">Cập nhật</button>
                <button type="reset" class="btn btn-default">Reset</button>
            </form>

        </div>
    </div>
    <?php endif; ?>
</div>

<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea.tinymce",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });
    $(document).ready(function(){
        $('.btnShowAdd').click(function(){
            $('#add-page').removeClass('hide');
            $('#edit-page').addClass('hide');
        })
        $('.btnShowEdit').click(function(){
            $('#edit-page').removeClass('hide');
            $('#add-page').addClass('hide');
        })
        $('.btnInsert').click(function(){
            var formData = new FormData();
            formData.append('id',$(this).parents('tr').attr('data-id'));
            formData.append('position',$(this).parents('tr').find('input[name="position"]').val());
            formData.append('image',$(this).parents('tr').find('img.previewing').attr('src'));
            formData.append('title',$(this).parents('tr').find('input[name="title"]').val());

            if(confirm("Bạn có chắc chắn cập nhật Logo này không ?") == true){
                var xhr = new XMLHttpRequest();
                xhr.open("POST", '/setting/actUpdateLogo');
                xhr.send(formData);
                alert('Cập nhật thành công !');
                location.reload();
            }

        })
        $('.btnUpdate').click(function(){
            var formData = new FormData();
            formData.append('id',$(this).parents('tr').attr('data-id'));
            formData.append('position',$(this).parents('tr').find('input[name="position"]').val());
            formData.append('image',$(this).parents('tr').find('img.previewing').attr('src'));
            formData.append('title',$(this).parents('tr').find('input[name="title"]').val());


            if(confirm("Bạn có chắc chắn cập nhật Logo này không ?") == true){
                var xhr = new XMLHttpRequest();
                xhr.open("POST", '/setting/actUpdateLogo');
                xhr.send(formData);
                alert('Cập nhật thành công !');
                location.reload();
            }

        })
        $('.btnDelete').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa bản ghi này khỏi hệ thống không ?") == true){
                $.ajax({
                    url: '/page/actDelete',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })


    })
</script>
