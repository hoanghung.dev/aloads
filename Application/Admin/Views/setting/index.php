<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
$item = $this->data;
?>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cấu hình Website</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
<?php if (!empty($item)): ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="myForm" data-toggle="validator" role="form"
                                  action="/config" method="POST"
                                  enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Tên Website</label>
                                    <input type="text" name="title" class="form-control" placeholder="Tên website"
                                           required value="<?php echo $item->title; ?>">
                                    <span class="help-block with-errors"></span>
                                </div>
                                <div class="form-group">
                                    <label>Tiêu đề SEO</label>
                                    <input type="text" name="title_page" class="form-control" data-maxlength="110"
                                           placeholder="Tiêu đề SEO" required value="<?php echo $item->title_page; ?>">
                                    <span class="help-block with-errors"></span>
                                </div>
                                <div class="form-group">
                                    <label>Mô tả SEO</label>
                                    <textarea name="intro" class="form-control" rows="3" required><?php echo $item->intro; ?></textarea>
                                    <span class="help-block with-errors"></span>
                                </div>

                                <div class="form-group">
                                    <label>Từ khóa SEO</label>
                                    <input type="text" name="keywords" class="form-control"
                                           placeholder="Keyword 1, keyword 2,keyword 3" value="<?php echo $item->keywords; ?>" required>
                                    <span class="help-block with-errors"></span>
                                </div>

                                <div class="form-group">
                                    <label>Địa chỉ liên hệ</label>
                                    <input type="text" name="address" class="form-control"
                                           placeholder="Địa chỉ liên hệ" value="<?php echo $item->address; ?>" required>
                                    <span class="help-block with-errors"></span>
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" class="form-control"
                                           placeholder="Email" value="<?php echo $item->email; ?>" required>
                                    <span class="help-block with-errors"></span>
                                </div>
                                <div class="form-group">
                                    <label>Điện thoại</label>
                                    <input type="tel" name="telephone" class="form-control"
                                           placeholder="Điện thoại liên hệ" value="<?php echo $item->telephone; ?>" required>
                                    <span class="help-block with-errors"></span>
                                </div>

                                <div class="form-group">
                                    <label>Javascript Head</label>
                                    <textarea name="script" class="form-control" rows="5"><?php echo $item->script; ?></textarea>
                                    <span class="help-block with-errors"></span>
                                </div>

                                <button type="submit" class="btn btn-default">Update</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </form>

                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>


<?php endif; ?>