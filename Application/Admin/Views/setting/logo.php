<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Quản lý logo</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            Danh sách logo
        </div>
        <?php if($this->data): ?>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th class="text-center">Vị trí</th>
                            <th class="text-center">Ảnh</th>
                            <th class="text-center">Tiêu đề logo</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        <?php foreach($this->data as $item): ?>
                            <tr data-id="<?php echo $item->setting_id; ?>">
                                <td><?php echo $item->setting_id; ?></td>
                                <td><input name="position" type="text" value="<?php echo $item->position; ?>"></td>
                                <td>
                                    <img class="previewing" onerror="imgError(this);" src="/images/<?php echo $item->url; ?>" style="width: 80px;"/>
                                    <div class="fileUpload btn btn-primary">
                                        <i class="fa fa-file-image-o fa-w"></i> Chọn ảnh
                                        <input type="file" class="upload btnUploadImage" />
                                    </div>
                                </td>
                                <td><input name="title" type="text" value="<?php echo $item->title; ?>"></td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-success btn-circle btnUpdate"><i class="fa fa-repeat"></i></button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        <?php endif; ?>
    </div>

</div>
<script type="text/javascript">
    function readImage(input) {
        if ( input.files && input.files[0] ) {
            var FR= new FileReader();
            FR.onload = function(e) {
                $(input).parents('tr').find('img.previewing').attr('src', e.target.result);
            };
            FR.readAsDataURL( input.files[0] );
        }
    }
    $(document).ready(function(){
        $('.btnUploadImage').change(function(){
            readImage( this );
        })

        $('.btnUpdate').click(function(){
            var formData = new FormData();
            formData.append('id',$(this).parents('tr').attr('data-id'));
            formData.append('position',$(this).parents('tr').find('input[name="position"]').val());
            formData.append('image',$(this).parents('tr').find('img.previewing').attr('src'));
            formData.append('title',$(this).parents('tr').find('input[name="title"]').val());


            if(confirm("Bạn có chắc chắn cập nhật Logo này không ?") == true){
                var xhr = new XMLHttpRequest();
                xhr.open("POST", '/setting/actUpdateLogo');
                xhr.send(formData);
                alert('Cập nhật thành công !');
                location.reload();
            }

        })


    })
</script>