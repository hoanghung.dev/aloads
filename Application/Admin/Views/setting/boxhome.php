<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Quản lý Box</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            Danh sách Box
            <button type="button" class="btn btn-primary showInsert"> Thêm Box</button>
            <span class="ajax-loader hidden"><img src="../images/ajax-loader-3.gif"> </span>
        </div>
        <?php if($this->data): ?>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th class="text-center">Mã box</th>
                            <th class="text-center">Tiêu đề Box</th>
                            <th class="text-center">Thứ tự</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        <?php foreach($this->data as $item): ?>
                            <tr data-id="<?php echo $item->setting_id; ?>">
                                <td><?php echo $item->setting_id; ?></td>
                                <td><input name="meta_des" type="text" value="<?php echo $item->meta_des; ?>"></td>
                                <td><input name="title" type="text" value="<?php echo $item->title; ?>"></td>
                                <td><input name="position" type="text" value="<?php echo $item->position; ?>"></td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-success btn-circle btnUpdate"><i class="fa fa-repeat"></i></button>
                                    <button type="button" class="btn btn-danger btn-circle btnDelete"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <tr class="add-setting hidden">
                            <td>..</td>
                            <td><input name="meta_des" type="text"></td>
                            <td><input name="title" type="text"></td>
                            <td><input name="position" type="text"></td>
                            <input type="hidden" name="type" value="4">
                            <td class="text-center">
                                <button type="button" class="btn btn-success btn-circle btnInsert"><i class="fa fa-plus-circle"></i></button>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        <?php endif; ?>
    </div>

</div>
<script type="text/javascript">

    $(document).ready(function(){
        $('.showInsert').click(function(){
            $('.add-setting').toggleClass('hidden');
        });
        $('.btnUpdate').click(function(){
            var formData = new FormData();
            formData.append('id',$(this).parents('tr').attr('data-id'));
            formData.append('position',$(this).parents('tr').find('input[name="position"]').val());
            formData.append('meta_des',$(this).parents('tr').find('input[name="meta_des"]').val());
            formData.append('title',$(this).parents('tr').find('input[name="title"]').val());


            if(confirm("Bạn có chắc chắn cập nhật Box này không ?") == true){
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function() {
                    if (xhr.readyState == 4) {
                        alert(xhr.responseText);
                        location.reload();
                    }
                };
                xhr.open("POST", '/setting/actUpdate',true);
                xhr.send(formData);
               // location.reload();
            }

        });
        $('.btnInsert').click(function(){
            var formData = new FormData();
            formData.append('position',$(this).parents('tr').find('input[name="position"]').val());
            formData.append('meta_des',$(this).parents('tr').find('input[name="meta_des"]').val());
            formData.append('title',$(this).parents('tr').find('input[name="title"]').val());
            formData.append('type',$(this).parents('tr').find('input[name="type"]').val());
            if(confirm("Bạn có chắc chắn thêm Box này không ?") == true){
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function() {
                    if (xhr.readyState == 4) {
                        alert(xhr.responseText);
                        location.reload();
                    }
                };
                xhr.open("POST", '/setting/actInsert',true);
                xhr.send(formData);
                // location.reload();
            }
        });
        $('.btnDelete').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa slider này khỏi hệ thống không ?") == true){
                $.ajax({
                    url: '/setting/actDelete',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })


    })
</script>