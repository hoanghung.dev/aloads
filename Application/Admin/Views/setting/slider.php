<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Quản lý slider</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            Danh sách slider
            <button type="button" class="btn btn-primary showInsert"> Thêm Slider</button>
            <span class="ajax-loader hidden"><img src="../images/ajax-loader-3.gif"> </span>
        </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th class="text-center">Vị trí</th>
                            <th class="text-center">Ảnh</th>
                            <th class="text-center">Tiêu đề slider</th>
                            <th class="text-center">Link</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        <?php if($this->data) foreach($this->data as $item): ?>
                            <tr data-id="<?php echo $item->setting_id; ?>">
                                <td><?php echo $item->setting_id; ?></td>
                                <td><input name="position" type="text" value="<?php echo $item->position; ?>"></td>
                                <td>
                                    <img class="previewing" onerror="imgError(this);" src="<?php echo _ROOT_IMAGES.'/'.$item->image; ?>" style="width: 80px;"/>
                                    <div class="fileUpload btn btn-primary">
                                        <i class="fa fa-file-image-o fa-w"></i> Chọn ảnh
                                        <input type="file" class="upload btnUploadImage" />
                                    </div>
                                </td>
                                <td><input name="title" type="text" value="<?php echo $item->title; ?>"></td>
                                <td><input name="link" type="text" value="<?php echo $item->link; ?>"></td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-success btn-circle btnUpdate"><i class="fa fa-repeat"></i></button>
                                    <button type="button" class="btn btn-danger btn-circle btnDelete"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                            <tr class="add-slider hidden">
                                <td>..</td>
                                <td><input name="position" type="text"></td>
                                <td>
                                    <img class="previewing" onerror="imgError(this);" style="width: 80px;"/>
                                    <div class="fileUpload btn btn-primary">
                                        <i class="fa fa-file-image-o fa-w"></i> Chọn ảnh
                                        <input type="file" class="upload btnUploadImage" />
                                    </div>
                                </td>
                                <td><input name="title" type="text"></td>
                                <td><input name="link" type="text"></td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-success btn-circle btnInsert"><i class="fa fa-plus-circle"></i></button>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->

    </div>

</div>
<script type="text/javascript">
    $('.showInsert').click(function(){
        $('.add-slider').toggleClass('hidden');
    });
    function readImage(input) {
        if ( input.files && input.files[0] ) {
            var FR= new FileReader();
            FR.onload = function(e) {
                $(input).parents('tr').find('img.previewing').attr('src', e.target.result);
            };
            FR.readAsDataURL( input.files[0] );
        }
    }
    $(document).ready(function(){
        $('.btnUploadImage').change(function(){
            readImage( this );
        });

        $('.btnUpdate').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            var position = $(this).parents('tr').find('input[name="position"]').val();
            var image = $(this).parents('tr').find('img.previewing').attr('src');
            var title = $(this).parents('tr').find('input[name="title"]').val();
            var link = $(this).parents('tr').find('input[name="link"]').val();


            if(confirm("Bạn có chắc chắn sửa Slider này không ?") == true){
                //alert('Thêm mới thành công !');
                //location.reload();
                $.ajax({
                    type: "POST",
                    url: "/setting/actUpdateSlider",
                    data: {id:id,position:position,image:image,title:title,link:link},
                    beforeSend: function(){
                        console.log('Đang update');
                        $('.ajax-loader').removeClass('hidden');
                    },
                    success: function(response){
                        console.log(response);
                        $('.ajax-loader').addClass('hidden');
                        if(response){
                            alert(response);
                            location.reload();
                        }
                    }
                })
            }

        });
        $('.btnInsert').click(function(){
            var position = $(this).parents('tr').find('input[name="position"]').val();
            var image = $(this).parents('tr').find('img.previewing').attr('src');
            var title = $(this).parents('tr').find('input[name="title"]').val();
            var link = $(this).parents('tr').find('input[name="link"]').val();


            if(confirm("Bạn có chắc chắn thêm Slider này không ?") == true){
                //alert('Thêm mới thành công !');
                //location.reload();
                $.ajax({
                    type: "POST",
                    url: "/setting/actInsertSlider",
                    data: {position:position,image:image,title:title,link:link},
                    beforeSend: function(){
                        console.log('Đang insert');
                        $('.ajax-loader').removeClass('hidden');
                    },
                    success: function(response){
                        console.log(response);
                        $('.ajax-loader').addClass('hidden');
                        if(response){
                            alert(response);
                            location.reload();
                        }
                    }
                })
            }

        })
        $('.btnDelete').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa slider này khỏi hệ thống không ?") == true){
                $.ajax({
                    url: '/setting/actDelete',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })

    })
</script>