<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin </title>

    <link href="css/style.default.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="signin">


<section>

    <div class="panel panel-signin">
        <div class="panel-body">
            <div class="logo text-center">
                <img src="/images/logo.png" alt="Admin " width="100%">
            </div>
            <br />
            <!--<h4 class="text-center mb5">Already a Member?</h4>
            <p class="text-center">Sign in to your account</p>-->

            <div class="mb30"><?php echo $this->flash->message();?></div>

            <form action="/login" method="post">
                <div class="input-group mb15">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="text" name="username" class="form-control" placeholder="Username">
                </div><!-- input-group -->
                <div class="input-group mb15">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" name="password" class="form-control" placeholder="Password">
                </div><!-- input-group -->

                <!--<div class="row">
                    <div class="col-md-8 col-xs-12">
                        <div class="input-group mb15">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-compressed"></i></span>
                            <input type="text" name="captcha" class="form-control" placeholder="Nhập mã bên" required>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="mb15 text-center">
                            <img src="<?php /*echo $this->_session->captcha['image_src']; */?>" alt="CAPTCHA code" style="height: 40px;">
                        </div>
                    </div>
                </div>-->

                <div class="clearfix text-center">
                    <!--<div class="pull-left">
                        <button type="submit" class="btn btn-warning" onclick="window.location.reload();">Đổi mã khác </button>
                        <div class="ckbox ckbox-primary mt10">
                            <input type="checkbox" id="rememberMe" value="1">
                            <label for="rememberMe">Remember Me</label>
                        </div>
                    </div>-->

                        <input type="hidden" name="CSRF_TOKEN" value="<?php echo md5(date('Ymd').'_steven'); ?>">
                        <button type="submit" class="btn btn-success">Đăng nhập <i class="fa fa-angle-right ml5"></i></button>

                </div>
            </form>

        </div><!-- panel-body -->
        <div class="panel-footer">
            <a href="/register" class="btn btn-primary btn-block">Bạn chưa có tài khoản ? Đăng ký ngay !</a>
        </div><!-- panel-footer -->
    </div><!-- panel -->

</section>


<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/custom.js"></script>

</body>
</html>
