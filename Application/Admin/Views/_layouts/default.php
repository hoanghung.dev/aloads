<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS | Aloads</title>

    <link href="/css/style.default.css" rel="stylesheet">
    <link href="/css/morris.css" rel="stylesheet">
    <link href="/css/select2.css" rel="stylesheet" />
    <link href="/css/jquery.tagsinput.css" rel="stylesheet" />
    <!--<link href="/css/toggles.css" rel="stylesheet" />-->
    <link href="/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="/css/colorpicker.css" rel="stylesheet" />
    <link href="/css/custom.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
    <script src="/js/jquery-1.11.1.min.js"></script>
    <script src="/plugins/tinymce/plugins/moxiemanager/js/moxman.loader.min.js"></script>

</head>

<body>
<?php echo $this->action('boxHeader','block','admin',$this->params); ?>
<section>
    <div class="mainwrapper">
        <?php echo $this->action('sidebar','block','admin',$this->params); ?>
        <div class="mainpanel">
            <?php echo $this->layoutContent; ?>
        </div>
    </div>
</section>
<?php
$moduleName = $this->params['router']['controller'];
?>
<?php echo $this->flash->message();?>
<script src="/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/validator.min.js"></script>
<script src="/js/jquery-ui-1.10.3.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src="/js/modernizr.min.js"></script>
<script src="/js/pace.min.js"></script>
<script src="/js/retina.min.js"></script>
<script src="/js/jquery.cookies.js"></script>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

<script src="/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script src="/plugins/bootstrap-validator-master/dist/validator.min.js"></script>
<script src="/js/select2.min.js"></script>

<!--form-->
<!--<script src="/js/jquery.autogrow-textarea.js"></script>
<script src="/js/jquery.mousewheel.js"></script>
<script src="/js/jquery.tagsinput.min.js"></script>
<script src="/js/toggles.min.js"></script>
<script src="/js/jquery.maskedinput.min.js"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/colorpicker.js"></script>
<script src="/js/dropzone.min.js"></script>-->

<script src="/js/custom.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.sidebar-menu li.article').addClass('active');
        jQuery('ul.sidebar-menu li.article_add').addClass('active');
        jQuery('#myForm').validator();
        jQuery('input[type="checkbox"]').bootstrapSwitch();
        /*$('input.daterange').daterangepicker();*/

        // This will empty first option in select to enable placeholder
//        jQuery('select option:first-child').text('');


        jQuery("#fileUpload").on('change', function () {

            //Get count of selected files
            var countFiles = jQuery(this)[0].files.length;

            var imgPath = jQuery(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = jQuery("#image-holder");
            image_holder.empty();

            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof (FileReader) != "undefined") {

                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++) {

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            jQuery("<img />", {
                                "src": e.target.result,
                                "class": "thumb-image",
                                "style" : "width: 200px; padding: 10px 0;"
                            }).appendTo(image_holder);
                        };

                        image_holder.show();
                        reader.readAsDataURL(jQuery(this)[0].files[i]);
                    }

                } else {
                    alert("This browser does not support FileReader.");
                }
            } else {
                alert("Vui lòng chọn file ảnh");
            }
        });

        $('.btnDelete').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa <?php echo $moduleName; ?> này khỏi hệ thống không ?") == true){
                $.ajax({
                    type: 'POST',
                    url: '/<?php echo $moduleName; ?>/actDelete',
                    data: {id:id},
                    success: function(response){
                        console.log(response);
                        if(response == true) alert('Xóa <?php echo $moduleName; ?> thành công !');
                        else alert('Xóa <?php echo $moduleName; ?> không thành công !');
                        location.reload();
                    }
                })
            }
        });
        jQuery('.btnDeleteGroup').click(function(){
            var id = jQuery(this).parents('tr').attr('data-id');

            if(confirm("Bạn có chắc chắn xóa nhóm này khỏi hệ thống không ?") == true){
                jQuery.ajax({
                    type: 'POST',
                    url: '/article/actDeleteGroup',
                    data: {id:id},
                    success: function(response){
                        console.log(response);
                        if(response == true) alert('Xóa nhóm thành công !');
                        else alert('Xóa nhóm không thành công !');
                        //location.reload();
                    }
                })
            }
        });
        jQuery('.showInsert').click(function(){
            jQuery('.add-slider').toggleClass('hidden');
        });
        jQuery('.btnUpdate').click(function(){
            var id = jQuery(this).parents('tr').attr('data-id');
            var position = jQuery(this).parents('tr').find('input[name="position"]').val();
            var image = jQuery(this).parents('tr').find('.image-holder img').attr('src');
            var title = jQuery(this).parents('tr').find('input[name="title"]').val();
            var link = jQuery(this).parents('tr').find('input[name="link"]').val();

            console.log(id+image);
            if(confirm("Bạn có chắc chắn sửa Slider này không ?") == true){
                jQuery.ajax({
                    type: "POST",
                    url: "/setting/actUpdateSlider",
                    data: {id:id,position:position,image:image,title:title,link:link},
                    beforeSend: function(){
                        console.log('Đang update');
                        jQuery('.ajax-loader').removeClass('hidden');
                    },
                    success: function(response){
                        console.log(response);
                        jQuery('.ajax-loader').addClass('hidden');
                        if(response){
                            location.reload();
                        }
                    }
                })
            }

        });
        jQuery('.btnInsert').click(function(){
            var position = jQuery(this).parents('tr').find('input[name="position"]').val();
            var image = jQuery(this).parents('tr').find('img.previewing').attr('src');
            var title = jQuery(this).parents('tr').find('input[name="title"]').val();
            var link = jQuery(this).parents('tr').find('input[name="link"]').val();


            if(confirm("Bạn có chắc chắn thêm Slider này không ?") == true){
                //alert('Thêm mới thành công !');
                //location.reload();
                jQuery.ajax({
                    type: "POST",
                    url: "/setting/actInsertSlider",
                    data: {position:position,image:image,title:title,link:link},
                    beforeSend: function(){
                        console.log('Đang insert');
                        jQuery('.ajax-loader').removeClass('hidden');
                    },
                    success: function(response){
                        console.log(response);
                        jQuery('.ajax-loader').addClass('hidden');
                        if(response){
                            alert(response);
                            location.reload();
                        }
                    }
                })
            }
        });
        jQuery('table tr th.group').each(function(){
            var role = jQuery(this).attr('data-perm');
            var allPermission=role.split('|');
            var col = jQuery(this).attr('data-col');
            //console.log(allPermission);
            jQuery('table tr td:nth-child('+col+')').each(function(){
                var permId = jQuery(this).parents('tr').attr('data-id');
                if (jQuery.inArray(permId,allPermission) > -1){
                    //console.log(permId+' Checked');
                    jQuery(this).find('input[type="checkbox"]').attr("checked","checked");
                }
            });
        });
        jQuery('input[type="checkbox"].btnCheckAll').click(function(){
            var col = jQuery(this).parents('th').attr('data-col');
            if(jQuery(this).is(":checked")) {
                jQuery('table tr td:nth-child('+col+') input').each(function(){
                    this.checked = true;
                });
            } else {
                jQuery('table tr td:nth-child('+col+') input').each(function(){
                    this.checked = false;
                });
            }

        });


        jQuery('.btnUpdatePerm').click(function(){
            console.clear();
            jQuery('table tr th.group').each(function(){
                var groupId = jQuery(this).attr('data-id');
                var col = jQuery(this).attr('data-col');
                var allPermission = '';
                jQuery('table tr td:nth-child('+col+')').each(function(){

                    var permId = jQuery(this).parents('tr').attr('data-id');
                    var checked = jQuery(this).val();
                    if (jQuery(this).find('input[type="checkbox"]').is(":checked")){
                        if(allPermission != ''){
                            allPermission += '|'+permId;
                        }else{
                            allPermission += permId;
                        }
                    }
                    jQuery(this).find('i').addClass('s25 fa fa-smile-o pull-right');
                });
                jQuery.ajax({
                    type:'POST',
                    url: '/permission/actUpdate',
                    data: {id: groupId,permission:allPermission},
                    beforeSend: function(){
                        jQuery('.ajax-loader').removeClass('hidden');
                    },
                    success: function(response) {
                        /*alert(response);
                         location.reload();
                         }*/
                        console.log(response);
                        jQuery('.ajax-loader').html('<div class="alert alert-success alert-dismissable col-lg-7 pull-right" style="margin: 0;padding: 5px;">'+response+'</div>');
                    }
                });

                console.log(allPermission);
                ///jQuery('table tr td:nth-child('+col+') i').addClass('s25 fa fa-smile-o pull-right');
            });
            /*var id = jQuery(this).parents('tr').attr('data-id');
             if(confirm("Bạn có chắc chắn xóa tạm bản ghi này không ?") == true){
             jQuery.ajax({
             url: '/permission/actTrash',
             data: {id:id},
             success: function(response){
             alert(response);
             location.reload();
             }
             })
             }*/

        });
        //setTimeout(loadTinyMce(),2000);
    });
    function previewImage(element) {
        //Get count of selected files
        var countFiles = element[0].files.length;

        var imgPath = element[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_holder = element.parent().find('.image-holder');
        image_holder.empty();

        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof (FileReader) != "undefined") {

                //loop for each file selected for uploaded.
                for (var i = 0; i < countFiles; i++) {

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        jQuery("<img />", {
                            "src": e.target.result,
                            "class": "thumb-image",
                            "style" : "width: 200px; padding: 10px 0;"
                        }).appendTo(image_holder);
                    };

                    image_holder.show();
                    reader.readAsDataURL(element[0].files[i]);
                }

            } else {
                alert("This browser does not support FileReader.");
            }
        } else {
            alert("Vui lòng chọn file ảnh");
        }
    };
    function loadTinyMce(){
        tinymce.PluginManager.load('moxiemanager', '/plugins/tinymce/plugins/moxiemanager/plugin.min.js');
        tinymce.init({
            selector: "textarea.tinymce",
            plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste textcolor colorpicker textpattern moxiemanager link image",
            ],

            toolbar1: "newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft insertfile link image",

            menubar: false,
            toolbar_items_size: 'small',

            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            document_base_url: 'http://gamehtml5.vn/upload',
            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],

            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ]
        })
    }
</script>

<!--Input type=date trong firefox-->
<!-- cdn for modernizr, if you haven't included it already -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<!-- polyfiller file to detect and load polyfills -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script>
    webshims.setOptions('waitReady', false);
    webshims.setOptions('forms-ext', {types: 'date'});
    webshims.polyfill('forms forms-ext');
</script>

</body>
</html>