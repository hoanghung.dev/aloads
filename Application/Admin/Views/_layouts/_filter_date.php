<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 03/09/2015
 * Time: 10:01 SA
 */
?>
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            Tìm kiếm
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <form role="form" method="GET" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                        <div class="form-group">
                            <div class="input-daterange input-group" id="datepicker">
                                <span class="input-group-addon">Date Begin</span>
                                <input data-date-format="dd/mm/yyyy" type="text" class="input-sm form-control" name="startDate" value="<?php echo isset($_GET['startDate'])?$_GET['startDate']:''; ?>" placeholder="Từ ngày"/>
                                <span class="input-group-addon">to</span>
                                <input data-date-format="dd/mm/yyyy" type="text" class="input-sm form-control" name="endDate" value="<?php echo isset($_GET['endDate'])?$_GET['endDate']:''; ?>" placeholder="Tới ngày"/>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        //$('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' });
        var datepicker = $('#datepicker');
        datepicker.datepicker({
            maxDate: '+0D',
            dateFormat: 'dd-mm-yy'
        });
    });
</script>