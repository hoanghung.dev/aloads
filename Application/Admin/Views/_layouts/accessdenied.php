<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS - Quản trị nội dung</title>

    <!-- Bootstrap Core CSS -->
    <link href="/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/plugins/jquery/dist/jquery-ui.css">

    <!-- MetisMenu CSS -->
    <link href="/plugins/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <!--<link href="/plugins/morrisjs/morris.css" rel="stylesheet">-->

    <!-- Custom Fonts -->
    <link href="/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="/plugins/jquery/dist/jquery.min.js"></script>
    <script src="/plugins/jquery/dist/jquery-ui.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="/plugins/metisMenu/dist/metisMenu.min.js"></script>
    <script src="/plugins/bootstrap-validator-master/dist/validator.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!--<script src="/plugins/raphael/raphael-min.js"></script>
    <script src="/plugins/morrisjs/morris.min.js"></script>
    <script src="/js/morris-data.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="/js/sb-admin-2.js"></script>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php echo $this->action('boxHeader','block','admin',$this->params); ?>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thông báo</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="alert alert-warning fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Bạn không có quyền truy cập vào trang này. Vui lòng liên hệ Ban Quản trị để được giúp đỡ !</strong>
        </div>

    </div>
    <!-- /#page-wrapper -->

</div>

</body>
</html>
