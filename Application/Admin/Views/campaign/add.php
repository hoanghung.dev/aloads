<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-th-large"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Campaign</h4>
        </div>
    </div>
</div>
<div class="contentpanel">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Add Campaign
            </div>
            <div class="panel-body">
                <div class="row">
                    <form method="get" class="form-horizontal form-bordered add-campaign">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Kiểu Campaign</label>
                            <div class="col-sm-10">
                                <select name="adType" title="Kiểu Campaign" data-placeholder="Loại campaign" class="width300" required>
                                    <option value="">Loại campaign</option>
                                    <option value="1" <?php if(isset($_GET['adType']) && $_GET['adType'] == 1) echo "selected"; ?>>Custom - Hidden</option>
                                    <option value="2" <?php if(isset($_GET['adType']) && $_GET['adType'] == 2) echo "selected"; ?>>Custom - Banner Text</option>
                                    <option value="3" <?php if(isset($_GET['adType']) && $_GET['adType'] == 3) echo "selected"; ?>>Custom - Banner Image</option>
                                    <option value="4" <?php if(isset($_GET['adType']) && $_GET['adType'] == 4) echo "selected"; ?>>Custom - Center Image</option>
                                    <option value="5" <?php if(isset($_GET['adType']) && $_GET['adType'] == 5) echo "selected"; ?>>Custom - Full Screen Image</option>
                                    <option value="6" <?php if(isset($_GET['adType']) && $_GET['adType'] == 6) echo "selected"; ?>>Admob - Banner</option>
                                    <option value="7" <?php if(isset($_GET['adType']) && $_GET['adType'] == 7) echo "selected"; ?>>Facebook - Banner</option>
                                    <option value="8" <?php if(isset($_GET['adType']) && $_GET['adType'] == 8) echo "selected"; ?>>Custom - Full Screen Text</option>
                                    <option value="9" <?php if(isset($_GET['adType']) && $_GET['adType'] == 9) echo "selected"; ?>>StartApp - Banner</option>
                                    <option value="10" <?php if(isset($_GET['adType']) && $_GET['adType'] == 10) echo "selected"; ?>>Inmobi - Banner</option>
                                    <option value="11" <?php if(isset($_GET['adType']) && $_GET['adType'] == 11) echo "selected"; ?>>Admob - FullScreen</option>
                                    <option value="12" <?php if(isset($_GET['adType']) && $_GET['adType'] == 12) echo "selected"; ?>>Facebook - Full Screen</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    <form id="myForm" class="form-horizontal form-bordered" data-toggle="validator" role="form" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tên Campaign</label>
                            <div class="col-sm-10">
                                <input type="text" name="campaignName" placeholder="Tên Campaign" class="form-control" required>
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
                        <?php
                        $listField = array("all");
                        if(isset($_GET['adType'])) switch ($_GET['adType']){
                            case 1:
                                $listField = array("nextTime","adId","accountId","imageUrl","text","rate","description","download");
                                break;
                            case 2:
                                $listField = array("nextTime","adId","accountId","imageUrl","rate","description","download");
                                break;
                            case 3:
                                $listField = array("nextTime","adId","accountId","text","description");
                                break;
                            case 4:
                                $listField = array("nextTime","adId","numberClick","accountId","text","rate","description","download");
                                break;
                            case 5:
                                $listField = array("nextTime","adId","numberClick","accountId","text","rate","description","download");
                                break;
                            case 6:
                                $listField = array("nextTime","accountId","imageUrl","text","rate","description","download","adUrl");
                                break;
                            case 7:
                                $listField = array("nextTime","accountId","imageUrl","text","rate","description","download","adUrl");
                                break;
                            case 8:
                                $listField = array("nextTime","adId","numberClick","accountId","imageUrl","rate","download");
                                break;
                            case 9:
                                $listField = array("nextTime","imageUrl","text","rate","description","download","adUrl");
                                break;
                            case 10:
                                $listField = array("nextTime","imageUrl","text","rate","description","download","adUrl");
                                break;
                            case 11:
                                $listField = array("nextTime","accountId","imageUrl","text","rate","description","download","adUrl");
                                break;
                            case 12:
                                $listField = array("nextTime","accountId","imageUrl","text","rate","description","download","adUrl");
                                break;
                        }
                        if($listField != array('all')) for($i = 0;$i < $this->countColumn; $i++):
                            $metaName = $this->getColumnMeta('campaign',$i);
                            $listField2 = array('adType','campaignId','campaignName','createdAt','updatedAt');
                            $listFieldAll = array_merge($listField,$listField2);
                            if(in_array($metaName['name'],$listFieldAll) == false):
                                if($metaName['native_type'] == 'VAR_STRING' && $metaName['len'] == '6144'):
                                    ?>
                                    <div class="form-group col-md-6 col-xs-12" style="min-height: 81px;">
                                        <label class="col-sm-4 control-label" data-toggle="tooltip" data-placement="top" title="<?php echo $this->getCommentField($metaName['table'],$metaName["name"]);?>"><?php echo $metaName['name']; ?></label>
                                        <div class="col-sm-8">
                                            <select title="<?php echo $metaName['name']; ?>" data-placeholder="All" name="<?php echo $metaName["name"]; ?>[]" multiple class="width300"></select>
                                        </div>
                                        <span class="help-block with-errors"></span>
                                    </div>
                                <?php elseif($metaName['name'] =='imageUrl'): ?>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label class="col-sm-4 control-label" data-toggle="tooltip" data-placement="top" title="<?php echo $this->getCommentField($metaName['table'],$metaName["name"]);?>"><?php echo $metaName['name']; ?></label>
                                        <div class="col-sm-8 input-group mb15">
                                            <input type="text" id="imageUrl" name="<?php echo $metaName["name"]; ?>" class="form-control" placeholder="<?php echo $this->getCommentField($metaName['table'],$metaName["name"]);?>" required>
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" onclick="moxman.browse({fields: 'imageUrl', no_host: false});">Chọn ảnh</button>
                                            </span>
                                        </div>
                                        <span class="help-block with-errors"></span>
                                        <div>
                                            <img id="imagereview" src="<?php echo _ROOT_UPLOAD.'/no-image.png'; ?>" style="width: 300px;display: block;margin: 0 auto;">
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <label class="col-sm-4 control-label" data-toggle="tooltip" data-placement="top" title="<?php echo $this->getCommentField($metaName['table'],$metaName["name"]);?>"><?php echo $metaName['name']; ?></label>
                                        <div class="col-sm-8" style="position: relative">
                                            <?php
                                            switch ($metaName['native_type']){
                                                case 'TINY':
                                                    echo '<input type="checkbox" name="'.$metaName["name"].'" placeholder="" class="form-control"/>';
                                                    break;
                                                case 'DATETIME':
                                                    echo '<input type="datetime-local"  name="'.$metaName["name"].'" value="'.date('Y-m-d\TH:i:s',time()).'" placeholder="" class="form-control" required/>';
                                                    break;
                                                default:
                                                    echo  '<input type="text" name="'.$metaName["name"].'" placeholder="' . $this->getCommentField($metaName['table'],$metaName["name"]). '" class="form-control" required/>';
                                            }
                                            ?>
                                            <?php /*if($metaName["name"] == 'startCampaign') echo "<span style=\"position: absolute;top: 13px;left: 105px;\">00:00:00</span>"; */?><!--
                                            --><?php /*if($metaName["name"] == 'endCampaign') echo "<span style=\"position: absolute;top: 13px;left: 105px;\">23:59:59</span>"; */?>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                <?php endif; endif; endfor; ?>
                        <input type="hidden" name="adType" value="<?php echo isset($_GET['adType'])?$_GET['adType']:$_GET['adType']; ?>">
                        <div class="form-group text-center clearfix">
                            <button type="submit" class="btn btn-primary">Tạo Campaign</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('select').select2();
        $.getJSON("<?php echo _ROOT_CMS; ?>/files/countries.json", function(data) {
            var option  = "<option value='all'>All Country</option>";
            var element = jQuery("select[name='filterCountry[]']");
            var getSelected = element.data('selected');
            $.each(data, function(k, v) {
                option += "<option value='" + v.code + "'>" + v.name + "</option>";
            });
            element.html(option);
            element.select2().select2('val',getSelected);
        });
        $.getJSON("<?php echo _ROOT_CMS; ?>/app-api", function(data) {
            var option  = "<option value='all'>All App</option>";
            var element = jQuery("select[name='packageAppFilter[]']");
            var getSelected = element.data('selected');
            $.each(data, function(k, v) {
                option += "<option value='" + v.packageName + "'>" + v.packageName + "</option>";
            });
            element.html(option);
            element.select2().select2('val',getSelected);
        });
        $.getJSON("<?php echo _ROOT_CMS; ?>/files/MobileType.json", function(data) {
            var option = "<option value='all'>All Mobile Id</option>";
            var element = jQuery("select[name='mobileIdFilter[]']");
            var getSelected = element.data('selected');
            $.each(data, function(k, v) {
                option += "<option value='" + v.type + "'>" + v.name + "</option>";
            });
            element.html(option);
            element.select2().select2('val',getSelected);
        });
        jQuery('select[name="adType"]').on("click", function(e) {
            jQuery('form.add-campaign').submit();
        });
    });
</script>
