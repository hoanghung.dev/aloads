<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-th-large"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Quản lý chiến dịch</h4>
        </div>
    </div>
</div>
<?php
    $oneItem = $this->getCampaign($this->campaign_id,'title,begin_date,end_date');
?>
<div class="contentpanel">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Thay đổi giá chiến dịch
            </div>
            <div class="panel-body">
                <div class="row">
                    <form id="myForm" class="form-horizontal form-bordered" data-toggle="validator"  role="form" action="" method="POST">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Campaign Id</label>
                            <div class="col-sm-8">
                                <input name="campaign_id" type="text" class="form-control" value="<?php echo isset($this->campaign_id)?$this->campaign_id:'' ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Tên chiến dịch</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" value="<?php echo isset($oneItem->title)?$oneItem->title:''; ?>" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Thời gian toàn bộ chiến dịch</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" value="<?php echo $this->timeAgo($oneItem->begin_date,'d/m/Y').' - '.$this->timeAgo($oneItem->end_date,'d/m/Y'); ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Giá cài đặt</label>
                            <div class="col-sm-8">
                                <input name="price_out" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Ngày bắt đầu:</label>
                            <div class="col-sm-8 input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <input type="text" name="begin_date" class="form-control datepicker" placeholder="yyyy/mm/dd">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Ngày kết thúc:</label>
                            <div class="col-sm-8 input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <input type="text" name="end_date" class="form-control datepicker" placeholder="yyyy/mm/dd">
                            </div>
                        </div>

                        <div class="form-group text-center clearfix">
                            <button type="submit" class="btn btn-primary">Thêm giá chiến dịch</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('input.datepicker').datepicker({
            dateFormat: 'yy/mm/dd',
            endDate: "+1 days"
        });
    });
</script>