<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-th-large"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                <li>Dashboard</li>
            </ul>
            <h4>Campaign (Total: <?php echo $this->total; ?>)</h4>
        </div>
    </div>
</div>
<style>
    table tr td:nth-child(11), table tr td:nth-child(12) {
        min-width: 135px;
    }

    table tr td input {
        border: none;
        outline: none;
        text-align: center;
        overflow: hidden;
        text-overflow: ellipsis;
        width: 150px;
    }
</style>
<div class="contentpanel">
    <div class="row filter">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="javascript:;" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i
                            class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i
                            class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Filter</h4>
            </div>
            <div class="panel-body nopadding">
                <form class="form-horizontal form-bordered" method="get" enctype="application/x-www-form-urlencoded">
                    <div class="form-group col-md-4">
                        <label class="col-sm-3 control-label">Loại Campaign</label>
                        <div class="col-sm-9">
                            <select name="adType" title="All Campaign" data-placeholder="All Campaign"
                                    class="form-control">
                                <option value="" selected>All Campaign</option>
                                <option
                                    value="1" <?php if (isset($_GET['adType']) && $_GET['adType'] == 1) echo "selected"; ?>>
                                    Custom - Hidden
                                </option>
                                <option
                                    value="2" <?php if (isset($_GET['adType']) && $_GET['adType'] == 2) echo "selected"; ?>>
                                    Custom - Banner Text
                                </option>
                                <option
                                    value="3" <?php if (isset($_GET['adType']) && $_GET['adType'] == 3) echo "selected"; ?>>
                                    Custom - Banner Image
                                </option>
                                <option
                                    value="4" <?php if (isset($_GET['adType']) && $_GET['adType'] == 4) echo "selected"; ?>>
                                    Custom - Center Image
                                </option>
                                <option
                                    value="5" <?php if (isset($_GET['adType']) && $_GET['adType'] == 5) echo "selected"; ?>>
                                    Custom - Full Screen Image
                                </option>
                                <option
                                    value="6" <?php if (isset($_GET['adType']) && $_GET['adType'] == 6) echo "selected"; ?>>
                                    Admob - Banner
                                </option>
                                <option
                                    value="7" <?php if (isset($_GET['adType']) && $_GET['adType'] == 7) echo "selected"; ?>>
                                    Facebook - Banner
                                </option>
                                <option
                                    value="8" <?php if (isset($_GET['adType']) && $_GET['adType'] == 8) echo "selected"; ?>>
                                    Custom - Full Screen Text
                                </option>
                                <option
                                    value="9" <?php if (isset($_GET['adType']) && $_GET['adType'] == 9) echo "selected"; ?>>
                                    StartApp - Banner
                                </option>
                                <option
                                    value="10" <?php if (isset($_GET['adType']) && $_GET['adType'] == 10) echo "selected"; ?>>
                                    Inmobi - Banner
                                </option>
                                <option
                                    value="11" <?php if (isset($_GET['adType']) && $_GET['adType'] == 11) echo "selected"; ?>>
                                    Admob - FullScreen
                                </option>
                                <option
                                    value="12" <?php if (isset($_GET['adType']) && $_GET['adType'] == 12) echo "selected"; ?>>
                                    Facebook - Full Screen
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <label class="col-sm-3 control-label">Query</label>
                        <div class="col-sm-9">
                            <input type="text" name="query" placeholder="Input query: (field1=value1&field2=value2)"
                                   value="<?php echo isset($_GET['query']) ? str_replace('adtype', '', $_GET['query']) : ''; ?>"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive" style="overflow: auto;">
            <?php
            $listField = [];
            if (isset($_GET['adType'])) switch ($_GET['adType']) {
                case 1:
                    $listField = array("nextTime", "adId", "accountId", "imageUrl", "text", "rate", "description", "download");
                    break;
                case 2:
                    $listField = array("nextTime", "adId", "accountId", "imageUrl", "rate", "description", "download");
                    break;
                case 3:
                    $listField = array("nextTime", "adId", "accountId", "text", "description");
                    break;
                case 4:
                    $listField = array("nextTime", "adId", "numberClick", "accountId", "text", "rate", "description", "download");
                    break;
                case 5:
                    $listField = array("nextTime", "adId", "numberClick", "accountId", "text", "rate", "description", "download");
                    break;
                case 6:
                    $listField = array("nextTime", "accountId", "imageUrl", "text", "rate", "description", "download", "adUrl");
                    break;
                case 7:
                    $listField = array("nextTime", "accountId", "imageUrl", "text", "rate", "description", "download", "adUrl");
                    break;
                case 8:
                    $listField = array("nextTime", "adId", "numberClick", "accountId", "imageUrl", "rate", "download");
                    break;
                case 9:
                    $listField = array("nextTime", "imageUrl", "text", "rate", "description", "download", "adUrl");
                    break;
                case 10:
                    $listField = array("nextTime", "imageUrl", "text", "rate", "description", "download", "adUrl");
                    break;
                case 11:
                    $listField = array("nextTime", "accountId", "imageUrl", "text", "rate", "description", "download", "adUrl");
                    break;
                case 12:
                    $listField = array("nextTime", "accountId", "imageUrl", "text", "rate", "description", "download", "adUrl");
                    break;
            }
            ?>

            <?php
            $fieldsChange = [
                'hiddenAd', 'hasButtonCancel'
            ]
            ?>
            <table class="table table-bordered mb30">
                <thead>
                <tr>
                    <th>STT</th>
                    <?php for ($i = 0; $i < $this->countColumn; $i++):
                        $metaName = $this->getColumnMeta('campaign', $i);
                        if (in_array($metaName['name'], array_merge($listField + ['createdAt', 'updatedAt', 'adType', 'nextTime'])) == false):
                            ?>
                            <th class="text-center"><?php echo $metaName['name']; ?></th>
                        <?php endif; endfor; ?>
                    <th class="text-center" colspan="2">ACTION</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($this->data)) foreach ($this->data as $key => $item): ?>
                    <?php $color = '';
                    if ($key % 2) $color = "background-color:#d6d6d6"; ?>
                    <tr data-id="<?php echo $item->campaignId; ?>"
                        onclick="location.href='/campaign-edit/<?php echo $item->campaignId; ?>';"
                        style="<?php echo $color; ?>">
                        <td><?php echo $key + 1; ?></td>
                        <?php for ($i = 0; $i < $this->countColumn; $i++):
                            $metaName = $this->getColumnMeta('campaign', $i);
                            $nameField = $metaName['name'];
                            if (in_array($metaName['name'], array_merge($listField + ['createdAt', 'updatedAt', 'adType', 'nextTime'])) == false):

                                if (in_array($metaName['name'], $fieldsChange) && $item->$nameField == 1)
                                    $val = 'yes';
                                elseif (in_array($metaName['name'], $fieldsChange) && $item->$nameField == 0)
                                    $val = 'no';
                                else
                                    $val = $item->$nameField;
                                ?>
                                <td><input class="<?php  if($val == 'yes') echo 'blue'; elseif($val == 'no') echo 'red'; ?>" type="text" value='<?php echo $val; ?>' onclick="this.select();"
                                           style="<?php echo $color; ?>"></td>
                            <?php endif; endfor; ?>
                        <td>
                            <a href="/campaign-edit/<?php echo $item->campaignId; ?>" data-toggle="tooltip" title="Edit"
                               class="tooltips"><i class="fa fa-pencil"></i></a>
                        </td>
                        <td>
                            <a href="javascript:;" data-toggle="tooltip" title="Delete"
                               class="delete-row tooltips btnDelete"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div>
            <ul class="pagination pagination-sm no-margin center-block">
                <?php echo isset($this->paging) ? $this->paging : ''; ?>
            </ul>
        </div>
    </div>
</div>
