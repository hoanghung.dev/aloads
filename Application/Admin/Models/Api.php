<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 23/04/2015
 * Time: 09:15 CH
 */
namespace Application\Admin\Models;

use Application\Admin\Controllers\Base;
use Soul\Mvc\Model;
use Soul\Registry;

class Api extends Model
{
    protected  $_service_api = SERVICE_API;


    public function sendSMS($msisdn,$channel='WAP'){
        if(isset($msisdn)){
            $client = new \SoapClient($this->_service_api);
            $result = $client->sendPass($msisdn,$channel);
            $result = explode('|',$result);
            if($result[0] == 0) return true;
            else return false;
        }else return false;
    }

    public function getSubscriber($msisdn){
        if(isset($msisdn)){
            $client = new \SoapClient($this->_service_api);
            $result = $client->getSubscriber($msisdn);
            return $result;
        }else return false;
    }

    public function submitRegister($msisdn,$package,$channel,$cpId=0){
        $base = new Base();
        if(isset($package) && isset($msisdn) && isset($channel)){
            $client = new \SoapClient($this->_service_api);
            //print_r($client->__getFunctions());
            $result = $client->register($msisdn,$package,$channel);
            $base->writeLog('SEND_REGISTER_API_'.date('Y-m-d'),'SEND_REGISTER_API_'.date('Y-m-d H:i:s').'=> Send Subscriber::: '.print_r($result).' API: '.$url.' !<===> MSISDN:'.$_SESSION['msisdn'].' CP_ID:'.$cpId);
            return $result;
        }else return false;
    }

    public function submitUnRegister($msisdn,$channel,$callMPS=0){
        if(isset($msisdn) && isset($channel)){
            $client = new \SoapClient($this->_service_api);
            $result = $client->unregister($msisdn,$callMPS,$channel);
            return $result;
        }else return false;
    }



}