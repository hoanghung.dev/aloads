<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 23/04/2015
 * Time: 09:15 CH
 */
namespace Application\Admin\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class Base extends Model
{
    public function init()
    {
        $this->_mysql = Registry::get('Mysql');
    }
    public function getOne($table,$field)
    {
        /*$sql = sprintf('SELECT COLUMN_COMMENT FROM INFORMATION_SCHEMA.Columns WHERE COLUMN_NAME = "%s"',$field);
        echo $sql;
        $st = $this->_mysql->prepare($sql);
        $st->execute();
        return $st->fetch(\PDO::FETCH_OBJ);*/
        $sql = sprintf('SELECT COLUMN_COMMENT FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = "%s" AND COLUMN_NAME = "%s"',$table,$field);
        //echo $sql;
        $st = $this->_mysql->prepare($sql);
        $st->execute();
        return $st->fetch(\PDO::FETCH_OBJ);
    }
 
}