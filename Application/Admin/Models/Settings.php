<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 23/04/2015
 * Time: 09:15 CH
 */
namespace Application\Admin\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class Settings extends Model
{
    protected  $_tbl ='default_setting';

    public function init()
    {
        $this->_mysql = Registry::get('Mysql');
    }

    public function getAll()
    {
        return $this->_mysql->select($this->_tbl());
    }

    public function getOne($where, $bind)
    {
        $sql = sprintf('SELECT * FROM %s WHERE %s LIMIT 1',$this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        $st->execute($bind);
        return $st->fetch(\PDO::FETCH_OBJ);
    }
    public function getDataArr($args = null)
    {
        $select = '';
        $setting_id = '';
        $not_in = '';
        $in = '';
        $order_by = '';
        $order = '';
        $limit = '';
        $offset = '';
        $type = '';//1: logo, 2: page

        $default = array('select'=>'*','setting_id' => 0,'type'=>0, 'not_in' => 0, 'in' => 0, 'order_by' => null,'limit' => 0, 'offset'=>0);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = ' WHERE 1=1';
        $where .= sprintf(' AND %s.type = %s',$this->_tbl, $type);
        if ($setting_id != 0)
            $where .= sprintf(' AND %s.setting_id = %s',$this->_tbl, $setting_id);

        if ($in != 0)
            $where .= sprintf(' AND %s.setting_id IN (%s)',$this->_tbl, $in);

        if ($not_in != 0)
            $where .= sprintf(' AND %s.setting_id NOT IN (%s)',$this->_tbl, $not_in);


        if($order_by != null)
            $order = sprintf('ORDER BY %s', $order_by);

        if($limit != 0) $limit = sprintf('LIMIT %s,%s',$offset,$limit);
        else $limit = 'LIMIT 0,10';

        $sql = sprintf('SELECT %s FROM %s %s %s %s',$select, $this->_tbl, $where, $order, $limit);
        // echo $sql;
        $st = $this->_mysql->query($sql);
        return $st->fetchALl(\PDO::FETCH_OBJ);

    }

    public function insert($arr){
        return $this->_mysql->insert($this->_tbl, $arr);
    }
    public function update($arr, $where, $bind){
        return $this->_mysql->update($this->_tbl, $arr, $where, $bind);
    }
    public function delete($where,$bind){
        return $this->_mysql->delete($this->_tbl, $where, $bind);
    }
}