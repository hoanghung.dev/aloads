<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 15/03/2014
 * Time: 10:53
 */
namespace Application\Admin\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class Members extends Model
{
    protected  $_tbl ='user';

    public function init()
    {
        $this->_mysql = Registry::get('Mysql');
    }

    public function getAll()
    {
        return $this->_mysql->select($this->_tbl());
    }

    public function getOne($where, $bind,$select='*')
    {
        $sql = sprintf('SELECT %s FROM %s WHERE %s LIMIT 1',$select,$this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        $st->execute($bind);
        return $st->fetch(\PDO::FETCH_OBJ);
    }
    public function getCount($args = null)
    {
        $user_id = '';
        $status = '';
        $appVersion = '';
        $device = '';
        $country= '';
        $start_date = '';
        $end_date = '';
        $androidVersionCode = '';
        $version_start = '';
        $version_end = '';
        $package = '';
        $not_in = '';
        $in = '';
        $search = '';
        $group_id = '';
        $full_name = '';
        $bind = array();


        $default = array('select'=>'*','user_id' => 0,'full_name'=> null,'version_start'=>NULL,'version_end'=>NULL,'start_date'=> null,'end_date'=> null,'appVersion'=> null,'device'=> null,'country'=> null,'androidVersionCode'=> null,'package'=>null,'group_id' => 0,'status'=>0,'not_in' => 0, 'in' => 0,'search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1=1';

        if ($status != null){
            $where .= ' AND status = :status';
            $bind[] = array(
                'element'=>':status',
                'value'=>$status,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($appVersion != null){
            $where .= ' AND appVersion = :appVersion';
            $bind[] = array(
                'element'=>':appVersion',
                'value'=>$appVersion,
            );
        }

        if ($device != null){
            $where .= " AND device LIKE '%".$device."%'";
        }

        if ($country != null){
            $where .= ' AND country = :country';
            $bind[] = array(
                'element'=>':country',
                'value'=>$country,
            );
        }

        if ($androidVersionCode != null){
            $where .= ' AND androidVersionCode = :androidVersionCode';
            $bind[] = array(
                'element'=>':androidVersionCode',
                'value'=>$androidVersionCode,
            );
        }

        if ($package != null){
            $where .= ' AND package = :package';
            $bind[] = array(
                'element'=>':package',
                'value'=>$package,
            );
        }

        if ($start_date != null && $end_date != null){

            $where .= " AND DATE(createdAt) BETWEEN '" . $start_date . "' AND '" . $end_date . "'";
        }

        if ($version_start != null){
            $where .= " AND androidVersionCode >= " . $version_start;
        }

        if ($version_end != null){
            $where .= " AND androidVersionCode <= " . $version_end ;
        }


        if ($group_id != 0){
            $where .= ' AND group_id = :group_id';
            $bind[] = array(
                'element'=>':group_id',
                'value'=>$group_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($in != 0){
            $where .= ' AND user_id IN (:user_id)';
            $bind[] = array(
                'element'=>':user_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND user_id NOT IN (:user_id)';
            $bind[] = array(
                'element'=>':user_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if($full_name != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(full_name) AGAINST (:full_name) OR full_name LIKE :full_name)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$full_name,
                //'type'=>'\PDO::PARAM_STR'
            );
            $bind[] = array(
                'element'=>':searchLike',
                'value'=>'%'.$full_name.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        if($search != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(title) AGAINST (:search) OR title LIKE :searchLike)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
                //'type'=>'\PDO::PARAM_STR'
            );
            $bind[] = array(
                'element'=>':searchLike',
                'value'=>'%'.$search.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        $sql = sprintf('SELECT count(1) FROM %s %s', $this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchColumn();
        return $data;
    }
    public function getDataArr($args = null)
    {
        $select = '';
        $group_id = '';
        $status = '';
        $appVersion = '';
        $device = '';
        $country= '';
        $androidVersionCode = '';
        $package = '';
        $start_date = '';
        $end_date = '';
        $version_start = '';
        $version_end = '';
        $not_in = '';
        $in = ''; 
        $full_name = '';
        $order_by = '';
        $order = '';
        $limit = '';
        $page = '';
        $search = '';
        $bind = array();

        $default = array('select'=>'*','user_id' => 0,'full_name'=> null,'version_start'=>NULL,'version_end'=>NULL,'start_date'=> null,'end_date'=> null,'appVersion'=> null,'device'=> null,'country'=> null,'androidVersionCode'=> null,'package'=>null,'group_id' => 0,'status'=>0,'not_in' => 0, 'in' => 0,'search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1=1';

        if ($status != null){
            $where .= ' AND status = :status';
            $bind[] = array(
                'element'=>':status',
                'value'=>$status,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($appVersion != null){
            $where .= ' AND appVersion = :appVersion';
            $bind[] = array(
                'element'=>':appVersion',
                'value'=>$appVersion,
            );
        }

        if ($device != null){
            $where .= " AND device LIKE '%".$device."%'";
        }

        if ($country != null){
            $where .= ' AND country = :country';
            $bind[] = array(
                'element'=>':country',
                'value'=>$country,
            );
        }

        if ($androidVersionCode != null){
            $where .= ' AND androidVersionCode = :androidVersionCode';
            $bind[] = array(
                'element'=>':androidVersionCode',
                'value'=>$androidVersionCode,
            );
        }

        if ($package != null){
            $where .= ' AND package = :package';
            $bind[] = array(
                'element'=>':package',
                'value'=>$package,
            );
        }

        if ($start_date != null && $end_date != null){

            $where .= " AND DATE(createdAt) BETWEEN '" . $start_date . "' AND '" . $end_date . "'";
        }

        if ($version_start != null){
            $where .= " AND androidVersionCode >= " . $version_start;
        }

        if ($version_end != null){
            $where .= " AND androidVersionCode <= " . $version_end ;
        }


        if ($group_id != 0){
            $where .= ' AND group_id = :group_id';
            $bind[] = array(
                'element'=>':group_id',
                'value'=>$group_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($in != 0){
            $where .= ' AND user_id IN (:user_id)';
            $bind[] = array(
                'element'=>':user_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND user_id NOT IN (:user_id)';
            $bind[] = array(
                'element'=>':user_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if($full_name != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(full_name) AGAINST (:full_name) OR full_name LIKE :full_name)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$full_name,
                //'type'=>'\PDO::PARAM_STR'
            );
            $bind[] = array(
                'element'=>':searchLike',
                'value'=>'%'.$full_name.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        if($search != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(title) AGAINST (:search) OR title LIKE :searchLike)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
                //'type'=>'\PDO::PARAM_STR'
            );
            $bind[] = array(
                'element'=>':searchLike',
                'value'=>'%'.$search.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        if($order_by != null)
            $order = sprintf('ORDER BY %s', $order_by);

        if($limit != 0){
            $page = intval($page);
            $offset = ($page-1)*$limit;
            $limit = sprintf('LIMIT %d,%d',$offset,$limit);
        }else $limit = 'LIMIT 0,10';
        $sql = sprintf('SELECT %s FROM %s %s %s %s',$select, $this->_tbl, $where, $order, $limit);

        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchAll(\PDO::FETCH_OBJ);
        return $data;

    }
    public function countColumn($column, $args = null)
    {
        $select = '';
        $group_id = '';
        $status = '';
        $appVersion = '';
        $device = '';
        $country= '';
        $androidVersionCode = '';
        $package = '';
        $start_date = '';
        $end_date = '';
        $version_start = '';
        $version_end = '';
        $not_in = '';
        $in = '';
        $full_name = '';
        $order_by = '';
        $order = '';
        $limit = '';
        $page = '';
        $search = '';
        $bind = array();

        $default = array('select'=>'*','user_id' => 0,'full_name'=> null,'version_start'=>NULL,'version_end'=>NULL,'start_date'=> null,'end_date'=> null,'appVersion'=> null,'device'=> null,'country'=> null,'androidVersionCode'=> null,'package'=>null,'group_id' => 0,'status'=>0,'not_in' => 0, 'in' => 0,'search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1=1';

        if ($status != null){
            $where .= ' AND status = :status';
            $bind[] = array(
                'element'=>':status',
                'value'=>$status,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($appVersion != null){
            $where .= ' AND appVersion = :appVersion';
            $bind[] = array(
                'element'=>':appVersion',
                'value'=>$appVersion,
            );
        }

        if ($device != null){
            $where .= " AND device LIKE '%".$device."%'";
        }

        if ($country != null){
            $where .= ' AND country = :country';
            $bind[] = array(
                'element'=>':country',
                'value'=>$country,
            );
        }

        if ($androidVersionCode != null){
            $where .= ' AND androidVersionCode = :androidVersionCode';
            $bind[] = array(
                'element'=>':androidVersionCode',
                'value'=>$androidVersionCode,
            );
        }

        if ($package != null){
            $where .= ' AND package = :package';
            $bind[] = array(
                'element'=>':package',
                'value'=>$package,
            );
        }

        if ($start_date != null && $end_date != null){

            $where .= " AND DATE(createdAt) BETWEEN '" . $start_date . "' AND '" . $end_date . "'";
        }

        if ($version_start != null){
            $where .= " AND androidVersionCode >= " . $version_start;
        }

        if ($version_end != null){
            $where .= " AND androidVersionCode <= " . $version_end ;
        }


        if ($group_id != 0){
            $where .= ' AND group_id = :group_id';
            $bind[] = array(
                'element'=>':group_id',
                'value'=>$group_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($in != 0){
            $where .= ' AND user_id IN (:user_id)';
            $bind[] = array(
                'element'=>':user_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND user_id NOT IN (:user_id)';
            $bind[] = array(
                'element'=>':user_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if($full_name != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(full_name) AGAINST (:full_name) OR full_name LIKE :full_name)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$full_name,
                //'type'=>'\PDO::PARAM_STR'
            );
            $bind[] = array(
                'element'=>':searchLike',
                'value'=>'%'.$full_name.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        if($search != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(title) AGAINST (:search) OR title LIKE :searchLike)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
                //'type'=>'\PDO::PARAM_STR'
            );
            $bind[] = array(
                'element'=>':searchLike',
                'value'=>'%'.$search.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }


//        $sql = sprintf('select count('.$column.') as count, '.$column.' FROM '.$this->_tbl.' group by ' . $column);

        $sql = sprintf('select count('.$column.') as count, '.$column.' FROM %s %s  group by ' . $column, $this->_tbl, $where);

        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }

        $st->execute();
        $data = $st->fetchAll(\PDO::FETCH_OBJ);
        return $data;

    }
    public function insert($arr){
        return $this->_mysql->insert($this->_tbl, $arr);
    }
    public function update($arr, $where, $bind){
        return $this->_mysql->update($this->_tbl, $arr, $where, $bind);
    }
    public function delete( $where, $bind){
        return $this->_mysql->delete($this->_tbl, $where, $bind);
    }
}