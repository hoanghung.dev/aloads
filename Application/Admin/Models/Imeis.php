<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 15/03/2014
 * Time: 10:53
 */
namespace Application\Admin\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class Imeis extends Model
{
    protected  $_tbl ='blacklistimei';

    public function init()
    {
        $this->_mysql = Registry::get('Mysql');
    }

    public function getAll()
    {
        return $this->_mysql->select($this->_tbl());
    }

    public function getOne($where, $bind,$select='*')
    {
        $sql = sprintf('SELECT %s FROM %s WHERE %s LIMIT 1',$select,$this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        $st->execute($bind);
        return $st->fetch(\PDO::FETCH_OBJ);
    }
    public function getColumnMeta($i)
    {
        $sql = sprintf('SELECT * FROM %s',$this->_tbl);
        $st = $this->_mysql->prepare($sql);
        $st->execute();
        return $st->getColumnMeta($i);
    }
    public function getCountColumn($args = null)
    {
        $select = '';
        $default = array('select'=>'*');
        $args = $this->parseArgs($args, $default);
        extract($args);
        $sql = sprintf('SELECT %s FROM %s',$select, $this->_tbl);
        $st = $this->_mysql->prepare($sql);
        $st->execute();
        $data = $st->columnCount();
        return $data;
    }
    public function getCount($args = null)
    {
        $search = '';
        $bind = array();

        $default = array('select'=>'*','search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1);
        $args = $this->parseArgs($args, $default);
        extract($args);

        unset($args['select']);
        unset($args['search']);
        unset($args['order_by']);
        unset($args['limit']);
        unset($args['page']);

        $where = 'WHERE 1=1';

        foreach ($args as $key=>$value){
            if($key != null){
                $where .= sprintf(' AND %s = :%s',$key,$key);
                $bind[] = array(
                    'element'=>':'.$key,
                    'value'=>$value,
                );
            }
        }

        if($search != null){
            $where .= ' AND MATCH(title,intro,keywords) AGAINST (:search)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
            );
        }
        $sql = sprintf('SELECT COUNT(1) FROM %s %s', $this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchColumn();
        return $data;
    }
    public function getDataArr($args = null)
    {
        $select = '';
        $order_by = '';
        $order = '';
        $limit = '';
        $page = '';
        $search = '';
        $bind = array();

        $default = array('select'=>'*','search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1);
        $args = $this->parseArgs($args, $default);
        extract($args);

        unset($args['select']);
        unset($args['search']);
        unset($args['order_by']);
        unset($args['limit']);
        unset($args['page']);

        $where = 'WHERE 1=1';

        foreach ($args as $key=>$value){
            if($key != null){
                $where .= sprintf(' AND %s = :%s',$key,$key);
                $bind[] = array(
                    'element'=>':'.$key,
                    'value'=>$value,
                );
            }
        }

        if($search != null){
            $where .= ' AND MATCH(title,intro,keywords) AGAINST (:search)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
            );
        }

        if($order_by != null)
            $order = sprintf('ORDER BY %s', $order_by);

        if($limit != 0){
            $page = intval($page);
            $offset = ($page-1)*$limit;
            $limit = sprintf('LIMIT %d,%d',$offset,$limit);
        }else $limit = 'LIMIT 0,10';
        $sql = sprintf('SELECT %s FROM %s %s %s %s',$select, $this->_tbl, $where, $order, $limit);

        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchAll(\PDO::FETCH_OBJ);
        return $data;

    }
    public function insert($arr){
        return $this->_mysql->insert($this->_tbl, $arr);
    }
    public function update($arr, $where, $bind){
        return $this->_mysql->update($this->_tbl, $arr, $where, $bind);
    }
    public function delete( $where, $bind){
        return $this->_mysql->delete($this->_tbl, $where, $bind);
    }
}