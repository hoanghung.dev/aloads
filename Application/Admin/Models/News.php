<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 23/04/2015
 * Time: 09:15 CH
 */
namespace Application\Admin\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class News extends Model
{
    protected  $_tbl ='default_news';

    public function init()
    {
        $this->_mysql = Registry::get('Mysql');
    }

    public function getAll()
    {
        return $this->_mysql->select($this->_tbl());
    }

    public function getOne($where, $bind, $select="*")
    {
        $sql = sprintf('SELECT %s FROM %s WHERE %s LIMIT 1',$select,$this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        $st->execute($bind);
        return $st->fetch(\PDO::FETCH_OBJ);
    }
    public function getCount($args = null)
    {
        $news_id = '';
        $category_id = '';
        $is_trash = '';
        $is_approval = '';
        $not_in = '';
        $in = '';
        $search = '';
        $bind = array();


        $default = array('select'=>'*','news_id' => 0,'category_id'=>0,'is_trash'=>0,'is_approval'=>0, 'not_in' => 0, 'in' => 0,'search'=> null);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE is_trash = :is_trash AND is_approval = :is_approval';
        $bind[] = array(
            'element'=>':is_trash',
            'value'=>$is_trash,
            //'type'=>'\PDO::PARAM_INT'
        );
        $bind[] = array(
            'element'=>':is_approval',
            'value'=>$is_approval,
            //'type'=>'\PDO::PARAM_INT'
        );
        if ($news_id != 0){
            $where .= ' AND news_id = :news_id';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$news_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($in != 0){
            $where .= ' AND news_id IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND news_id NOT IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($category_id != 0){
            $where .= ' AND category_id = :category_id';
            $bind[] = array(
                'element'=>':category_id',
                'value'=>$category_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }


        if($search != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(title) AGAINST (:search) OR title LIKE :searchLike)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
                //'type'=>'\PDO::PARAM_STR'
            );
            $bind[] = array(
                'element'=>':searchLike',
                'value'=>'%'.$search.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        $sql = sprintf('SELECT count(1) FROM %s %s', $this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchColumn();
        return $data;
    }
    public function getDataArr($args = null)
    {
        $select = '';
        $news_id = '';
        $category_id = '';
        $os_id = '';
        $is_trash = '';
        $is_approval = '';
        $not_in = '';
        $in = '';
        $order_by = '';
        $order = '';
        $limit = '';
        $page = '';
        $search = '';
        $bind = array();


        $default = array('select'=>'*','news_id' => 0,'category_id'=>0,'os_id'=>0,'is_trash'=>0,'is_approval'=> 0 ,'not_in' => 0, 'in' => 0,'search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1,'is_total'=>0);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE is_trash = :is_trash AND is_approval = :is_approval';
        $bind[] = array(
            'element'=>':is_trash',
            'value'=>$is_trash,
            //'type'=>'\PDO::PARAM_INT'
        );
        $bind[] = array(
            'element'=>':is_approval',
            'value'=>$is_approval,
            //'type'=>'\PDO::PARAM_INT'
        );
        if ($news_id != 0){
            $where .= ' AND news_id = :news_id';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$news_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($in != 0){
            $where .= ' AND news_id IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND news_id NOT IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($category_id != 0){
            $where .= ' AND category_id = :category_id';
            $bind[] = array(
                'element'=>':category_id',
                'value'=>$category_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($os_id != 0){
            $where .= ' AND os_id = :os_id';
            $bind[] = array(
                'element'=>':os_id',
                'value'=>$os_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if($search != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(title) AGAINST (:search) OR title LIKE :searchLike)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
                //'type'=>'\PDO::PARAM_STR'
            );
            $bind[] = array(
                'element'=>':searchLike',
                'value'=>'%'.$search.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        if($order_by != null)
            $order = sprintf('ORDER BY %s', $order_by);

        if($limit != 0){
            $page = intval($page);
            $offset = ($page-1)*$limit;
            $limit = sprintf('LIMIT %d,%d',$offset,$limit);
        }else $limit = 'LIMIT 0,10';

        $sql = sprintf('SELECT %s FROM %s %s %s %s',$select, $this->_tbl, $where, $order, $limit);
        /*echo "<!--".$sql."-->";
        echo "<!--<pre>";
        print_r($bind);
        echo "</pre>-->";*/
        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchAll(\PDO::FETCH_OBJ);
        return $data;
    }

    public function insert($arr){
        return $this->_mysql->insert($this->_tbl, $arr);
    }
    public function update($arr, $where, $bind){
        return $this->_mysql->update($this->_tbl, $arr, $where, $bind);
    }
    public function delete($where,$bind){
        return $this->_mysql->delete($this->_tbl, $where, $bind);
    }
}