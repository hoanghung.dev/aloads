<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 15/03/2014
 * Time: 10:53
 */
namespace Application\Admin\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class Modules extends Model
{
    protected  $_tbl ='default_modules';

    public function init()
    {
        $this->_mysql = Registry::get('Mysql');
    }

    public function getAll()
    {
        return $this->_mysql->select($this->_tbl());
    }

    public function getOne($where, $bind,$select='*')
    {
        $sql = sprintf('SELECT %s FROM %s WHERE %s LIMIT 1',$select,$this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        $st->execute($bind);
        return $st->fetch(\PDO::FETCH_OBJ);
    }

    public function getDataArr($args = null)
    {
        $select = '';
        $module_id = '';
        $is_trash = '';
        $not_in = '';
        $in = '';
        $order_by = '';
        $order = '';
        $limit = '';
        $offset = '';
        $search = '';
        $is_total = '';

        $default = array('select'=>'*','module_id' => 0,'is_trash'=>0,'not_in' => 0, 'in' => 0,'search'=> null, 'order_by' => null,'limit' => 0, 'offset'=>0,'is_total'=>0);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = ' WHERE 1=1';
        $where .= sprintf(' AND %s.is_trash = %s',$this->_tbl, $is_trash);

        if ($module_id != 0)
            $where .= sprintf(' AND %s.module_id = %s',$this->_tbl, $module_id);

        if ($in != 0)
            $where .= sprintf(' AND %s.module_id IN (%s)',$this->_tbl, $in);

        if ($not_in != 0)
            $where .= sprintf(' AND %s.module_id NOT IN (%s)',$this->_tbl, $not_in);

        if($search != null){
            $search = str_replace(' ','%',$search);
            $where .= ' AND '.$this->_tbl.'.title LIKE "%'.$search.'%"';
        }

        if($order_by != null)
            $order = sprintf('ORDER BY %s', $order_by);

        if($limit != 0) $limit = sprintf('LIMIT %d,%d',$offset,$limit);
        else $limit = 'LIMIT 0,10';

        $sql = sprintf('SELECT %s FROM %s %s %s %s',$select, $this->_tbl, $where, $order, $limit);
        //echo $sql;
        $st = $this->_mysql->query($sql);
        $data = $st->fetchALl(\PDO::FETCH_OBJ);


        if($is_total == true && !empty($data)){
            $sql = sprintf('SELECT COUNT(*) AS intCount FROM %s %s', $this->_tbl, $where);
            $st = $this->_mysql->query($sql);
            $data[0]->total_item = $st->fetchColumn();
        }

        return $data;

    }
    public function insert($arr){
        return $this->_mysql->insert($this->_tbl, $arr);
    }
    public function update($arr, $where, $bind){
        return $this->_mysql->update($this->_tbl, $arr, $where, $bind);
    }
    public function delete( $where, $bind){
        return $this->_mysql->delete($this->_tbl, $where, $bind);
    }
}