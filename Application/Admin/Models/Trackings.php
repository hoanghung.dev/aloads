<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 23/04/2015
 * Time: 09:15 CH
 */
namespace Application\Admin\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class Trackings extends Model
{
    protected  $_tbl ='default_tracking';
    protected  $_tbl_click ='default_tracking_click';
    protected  $_tbl_install ='default_tracking_install';
    protected  $_tbl_client ='default_tracking_client';

    public function init()
    {
        $this->_mysql = Registry::get('Mysql');
        if(ENABLE_MEMCACHED == true) $this->_memcache = Registry::get('Memcache');
    }

    public function getAll()
    {
        return $this->_mysql->select($this->_tbl());
    }

    public function getOne($where, $bind, $select="*")
    {
        $sql = sprintf('SELECT %s FROM %s WHERE %s LIMIT 1',$select,$this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        $st->execute($bind);
        return $st->fetch(\PDO::FETCH_OBJ);
    }
    public function getOneClick($where, $bind, $select="*")
    {
        $sql = sprintf('SELECT %s FROM %s WHERE %s LIMIT 1',$select,$this->_tbl_click, $where);
        $st = $this->_mysql->prepare($sql);
        $st->execute($bind);
        return $st->fetch(\PDO::FETCH_OBJ);
    }

    public function countAll($sql){
        $sql = sprintf('SELECT COUNT(1) FROM (%s) AS T ',$sql);
        $st = $this->_mysql->prepare($sql);
        $st->execute();
        return $st->fetchColumn();
    }
    public function getCount($args = null)
    {
        $tracking_id = '';
        $status = '';
        $is_approval = '';
        $not_in = '';
        $in = '';
        $search = '';
        $bind = array();


        $default = array('select'=>'*','tracking_id'=>0,'status'=>null, 'not_in' => 0, 'in' => 0,'search'=> null);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1=1';

        if ($status != null){
            $where .= ' AND status = :status';
            $bind[] = array(
                'element'=>':status',
                'value'=>$status,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($in != 0){
            $where .= ' AND tracking_id IN (:tracking_id)';
            $bind[] = array(
                'element'=>':tracking_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND tracking_id NOT IN (:tracking_id)';
            $bind[] = array(
                'element'=>':tracking_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($tracking_id != 0){
            $where .= ' AND tracking_id = :tracking_id';
            $bind[] = array(
                'element'=>':tracking_id',
                'value'=>$tracking_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }


        if($search != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(title) AGAINST (:search) OR title LIKE :searchLike)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
                //'type'=>'\PDO::PARAM_STR'
            );
            $bind[] = array(
                'element'=>':searchLike',
                'value'=>'%'.$search.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        $sql = sprintf('SELECT count(1) FROM %s %s', $this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchColumn();
        return $data;
    }
    public function getDataArr($args = null){
        $select = '';
        $campaign_id = '';
        $user_id = '';
        $status = '';
        $not_in = '';
        $in = '';
        $order_by = '';
        $order = '';
        $limit = '';
        $page = '';
        $search = '';
        $bind = array();


        $default = array('select'=>'*','campaign_id'=>0,'user_id'=>0,'status'=>null,'is_approval'=> 0 ,'not_in' => 0, 'in' => 0,'search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1,'is_total'=>0);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1=1';

        if ($status != null){
            $where .= ' AND status = :status';
            $bind[] = array(
                'element'=>':status',
                'value'=>$status,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($campaign_id != 0){
            $where .= ' AND campaign_id = :campaign_id';
            $bind[] = array(
                'element'=>':campaign_id',
                'value'=>$campaign_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($user_id != 0){
            $where .= ' AND user_id = :user_id';
            $bind[] = array(
                'element'=>':user_id',
                'value'=>$user_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }


        if ($in != 0){
            $where .= ' AND tracking_id IN (:tracking_id)';
            $bind[] = array(
                'element'=>':tracking_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND tracking_id NOT IN (:tracking_id)';
            $bind[] = array(
                'element'=>':tracking_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }



        if($search != null){
            //$search = rawurldecode($search);
            $where .= ' AND (MATCH(title) AGAINST (:search) OR title LIKE :searchLike)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
                //'type'=>'\PDO::PARAM_STR'
            );
            $bind[] = array(
                'element'=>':searchLike',
                'value'=>'%'.$search.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        if($order_by != null)
            $order = sprintf('ORDER BY %s', $order_by);

        if($limit != 0){
            $page = intval($page);
            $offset = ($page-1)*$limit;
            $limit = sprintf('LIMIT %d,%d',$offset,$limit);
        }else $limit = 'LIMIT 0,10';
        $sql = sprintf('SELECT %s FROM %s %s %s %s',$select, $this->_tbl, $where, $order, $limit);
        echo "<!--".$sql."-->";
        echo "<!--<pre>";
        print_r($bind);
        echo "</pre>-->";
        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchAll(\PDO::FETCH_OBJ);
        return $data;
    }
    public function getClick($args = null){
        $view_date = '';
        $tracking_id ='';
        $campaign_id ='';
        $bind = array();

        $default = array('select'=>'*','view_date' => NULL,'tracking_id' => 0,'campaign_id' => 0,'category_id' => NULL,'cateIn'=> NULL,'order' => '','by'=>'DESC','limit' => 10,'offset'=>0);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1=1';

        switch ($view_date) {
            case 'day':
                //$where .= ' AND view_date = DATE_FORMAT(NOW(),"%Y-%m-%d")';
                $where .= sprintf(' AND view_date = "%s"',date('Y-m-d'));
                break;
            case 'week':
                $where .= sprintf(' AND %s.view_date > DATE_SUB(NOW(), INTERVAL 7 DAY)',$this->_tbl_click);
                break;
            case 'month':
                $where .= sprintf(' AND %s.view_date = %s',$this->_tbl_click,$view_date);
                break;
            default:
                break;
        }

        if ($campaign_id != 0){
            $where .= ' AND campaign_id = :campaign_id';
            $bind[] = array(
                'element'=>':campaign_id',
                'value'=>$campaign_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($tracking_id != 0){
            $where .= ' AND tracking_id = :tracking_id';
            $bind[] = array(
                'element'=>':tracking_id',
                'value'=>$tracking_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        $sql = sprintf('SELECT SUM(click) AS total FROM %s %s GROUP BY tracking_id', $this->_tbl_click, $where);
        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetch(\PDO::FETCH_OBJ);
        return $data;
    }
    public function getInstall($args = null){
        $view_date = '';
        $tracking_id ='';

        $default = array('select'=>'*','view_date' => NULL,'tracking_id' => 0,'category_id' => NULL,'cateIn'=> NULL,'order' => '','by'=>'DESC','limit' => 10,'offset'=>0);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1=1';

        switch ($view_date) {
            case 'day':
                //$where .= ' AND view_date = DATE_FORMAT(NOW(),"%Y-%m-%d")';
                $where .= sprintf(' AND view_date = "%s"',date('Y-m-d'));
                break;
            case 'week':
                $where .= sprintf(' AND %s.view_date > DATE_SUB(NOW(), INTERVAL 7 DAY)',$this->_tbl_install);
                break;
            case 'month':
                $where .= sprintf(' AND %s.view_date = %s',$this->_tbl_install,$view_date);
                break;
            default:
                break;
        }

        if ($tracking_id != 0)
            $where .= sprintf(' AND %s.tracking_id = %s',$this->_tbl_install, $tracking_id);


        $sql = sprintf('SELECT SUM(install) AS total FROM %s %s GROUP BY tracking_id', $this->_tbl_install, $where);
        $st = $this->_mysql->query($sql);
        $data = $st->fetch(\PDO::FETCH_OBJ);
        return $data;
    }
    public function updateClick($trackingId) {
        $key = md5(MEMCACHE_KEY.'tracking_click');
        if(ENABLE_MEMCACHED == true){
            $data = $this->_memcache->get($key);
            if($data >= 5){
                $sql = sprintf("INSERT INTO %s (`created_time`, `tracking_id`, `click`) VALUES ('%s',%d,5) ON DUPLICATE KEY UPDATE `click` = `click` + 5",$this->_tbl_click, date('Y-m-d'), $trackingId);
                $this->_mysql->query($sql);
                //$this->_mysql->query(sprintf('UPDATE %s SET  click = click + 5 WHERE tracking_id = %d', $this->_tbl_click, $trackingId));
                $this->_memcache->set($key,0,false);
            }else{
                $data = $data + 1;
                $this->_memcache->set($key,$data,false);
            }
        }else{
            $sql = sprintf("INSERT INTO %s (`created_time`, `tracking_id`, `click`) VALUES ('%s',%d,1) ON DUPLICATE KEY UPDATE `click` = `click` + 1",$this->_tbl_click, date('Y-m-d'), $trackingId);
            $this->_mysql->query($sql);
            //$this->_mysql->query(sprintf('UPDATE %s SET  click = click + 1 WHERE tracking_id = %d', $this->_tbl_click, $trackingId));
            return 'update no cache !';
        }

    }
    public function insert($arr){
        return $this->_mysql->insert($this->_tbl, $arr);
    }
    public function insertClick($arr){
        return $this->_mysql->insert($this->_tbl_click, $arr);
    }
    public function insertInstall($arr){
        return $this->_mysql->insert($this->_tbl_install, $arr);
    }
    public function insertClient($arr){
        return $this->_mysql->insert($this->_tbl_client, $arr);
    }

    public function update($arr, $where, $bind){
        return $this->_mysql->update($this->_tbl, $arr, $where, $bind);
    }

    public function delete($where,$bind){
        return $this->_mysql->delete($this->_tbl, $where, $bind);
    }

}