<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 23/04/2015
 * Time: 09:15 CH
 */
namespace Application\Admin\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class Report extends Model
{
    protected  $_tbl;

    public function init()
    {
        $this->_mysql = Registry::get('MysqlUser');
    }

    public function countQuery($query, $bind = null)
    {
        $st = $this->_mysql->prepare($query);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null);
        }
        $st->execute();
        $data = $st->fetchColumn();
        return $data;
    }
    public function queryAll($query, $bind = null)
    {
        $st = $this->_mysql->prepare($query);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null);
        }
        $st->execute();
        $data = $st->fetchAll(\PDO::FETCH_OBJ);
        return $data;
    }
}