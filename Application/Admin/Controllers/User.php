<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 02:30 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Users;

class User extends Base{
    private function _registerSession($user){
        $this->_session->auth = $user;
        $_SESSION['isLoggedIn'] = true; // True/false if user is logged in or not, should be same as above
        $_SESSION["UserName"] = "Tony";
        $_SESSION['moxiemanager.filesystem.rootpath'] = DIR_UPLOAD; // Set a root path for this use
    }
    public function index(){
        $this->accessDenied('user_list');
        $userModel = new Users();
        $page = $this->_request->getParam('page',1);
        $limit = 30;
        $params['select'] = '*';
        $params['group_id'] = $this->_request->getParam('group_id');
        $params['order_by'] = 'user_id DESC';
        $params['page'] = $page;
        $params['limit'] = $limit;
        $data = $userModel->getDataArr($params);
        $total = $userModel->getCount($params);
        if(empty($data)) $this->_flash->warning('Chưa có dữ liệu !');
        $this->view->data = $data;
        $this->view->total = $total;
        $this->view->paging = $this->getPaging($total,$page,$limit,5);
        $this->view->flash = $this->_flash;
        $this->displayLayout('default', $this->render());
    }
    public function group(){
        $this->accessDenied('group');
        $userModel = new Users();
        $page = $this->_request->getParam('page',1);
        $limit = 30;
        $params['select'] = '*';
        $params['group_id'] = $this->_request->getParam('group_id');
        $params['order_by'] = 'group_id DESC';
        $params['page'] = $page;
        $params['limit'] = $limit;
        $data = $userModel->getDataGroup($params);
        $total = $userModel->getCountGroup($params);
        if(empty($data)) $this->_flash->warning('Chưa có dữ liệu !');
        $this->view->data = $data;
        $this->view->total = $total;
        $this->view->paging = $this->getPaging($total,$page,$limit,5);
        $this->view->flash = $this->_flash;
        $this->displayLayout('default', $this->render());
    }
    public function genCode(){
        $this->accessDenied('user_gencode');
        $page = $this->_request->getParam('page',1);
        $userModel = new Users();
        if($this->_session->auth->username == 'admin' || $this->_session->auth->group_id == 3) $user_id = $this->_request->getParam('user_id');else $user_id = null;
        $limit = 10;
        if($this->_session->auth->username != 'admin' || $this->_session->auth->group_id == 3) $user_id = $this->_session->auth->user_id;

        $this->view->data = $userModel->listCpId($user_id,$limit,$page);
        if($this->_request->isPost()){
            $data = array();
            $quality = intval($this->_request->getParam('quality'));
            $data['user_id'] = $this->_request->getParam('user_id')?$this->_request->getParam('user_id'):$user_id;
            if($quality >=1) for($i = 1;$i <=$quality; $i++){
                $userModel->insertCp($data);
            }
            $this->_flash->success("Tạo ".$quality." mã thành công !");
            unset($data);
        }
        $this->view->total = $total = $userModel->countCpId($user_id);
        $this->view->paging = $this->getPaging($total,$page,$limit,5,'/user/code');
        $this->displayLayout('default', $this->render());
    }
    function captcha(){
        include DIR_FOLDER."/Admin/captcha/simple-php-captcha.php";
        $this->_session->captcha = simple_php_captcha();
    }
    public function login(){
        /*if($this->_request->isPost() && strtolower($this->_request->getPost('captcha')) !== strtolower($this->_session->captcha['code'])){
            $this->_flash->danger('Sai mã Captcha !');
        }else{*/
            if($this->_request->getPost('CSRF_TOKEN') == md5(date('Ymd').'_steven') && $this->_request->getPost('username') != null && $this->_request->getPost('password') != null){
                $userModel = new Users();
                $userName = $this->_request->getPost('username');
                $userName = preg_replace('/[^A-Za-z0-9\-]/', '', $userName);
                $userPass = $this->_request->getPost('password');
                $data = $userModel->getOne('username = :name AND password = :pass',array(':name'=>$userName,':pass'=>md5($userPass.'_steven')));
                if(!empty($data)){
                    $this->_registerSession($data);
                    $this->redirect('/');
                }else{
                    $this->_flash->danger('Sai Username hoặc Password !');
                }
            }
        /*}*/
        //$this->captcha();
        $this->view->flash = $this->_flash;
        $this->displayLayout('login',$this->render());
    }
    function setSession($username,$password,$cookie=null){
        // Other code for login ($_POST[]....)
        // $row is result of your sql query
        $values = array($username,$this->obscure($password),$row['id']);
        $session = implode(",",$values);

        // check if cookie is enable for login
        if($cookie=='on'){
            setcookie("your_cookie_name", $session, time()+60*60*24*100,'/');
        } else {
            $_SESSION["your_session_name"] = $session;
        }
    }
    public function logout(){
        unset($this->_session->auth);
        $this->redirect('/');
    }
    public function profile(){
        $userModel = new Users();
        $id = $this->_request->getParam('id');
        $oneItem = $userModel->getOne('user_id = :id',array(':id'=>$id));
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            if($oneItem->password != $this->_request->getPost('password')) $data['password'] = md5($this->_request->getPost('password').'_steven');
            else $data['password'] = $oneItem->password;
            $data['birthday'] = date('Y-m-d',strtotime($data['birthday']));
            $data['updated_time'] = date('Y-m-d H:i:s');
            if($this->_request->getPost('status')) $data['status'] = true; else $data['status'] = false;
            if(is_array($data)){
                if($userModel->update($data,'user_id = :id',array(':id'=>$id))) $this->_flash->success("Cập nhật thành viên thành công !");
                else $this->_flash->danger("Cập nhật thành viên không thành công !");
                unset($user);
            }
        }
        $this->view->data = $oneItem;
        $this->displayLayout('default',$this->render());
    }
    public function setting(){
        $this->displayLayout('default',$this->render());
    }
    public function add(){
        $this->accessDenied('user_add');
        $userModel = new Users();
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['password'] = md5($this->_request->getPost('password')."_steven");
            $data['updated_time'] = date('Y-m-d H:i:s');
            $data['birthday'] = date('Y-m-d',strtotime($data['birthday']));
            //print_r($data);exit;
            if($this->_request->getPost('status')) $data['status'] = true; else $data['status'] = false;
            if($userModel->getOne('username = :user',array(':user'=>$data['username']))){
                $this->_flash->warning('Username đã tồn tại !');
            }else{
                if($userModel->insert($data)) $this->_flash->success("Thêm mới thành viên thành công !");
                else $this->_flash->danger("Thêm mới thành viên không thành công !");
                unset($data);
            }

        }
        $this->view->flash = $this->_flash;
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        if($this->_session->auth->user_id !== $this->_request->getParam('id')) $this->accessDenied('user_edit');
        $id = $this->_request->getParam('id');
        $userModel = new Users();
        $oneItem = $userModel->getOne('user_id = :id',array(':id'=>$id));
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            if($oneItem->password != $this->_request->getPost('password')) $data['password'] = md5($this->_request->getPost('password').'_steven');
            else $data['password'] = $oneItem->password;
            $data['birthday'] = date('Y-m-d',strtotime($data['birthday']));
            $data['updated_time'] = date('Y-m-d H:i:s');
            if($this->_request->getPost('status')) $data['status'] = true; else $data['status'] = false;
            if(is_array($data)){
                if($userModel->update($data,'user_id = :id',array(':id'=>$id))) $this->_flash->success("Cập nhật thành viên thành công !");
                else $this->_flash->danger("Cập nhật thành viên không thành công !");
                unset($user);
            }
        }
        $this->view->data = $oneItem;
        $this->view->flash = $this->_flash;
        $this->displayLayout('default',$this->render());
    }
    public function addGroup(){
        $this->accessDenied('group_add');
        $userModel = new Users();
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            if($this->_request->getPost('status')) $data['status'] = true; else $data['status'] = false;

            if($userModel->insertGroup($data)) $this->_flash->success("Thêm mới nhóm thành công !");
            else $this->_flash->danger("Thêm mới nhóm không thành công !");
            unset($data);
        }
        $this->view->flash = $this->_flash;
        $this->displayLayout('default',$this->render());
    }
    public function editGroup(){
        $this->accessDenied('group_edit');
        $id = $this->_request->getParam('id');
        $userModel = new Users();
        $oneItem = $userModel->getOneGroup('group_id = :id',array(':id'=>$id));
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            if($this->_request->getPost('status')) $data['status'] = true; else $data['status'] = false;
            if(is_array($data)){
                if($userModel->updateGroup($data,'group_id = :id',array(':id'=>$id))) $this->_flash->success("Cập nhật nhóm thành công !");
                else $this->_flash->danger("Cập nhật nhóm không thành công !");
                unset($user);
            }
        }
        $this->view->data = $oneItem;
        $this->view->flash = $this->_flash;
        $this->displayLayout('default',$this->render());
    }

    public function actDeleteCpId(){
        $this->accessDenied('user_cpid_delete');
        $userModel = new Users();
        $id = $this->_request->getParam('id');
        $res = $userModel->deleteCp('cp_id = :id',array(':id'=>$id));
        print_r($res);
        exit;
    }

    public function actDelete(){
        $this->accessDenied('user_delete');
        $userModel = new Users();
        $id = $this->_request->getParam('id');
        $res = $userModel->delete('user_id = :id',array(':id'=>$id));
        print_r($res);
        exit;
    }
    public function actDeleteGroup(){
        $this->accessDenied('group_delete');
        $userModel = new Users();
        $id = $this->_request->getParam('id');
        $res = $userModel->delete('group_id = :id',array(':id'=>$id));
        print_r($res);
        exit;
    }
}