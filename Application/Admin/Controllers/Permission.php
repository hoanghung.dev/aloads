<?php
namespace Application\Admin\Controllers;


use Application\Admin\Models\Permissions;
use Application\Admin\Models\UsersGroup;

class Permission extends Base
{
    public function index(){
        $this->accessDenied('permission');
        $permissionModel = new Permissions();
        $params['parent_id'] = 0;
        $params['order_by'] = 'order_number ASC';
        $data = $permissionModel->getDataArr($params);
        $this->view->data = $data;
        //print_r($data);
        $this->displayLayout('default',$this->render());
    }

    public function permissionRoles(){
        $this->accessDenied('permission-roles');
        $userModel = new UsersGroup();
        $param['select'] = '*';
        if($this->_request->getParam('status')) $param['status'] = $this->_request->getParam('status');

        $this->view->data = $userModel->getDataArr($param);
        $this->view->total = $userModel->getCount($param);
        $this->displayLayout('default',$this->render());
    }
    public function addRoles(){
        $this->accessDenied('permission-add-roles');
        $userModel = new UsersGroup();

        if($this->_request->isPost()){
            $user['title'] = $this->_request->getPost('title');
            if($this->_request->getPost('status') == 'on')
                $user['status'] = 0;//Trạng thái mở
            else  $user['status'] = 1; // Trạng thái khóa

            if($userModel->getOne('title = :title',array(':title'=>$user['title']))){
                echo "Tên nhóm đã tồn tại !";
            }else{
                if(is_array($user)){
                    if($userModel->insert($user) == true) $this->_flash->addMessage("Thêm mới nhóm thành công !");
                    else $this->_flash->addMessage("Thêm mới nhóm không thành công !");
                    unset($user);

                }
            }

        }
        $this->displayLayout('default',$this->render());
    }

    public function editRoles(){
        $this->accessDenied('permission-edit-roles');
        $userModel = new UsersGroup();
        $id = $this->_request->getParam('id');
        $data = $userModel->getOne('group_id = :id',array(':id'=>$id));
        if($this->_request->isPost()){
            $user['title'] = $this->_request->getPost('title');
            if($this->_request->getPost('status') == 'on')
                $user['status'] = 0;//Trạng thái mở
            else  $user['status'] = 1; // Trạng thái khóa
            if(is_array($user)){
                /*if($userModel->update($user,'group_id = :id',array(':id'=>$id)) == true) echo "Cập nhật nhóm thành công !";
                else echo "Cập nhật nhóm không thành công !";*/
                if($userModel->update($user,'group_id = :id',array(':id'=>$id)) == true) $this->_flash->addMessage("Cập nhật nhóm thành công !");
                else $this->_flash->addMessage("Cập nhật nhóm không thành công !");


                unset($user);

            }
        }
        $this->view->data = $data;
        $this->displayLayout('default',$this->render());

    }


    public function permissionDetail(){
        $this->accessDenied('permission-detail');
        $groupUserModel = new UsersGroup();
        $permissionModel = new Permissions();
        $params['parent_id'] = 0;
        $params['order_by'] = 'order_number ASC';
        $this->view->listPermission = $permissionModel->getDataArr($params);
        $this->view->listGroup = $groupUserModel->getDataArr();
        $this->displayLayout('default',$this->render());
    }
    public function add(){
        $this->accessDenied('permission_add');
        $permissionModel = new Permissions();
        if($this->_request->isPost()){
            $data['title'] = $this->_request->getPost('title');
            $data['module'] = $this->_request->getPost('module_key');
            $data['parent_id'] = $this->_request->getPost('parent_id');
            if($this->_request->getPost('status') == 'on')
                $user['status'] = 1;//Trạng thái mở
            else  $user['status'] = 0; // Trạng thái khóa
            if($permissionModel->getOne('module = :module',array(':module'=>$data['module']))){
                echo "Module đã tồn tại !";
            }else{
                if(is_array($data)){
                    if($permissionModel->insert($data) == true) $this->_flash->addMessage("Thêm mới module thành công !");
                    else $this->_flash->addMessage("Thêm mới module không thành công !");
                    unset($data);

                }
            }

        }
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $this->accessDenied('permission_edit');
        $permissionModel = new Permissions();
        $id = $this->_request->getParam('id');
        if($this->_request->isPost()){
            $data['title'] = $this->_request->getPost('title');
            $data['module'] = $this->_request->getPost('module_key');
            $data['parent_id'] = $this->_request->getPost('parent_id');
            if($this->_request->getPost('status') == 'on')
                $user['status'] = 1;//Trạng thái mở
            else  $user['status'] = 0; // Trạng thái khóa
            if(is_array($data)){
                if($permissionModel->update($data,'permission_id = :id',array(':id'=>$id)) == true) $this->_flash->addMessage("Cập nhật module thành công !");
                else $this->_flash->addMessage("Cập nhật module không thành công !");
                unset($data);

            }

        }
        $this->view->data = $permissionModel->getOne('permission_id = :id',array(':id'=>$id));
        $this->displayLayout('default',$this->render());
    }
    public function actDelete(){
        $permissionModel = new Permissions();
        $id = $this->_request->getParam('id');
        if($permissionModel->delete('permission_id = :id',array(':id'=>$id))) print 'Xóa bản ghi thành công !';
        else print 'Xóa bản ghi không thành công !';
        exit;
    }
    public function actUpdate(){
        if($this->_request->isPost()){
            $userGroupModel = new UsersGroup();
            $id = $this->_request->getPost('id');
            $permission = $this->_request->getPost('permission');
            if($userGroupModel->update(array('role_permission'=>$permission),'group_id = :id',array(':id'=>$id))){
                print 'Cập nhật phân quyền thành công !';
            }else{
                print 'Cập nhật phân quyền không thành công !';
            }
            exit;
        }
    }
    public function actTrashRoles(){
        $userModel = new UsersGroup();
        $id = $this->_request->getParam('id');
        if($userModel->update(array('status' => 1),'group_id = :id',array(':id'=>$id))) print 'Xóa tạm bản ghi thành công !';
        else print 'Xóa tạm bản ghi không thành công !';
        exit;
    }
    public function actUnTrashRoles(){
        $userModel = new UsersGroup();
        $id = $this->_request->getParam('id');
        if($userModel->update(array('status' => 0),'group_id = :id',array(':id'=>$id))) print 'Khôi phục bản ghi thành công !';
        else print 'Khôi phục bản ghi không thành công !';
        exit;
    }

    public function actDeleteRoles(){
        $userModel = new UsersGroup();
        $id = $this->_request->getParam('id');
        if($userModel->delete('group_id = :id',array(':id'=>$id))) print 'Xóa bản ghi thành công !';
        else print 'Xóa bản ghi không thành công !';
        exit;
    }

}