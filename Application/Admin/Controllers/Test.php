<?php
namespace Application\Admin\Controllers;

require_once dirname(dirname(dirname(__FILE__))) . '/Library/PHPMailer/PHPMailerAutoload.php';

use Application\Admin\Models\Category;
use Application\Admin\Models\Movies;
use Application\Admin\Models\News;
use Application\Admin\Models\Users;

class Test extends Base
{
    public function index(){
        echo $_SERVER['HTTP_USER_AGENT'] . "\n\n";

        $browser = get_browser(null, true);
        echo "<pre>";
        print_r($browser);
        echo "</pre>";
    }


    function checkUrl($url)
    {
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);

        /* Get the HTML or whatever is linked in $url. */
        $response = curl_exec($handle);

        /* Check for 404 (file not found). */
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        if ($httpCode == 404 || $httpCode == 403 || $httpCode == 400) {
            return 0;
        } else {
            return 1;
        }
    }

    public function test()
    {
        $appModel = new \Application\Admin\Models\Apps();
        $apps = $appModel->getDataArr();
        $appOn = [];
        $appOff = [];

        foreach ($apps as $item) {
            if($this->checkUrl('https://play.google.com/store/apps/details?id=' . $item->packageName) == 0 && $item->appStatus == 1) {
                echo $item->packageName . " đã off";
                echo "\n";
                if($appModel->update(['appStatus'=>0], 'appId = :appId', [':appId' => $item->appId]) == true) {
                    echo $item->packageName . " đã cập nhật trạng thái";
                    echo "\n";
                }
                $appOff['http://admin.aloads.net/app-edit/' . $item->appId] = $item->packageName;
            } elseif($this->checkUrl('https://play.google.com/store/apps/details?id=' . $item->packageName) == 1 && $item->appStatus == 0) {
                echo $item->packageName . " đã on";
                echo "\n";
                if($appModel->update(['appStatus'=>1], 'appId = :appId', [':appId' => $item->appId]) == true) {
                    echo $item->packageName . " đã cập nhật trạng thái";
                    echo "\n";
                }
                $appOn['http://admin.aloads.net/app-edit/' . $item->appId] = $item->packageName;
            }
        }

        $_SESSION['mail'] = [
            'title'     => 'Aloads thông báo trạng thái app thay đổi',
            'data'      => [
                'on'    => $appOn,
                'off'   => $appOff,
                'date'  => date('d-m-Y H:m:s')
            ]
        ];
        $this->sendMai();
    }

    public function sendMai() {

        $mail = new \PHPMailer();

        $mail->isSMTP();

        $mail->SMTPDebug = 2;

        $mail->Debugoutput = 'html';

        $mail->Host = "ssl://smtp.gmail.com";

        $mail->Port = 465;

        $mail->SMTPAuth = true;

        $mail->Username = "hoanghung.developer@gmail.com";

        $mail->Password = "vungoimocuara!@#";

        $mail->setFrom('hoanghung.developer@gmail.com', 'Hoàng Hùng');

        $mail->addReplyTo('alert@netvietgroups.com', 'Aloads');

        $mail->addAddress('hoanghung.developer2@gmail.com', 'alert@netvietgroups.com');

        $mail->Subject = 'Aloads thông báo trạng thái app thay đổi';

        $mail->msgHTML('hunghung');

        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }

    }
}