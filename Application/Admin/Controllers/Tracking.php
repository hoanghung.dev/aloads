<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;

use Application\Admin\Models\Trackings;

class Tracking extends Base{
    public $_title = 'Tracking';
    public $_keyId = 'tracking_id';

    public function index(){
        $this->accessDenied('tracking');
        $trackingModel = new Trackings();
        $campaignId = $this->_request->getParam('campaign_id');
        $userId = $this->_request->getParam('user_id');
        $params['select'] = '*';
        if($campaignId != '') $params['campaign_id'] = $campaignId;
        if($userId != '') $params['user_id'] = $userId;
        $data = $trackingModel->getDataArr($params);
        $this->view->data = $data;
        $this->view->countAll = '';
        $this->displayLayout('default', $this->render());
    }
    public function add(){
        $this->accessDenied('tracking-add');
        $trackingModel = new Trackings();
        if($this->_request->isPost()){
           /* echo "<pre>".
                print_r($this->_request->getPosts())
                ."</pre>";*/
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['user_id'] = $data['user_id'] != 0?$data['user_id']:$data['parent_id'];
            $data['param'] = sprintf('parent_id=%d&campaign_id=%d&user_id=%d',$data['parent_id'],$data['campaign_id'],$data['user_id']);
            $data['link'] = $this->encrypt($data['param']);
            $data['password'] = md5($data['param'].'_steven');
            $data['status'] = $this->_request->getPost('status') == 'on'?1:0;
            $data['created_time'] = date(DATE_TIME_FORMAT);

            /*echo "<pre>".
                print_r($data)
                ."</pre>";die('ok');*/
            if($trackingModel->insert($data) == true){
                $this->_flash->addMessage("Thêm mới ".$this->_title." thành công !");
                $this->redirect('/tracking');
            }
            else $this->_flash->addMessage("Thêm mới ".$this->_title." không thành công !");
            unset($data);
        }
        $this->displayLayout('default',$this->render());
    }
    public function addPrice(){
        $this->accessDenied('tracking-price-add');
        $this->view->tracking_id = $this->_request->getParam('id');
        $urlReturn = $this->_request->getParam('return','/tracking');
        $trackingModel = new Trackings();
        /*echo "<pre>".
                print_r($this->_request->getPosts())
                ."</pre>";*/
        if($this->_request->isPost()){
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['begin_date'] = date('Y-m-d H:i:s',strtotime($this->_request->getPost('begin_date')));
            $data['end_date'] = date('Y-m-d H:i:s',strtotime($this->_request->getPost('end_date')));
            $data['created_time'] = date(DATE_TIME_FORMAT);
            
             /*echo "<pre>".
                 print_r($data)
                 ."</pre>";die('ok');*/
            if($trackingModel->insertPrice($data)){
                $this->_flash->addMessage("Thêm mới giá thành công !");
                $this->redirect($urlReturn);
            }else $this->_flash->addMessage("Thêm mới giá không thành công !");
            unset($data);
        }
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $this->accessDenied('tracking-edit');
        $trackingModel = new Trackings();
        $id = $this->_request->getParam('id');
        $oneItem = $trackingModel->getOne($this->_keyId.' = :id',array(':id'=>$id));
        $this->view->data = $oneItem;
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }

            $data['is_incentive'] = $this->_request->getPost('auto_pause') == 'on'?1:0;
            $data['auto_pause'] = $this->_request->getPost('auto_pause') == 'on'?1:0;
            $data['updated_time'] = date(DATE_TIME_FORMAT);

            if(is_array($data)){
                if($trackingModel->updatePrice($data,$this->_keyId.' = :id',array(':id'=>$id)) == true){
                    $this->_flash->addMessage("Cập nhật ".$this->_title." thành công !");
                    $this->redirect('/tracking');
                }
                else $this->_flash->addMessage("Cập nhật ".$this->_title." không thành công !");
                unset($data);
            }

        }
        $this->displayLayout('default',$this->render());
    }
    public function editPrice(){
        $this->accessDenied('tracking-price-edit');
        $urlReturn = $this->_request->getParam('return','/tracking');
        $trackingModel = new Trackings();
        $id = $this->_request->getParam('id');
        $oneItem = $trackingModel->getOnePrice($this->_keyId.' = :id',array(':id'=>$id));
        $this->view->data = $oneItem;
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            if(is_array($data)){
                if($trackingModel->update($data,$this->_keyId.' = :id',array(':id'=>$id)) == true){
                    $this->_flash->addMessage("Cập nhật ".$this->_title." thành công !");
                    $this->redirect($urlReturn);
                }
                else $this->_flash->addMessage("Cập nhật ".$this->_title." không thành công !");
                unset($data);
            }

        }
        $this->displayLayout('default',$this->render());
    }
    public function actDelete(){
        $trackingModel = new Trackings();
        $id = $this->_request->getParam('id');
        if($trackingModel->delete($this->_keyId.' = :id',array(':id'=>$id))) print 'Xóa '.$this->_title.' thành công !';
        else print 'Xóa '.$this->_title.' không thành công !';
        exit;
    }
    public function actPriceDelete(){
        $trackingModel = new Trackings();
        $id = $this->_request->getParam('id');
        if($trackingModel->deletePrice($this->_keyId.' = :id',array(':id'=>$id))) print 'Xóa giá chiến dịch thành công !';
        else print 'Xóa giá chiến dịch không thành công !';
        exit;
    }
}
