<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Apps;

class App extends Base
{

    public function index(){
        $appModel = new Apps();
        if($this->_request->getPost('query')) $this->redirect(sprintf('?%s',$this->_request->getPost('query')));
        $page = $this->_request->getParam('page',1);
        $limit = 10;
        $params['select'] = '*';
        $params['page'] = $page;
        $params['order_by'] = 'appId DESC';
        $params['limit'] = $limit;
        if(isset($_SERVER['QUERY_STRING'])){
            parse_str($_SERVER['QUERY_STRING'],$query);
            $params = array_merge($params,$query);
        }
        //print_r($params);
        $data = $appModel->getDataArr($params);
        $total = $appModel->getCount($params);
        if(empty($data)) $this->_flash->warning('Không có dữ liệu !');
        $this->view->data = $data;
        $this->view->total = $total;
        $this->view->paging = $this->getPaging($total,$page,$limit,5);
        $this->view->countColumn = $appModel->getCountColumn($params);
        $this->displayLayout('default', $this->render());
    }
    public function api(){
        $appModel = new Apps();
        $params['select'] = 'packageName';
        $params['order_by'] = 'appId DESC';
        $params['limit'] = 1000;
        $data = $appModel->getDataArr($params);
        print json_encode($data);
    }
    public function add(){
        $appModel = new Apps();
        $this->view->countColumn = $appModel->getCountColumn();
        if($this->_request->isPost()){
            $data = array();
            //echo "<pre>";print_r($this->_request->getPosts());echo "</pre>";
            foreach($this->_request->getPosts() as $field=>$value){
                if($value == 'on') $data[$field] = 1; else $data[$field] = $value;
            }
            $data['createdAt'] = $data['updatedAt'] = date('Y-m-d H:i:s');
            if($appModel->insert($data) == true) $this->_flash->success("Thêm mới App thành công ! <a href='/app' class='btn btn-info btn-xs' title='Về trang quản lý'>App Manage</a>&nbsp;<a href='/app-add' class='btn btn-primary btn-xs' title='Thêm App khác'>Add More</a>");
            else $this->_flash->danger("Thêm mới App không thành công !");
            unset($data);

        }
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $appModel = new Apps();
        $this->view->countColumn = $appModel->getCountColumn();
        $id = $this->_request->getParam('id');
        $columnsCheckBox = [
            'appStatus', 'adEnable', 'contactEnable', 'notificationEnable', 'logClickEnable', 'adminPermissionEnable',
            'smsEnable', 'shortcutEnable', 'pictureEnable', 'videoEnable'
        ];
        if($this->_request->isPost()){
            $data = array();

            $columns = [];      // Need review
            foreach($this->_request->getPosts() as $field=>$value){
                if($value == 'on') $data[$field] = 1; else $data[$field] = $value;
                $columns[] = $field;        // Need review
            }
            // Check disable input checkbox
            foreach ($columnsCheckBox as $item) {
                if(!in_array($item, $columns)) $data[$item] = 0;
            }

            $data['updatedAt'] = date('Y-m-d H:i:s');
            if(is_array($data)){
                if($appModel->update($data,'appId = :cateId',array(':cateId'=>$id)) == true) $this->_flash->success("Cập nhật App thành công ! <a href='/app' class='btn btn-info btn-xs' title='Về trang quản lý'>App Manage</a>&nbsp;<a href='/app-add' class='btn btn-primary btn-xs' title='Thêm App khác'>Add More</a>");
                else $this->_flash->danger("Cập nhật App không thành công !");
                unset($data);
            }
        }
        $this->view->data = $appModel->getOne('appId = :id',array(':id'=>$id));
        $this->displayLayout('default',$this->render());

    }

    public function actDelete(){
        $appModel = new Apps();
        $id = $this->_request->getPost('id');
        $data = $appModel->delete('appId = :id',array(':id'=>$id));
        print $data;
        exit;
    }

    public function mailstatusapp() {
        echo $this->render();
    }
}