<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo $_SESSION['mail']['title']; ?></title>
    <!-- Latest compiled and minified CSS -->
</head>
<body>
<div style="width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 11px;">
    <h1><?php echo $_SESSION['mail']['title']; ?></h1>
    <?php $data = $_SESSION['mail']['data']; ?>
    <div align="center">
        <table style="width: 100%;">
            <thead>
            <tr>
                <td>Trạng thái</td>
                <td>Tên package</td>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>Live</td>
                <td>
                    <?php foreach ($data['on'] as $key => $item): ?>
                        <a href="<?php echo $key; ?>" target="_blank"><?php echo $item; ?></a> |
                    <?php endforeach;; ?>
                </td>
            </tr>
            <tr>
                <td>Die</td>
                <td>
                    <?php foreach ($data['off'] as $key => $item): ?>
                        <a href="<?php echo $key; ?>" target="_blank"><?php echo $item; ?></a> |
                    <?php endforeach;; ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <?php unset($_SESSION['mail']);?>
    <p>Xem chi tiết tại: <a href="http://admin.aloads.net/app" target="_blank">http://admin.aloads.net/app</a></p>
    <p>Mọi thay đổi đã được cập nhật vào database!</p>
    <p>Quét lúc: <?php echo $data['date']; ?></p>
</div>
</body>
</html>
