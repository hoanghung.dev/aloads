<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Category;

class Categories extends Base
{

    public function index(){
        $this->accessDenied('category');
        $categoryModel = new Category();
        $param['select'] = '*';
        if($this->_request->getParam('parent_id')) $param['parent_id'] = $this->_request->getParam('parent_id');
        if($this->_request->getParam('is_trash')) $param['is_trash'] = $this->_request->getParam('is_trash');
        $this->view->data = $categoryModel->getDataArr($param);
        $this->view->countAll = $categoryModel->getCount($param);
        $this->displayLayout('default', $this->render());
    }
    public function add(){
        $this->accessDenied('category_add');
        $categoryModel = new Category();
        if($this->_request->isPost()){
            $data['title'] = $this->_request->getPost('title');
            $data['title_page'] = $this->_request->getPost('title_page');
            $data['slug'] = $this->toSlug($this->_request->getPost('title'));
            $data['intro'] = $this->_request->getPost('intro');
            $data['parent_id'] = $this->_request->getPost('parent_id');
            $data['class'] = $this->_request->getPost('class');
            $data['created_time'] = date(DATE_TIME_FORMAT);
            $data['updated_time'] = date(DATE_TIME_FORMAT);
            if($categoryModel->getOne('slug = :slug',array(':slug'=>$data['slug']))){
                echo "Danh mục đã tồn tại !";
            }else{
                if(is_array($data)){
                    if($categoryModel->insert($data) == true) $this->_flash->addMessage("Thêm mới danh mục thành công !");
                    else $this->_flash->addMessage("Thêm mới danh mục không thành công !");
                    unset($data);

                }
            }

        }
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $this->accessDenied('category_edit');
        $categoryModel = new Category();
        $id = $this->_request->getParam('id');
        if($this->_request->isPost()){
            $data['title'] = $this->_request->getPost('title');
            $data['title_page'] = $this->_request->getPost('title_page');
            $data['slug'] = $this->toSlug($this->_request->getPost('title'));
            $data['intro'] = $this->_request->getPost('intro');
            $data['parent_id'] = $this->_request->getPost('parent_id');
            $data['class'] = $this->_request->getPost('class');
            $data['updated_time'] = date(DATE_TIME_FORMAT);


            if(is_array($data)){
                if($categoryModel->update($data,'category_id = :cateId',array(':cateId'=>$id)) == true) $this->_flash->addMessage("Cập nhật danh mục thành công !");
                else $this->_flash->addMessage("Cập nhật danh mục không thành công !");
                unset($data);

            }

        }
        $this->view->data = $categoryModel->getOne('category_id = :id',array(':id'=>$id));
        $this->displayLayout('default',$this->render());

    }
    public function actTrash(){
        $categoryModel = new Category();
        $id = $this->_request->getParam('id');
        if($categoryModel->update(array('is_trash' => 1),'category_id = :id',array(':id'=>$id))) print 'Xóa tạm bản ghi thành công !';
        else print 'Xóa tạm bản ghi không thành công !';
        exit;
    }
    public function actUnTrash(){
        $categoryModel = new Category();
        $id = $this->_request->getParam('id');
        if($categoryModel->update(array('is_trash' => 0),'category_id = :id',array(':id'=>$id))) print 'Khôi phục danh mục thành công !';
        else print 'Khôi phục danh mục không thành công !';
        exit;
    }
    public function actDelete(){
        $categoryModel = new Category();
        $id = $this->_request->getParam('id');
        if($categoryModel->delete('category_id = :id',array(':id'=>$id))) print 'Xóa bản ghi thành công !';
        else print 'Xóa bản ghi không thành công !';
        exit;
    }
}