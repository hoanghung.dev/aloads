<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Campaigns;

class Campaign extends Base
{
    public function index(){
        $campaignModel = new Campaigns();
        $adType = $this->_request->getParam('adType');
        $query = urldecode($this->_request->getParam('query'));
        //if(!empty($query)) $this->redirect(sprintf('?adType=%s&%s',$adType,$query));
        /*elseif($adType) $this->redirect('?adType='.$adType);*/
        $page = $this->_request->getParam('page',1);
        $limit = 10;
        $params['select'] = '*';
        if(isset($query)){
            parse_str($query,$queryArr);
            $params = array_merge($params,$queryArr);
        }
        if(!empty($adType)) $params['adType'] = $adType;
        $params['page'] = $page;
        $params['order_by'] = 'campaignId DESC';
        $params['limit'] = $limit;
        $data = $campaignModel->getDataArr($params);
        $total = $campaignModel->getCount($params);
        if(empty($data)) $this->_flash->danger('Không có dữ liệu !');
        $this->view->data = $data;
        /*echo "<pre>";
        print_r($data);exit;*/
        $this->view->total = $total;
        $this->view->paging = $this->getPaging($total,$page,$limit,5);
        $this->view->countColumn = $campaignModel->getCountColumn($params);
        $this->displayLayout('default', $this->render());
    }
    public function add(){
        $campaignModel = new Campaigns();
        $this->view->countColumn = $campaignModel->getCountColumn();

        if($this->_request->isPost()){
            $data = array();

            foreach($this->_request->getPosts() as $field=>$value){
                if($value == 'on') $data[$field] = 1;else $data[$field] = $value;
            }
            $data['packageAppFilter'] = ($this->_request->getPost('packageAppFilter') != null)?json_encode($this->_request->getPost('packageAppFilter')):'["all"]';
            $data['filterCountry'] = ($this->_request->getPost('filterCountry') != null)?json_encode($this->_request->getPost('filterCountry')):'["all"]';
            $data['mobileIdFilter'] = ($this->_request->getPost('mobileIdFilter') != null)?json_encode($this->_request->getPost('mobileIdFilter')):'["all"]';
            $data['startCampaign'] = date('Y-m-d H:i:s',strtotime($this->_request->getPost('startCampaign')));
            $data['endCampaign'] = date('Y-m-d H:i:s',strtotime($this->_request->getPost('endCampaign')));
            $data['createdAt'] = $data['updatedAt'] = date('Y-m-d H:i:s');

            //echo "<pre>";print_r($data);echo "</pre>";exit;
            if($campaignModel->insert($data) == true) $this->_flash->success("Thêm mới Campaign thành công ! <a href='/campaign' class='btn btn-info btn-xs' title='Về trang quản lý'>Campaign Manage</a>&nbsp;<a href='/campaign-add' class='btn btn-primary btn-xs' title='Thêm Campaign khác'>Add More</a>");
            else $this->_flash->danger("Thêm mới Campaign không thành công !");
            unset($data);
        }
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $campaignModel = new Campaigns();
        $id = $this->_request->getParam('id');
        $this->view->countColumn = $campaignModel->getCountColumn();
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                if($value == 'on') $data[$field] = 1;else $data[$field] = $value;
            }

            $data['hiddenAd'] = ($this->_request->getPost('hiddenAd') != null)?1:0;
            $data['hasButtonCancel'] = ($this->_request->getPost('hasButtonCancel') != null)?1:0;
            $data['packageAppFilter'] = ($this->_request->getPost('packageAppFilter') != null)?json_encode($this->_request->getPost('packageAppFilter')):'["all"]';
            $data['filterCountry'] = ($this->_request->getPost('filterCountry') != null)?json_encode($this->_request->getPost('filterCountry')):'["all"]';
            $data['mobileIdFilter'] = ($this->_request->getPost('mobileIdFilter') != null)?json_encode($this->_request->getPost('mobileIdFilter')):'["all"]';
            $data['startCampaign'] = date('Y-m-d H:i:s',strtotime($this->_request->getPost('startCampaign')));
            $data['endCampaign'] = date('Y-m-d H:i:s',strtotime($this->_request->getPost('endCampaign')));
            $data['updatedAt'] = date('Y-m-d H:i:s');

            if(is_array($data)){
                if($campaignModel->update($data,'campaignId = :cateId',array(':cateId'=>$id)) == true) $this->_flash->success("Cập nhật Campaign thành công ! <a href='/campaign' class='btn btn-info btn-xs' title='Về trang quản lý'>Campaign Manage</a>&nbsp;<a href='/campaign-add' class='btn btn-primary btn-xs' title='Thêm Campaign khác'>Add More</a>");
                else $this->_flash->danger("Cập nhật Campaign không thành công !");
                unset($data);
            }
        }
        $this->view->data = $campaignModel->getOne('campaignId = :id',array(':id'=>$id));
        $this->displayLayout('default',$this->render());

    }

    public function actDelete(){
        $campaignModel = new Campaigns();
        $id = $this->_request->getPost('id');
        $data = $campaignModel->delete('campaignId = :id',array(':id'=>$id));
        print $data;
        exit;
    }
}