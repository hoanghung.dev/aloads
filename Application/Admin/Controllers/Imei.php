<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Imeis;

class Imei extends Base
{

    public function index(){
        $memberModel = new Imeis();
        $page = $this->_request->getParam('page',1);
        $limit = 15;
        $param['select'] = '*';
        $params['page'] = $page;

        $start_date = $this->_request->getParam('start_date') == NULL ? date('Y-m-d 00:00:00') : $this->_request->getParam('start_date');
        $end_date = $this->_request->getParam('end_date') == NULL ? date('Y-m-d 23:59:00') : $this->_request->getParam('end_date');

        $params['order_by'] = 'id DESC';
        $params['limit'] = $limit;
        //print_r($params);
        $data = $memberModel->getDataArr($params);
        $total = $memberModel->getCount($params);
        if(empty($data)) $this->_flash->danger('Chưa có dữ liệu !');
        $this->view->data = $data;
        $this->view->total = $total;
        $this->view->totalAll = $memberModel->getCount();
        $this->view->paging = $this->getPaging($total,$page,$limit,5);
        //$this->view->countColumn = $memberModel->getCountColumn($param);
        $this->displayLayout('default', $this->render());
    }
    public function add(){
        $imeiModel = new Imeis();
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['createdAt'] = $data['updatedAt'] = date('Y-m-d H:i:s');
            if($imeiModel->insert($data) == true) $this->_flash->success("Thêm mới User thành công !");
            else $this->_flash->danger("Thêm mới User không thành công !");
            unset($data);


        }
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $imeiModel = new Imeis();
        $id = $this->_request->getParam('id');
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['updatedAt'] = date('Y-m-d H:i:s');
            if(is_array($data)){
                if($imeiModel->update($data,'id = :id',array(':id'=>$id)) == true) $this->_flash->success("Cập nhật User thành công !");
                else $this->_flash->danger("Cập nhật User không thành công !");
                unset($data);
            }
        }
        $this->view->data = $imeiModel->getOne('id = :id',array(':id'=>$id));
        $this->displayLayout('default',$this->render());

    }

    public function actDelete(){
        $imeiModel = new Imeis();
        $id = $this->_request->getPost('id');
        $data = $imeiModel->delete('id = :id',array(':id'=>$id));
        print $data;
        exit;
    }
}