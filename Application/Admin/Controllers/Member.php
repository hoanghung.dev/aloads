<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Members;

class Member extends Base 
{ 

    public function index(){
        $memberModel = new Members();
        $page = $this->_request->getParam('page',1);
        $limit = $this->_request->getParam('paginate') == NULL ? 15 : $this->_request->getParam('paginate');
        $param['select'] = '*';
        $params['page'] = $page;
        $params['appVersion'] = $this->_request->getParam('appVersion');
        $params['device'] = $this->_request->getParam('device');
        $params['country'] = strtolower($this->_request->getParam('country')) == '' ? NULL : strtolower($this->_request->getParam('country'));
        $params['version_start'] = $this->_request->getParam('version_start');
        $params['version_end'] = $this->_request->getParam('version_end');
        $params['package'] = $this->_request->getParam('package');
        $params['start_date'] = $this->_request->getParam('start_date') == NULL ? date('Y-m-d 00:00:00') : $this->_request->getParam('start_date');
        $params['end_date'] = $this->_request->getParam('end_date') == NULL ? date('Y-m-d 23:59:00') : $this->_request->getParam('end_date');
        $params['order_by'] = 'userId DESC';

        $params['order_by'] = 'userId DESC';
        $params['limit'] = $limit;
        //print_r($params);
        $data = $memberModel->getDataArr($params);
        if(empty($data)) $this->_flash->danger('Chưa có dữ liệu !');
        $this->view->data = $data;
        $this->view->filter = $params;

        unset($params['page']); unset($params['limit']);
        $this->view->totalFilter = $total = $memberModel->getCount($params);
        $this->view->total = $memberModel->getCount();
        
        // Thong ke
        $this->view->countryCount = $memberModel->countColumn('country', $params);
        $this->view->countOSVersion = $memberModel->countColumn('androidVersionCode', $params);

        $this->view->paging = $this->getPaging($total,$page,$limit,5);
        //$this->view->countColumn = $memberModel->getCountColumn($param);
        $this->displayLayout('default', $this->render());
    }
    public function add(){
        $UserModel = new Members();
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['createdAt'] = $data['updatedAt'] = date('Y-m-d H:i:s');
            if($UserModel->insert($data) == true) $this->_flash->success("Thêm mới User thành công !");
            else $this->_flash->danger("Thêm mới User không thành công !");
            unset($data);


        }
        $this->displayLayout('default',$this->render());
    }
    public function edit(){
        $UserModel = new Members();
        $id = $this->_request->getParam('id');
        if($this->_request->isPost()){
            $data = array();
            foreach($this->_request->getPosts() as $field=>$value){
                $data[$field] = $value;
            }
            $data['updatedAt'] = date('Y-m-d H:i:s');
            if(is_array($data)){
                if($UserModel->update($data,'UserId = :id',array(':id'=>$id)) == true) $this->_flash->success("Cập nhật User thành công !");
                else $this->_flash->danger("Cập nhật User không thành công !");
                unset($data);
            }
        }
        $this->view->data = $UserModel->getOne('UserId = :id',array(':id'=>$id));
        $this->displayLayout('default',$this->render());

    }

    public function actDelete(){
        $UserModel = new Members();
        $id = $this->_request->getPost('id');
        $data = $UserModel->delete('UserId = :id',array(':id'=>$id));
        print $data;
        exit;
    }
    
    public function exportExcel() {
        $memberModel = new Members();
        $param['select'] = '*';
        $params['appVersion'] = $this->_request->getParam('appVersion');
        $params['device'] = $this->_request->getParam('device');
        $params['country'] = strtolower($this->_request->getParam('country'));
        $params['androidVersionCode'] = $this->_request->getParam('androidVersionCode');
        $params['start_date'] = $this->_request->getParam('start_date') == NULL ? date('Y-m-d 00:00:00') : $this->_request->getParam('start_date');
        $params['end_date'] = $this->_request->getParam('end_date') == NULL ? date('Y-m-d 23:59:00') : $this->_request->getParam('end_date');

        $params['order_by'] = 'userId DESC';
        $params['limit'] = 10000;
        //print_r($params);
        $data = $memberModel->getDataArr($params);
        if(empty($data)) die('Không có dữ liệu');
        #
        #
        #

        define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        /** Include PHPExcel */
        include_once DIR_FOLDER.'/Application/Library/Excel/PHPExcel.php';

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Hoàng Hùng")
            ->setLastModifiedBy("Hoàng Hùng")
            ->setTitle("aloads")
            ->setSubject("aloads admin")
            ->setDescription("Quản lý user")
            ->setKeywords("aloads")
            ->setCategory("aloads");


        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'STT')
            ->setCellValue('B1', 'Package Name!')
            ->setCellValue('C1', 'Phone')
            ->setCellValue('D1', 'Country')
            ->setCellValue('E1', 'OS Version')
            ->setCellValue('F1', 'Device')
            ->setCellValue('G1', 'IMEI')
            ->setCellValue('H1', 'Creat Time')
            ->setCellValue('I1', 'Update time');
        $i = 1;
        foreach ($data as $item) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . ($i +1), $i)
                ->setCellValue('B' . ($i +1), $item->package)
                ->setCellValue('C' . ($i +1), $item->phone)
                ->setCellValue('D' . ($i +1), $item->country)
                ->setCellValue('E' . ($i +1), $item->androidVersionCode)
                ->setCellValue('F' . ($i +1), $item->device)
                ->setCellValue('G' . ($i +1), $item->imei)
                ->setCellValue('H' . ($i +1), $item->createdAt)
                ->setCellValue('I' . ($i +1), $item->updatedAt);
            $i ++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('aloads');

        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.date('d-m-Y').'_member.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }
}