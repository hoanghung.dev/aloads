<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 05:13 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Permissions;
use Application\Admin\Models\UsersGroup;

class Block extends Base
{

    public function boxHeader(){
        echo $this->render();
    }
    public function sidebar(){
        echo $this->render();
    }
    function getModuleKey($id){
        $permissionModel = new Permissions();
        $data = $permissionModel->getOne('permission_id = :permission_id',array(':permission_id'=>$id));
        if(!empty($data)) return $data->module;
    }
    public function unsetMessage(){
        unset($_SESSION['_FM']);
    }

}