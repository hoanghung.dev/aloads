<?php
namespace Application\Admin\Controllers;

class Index extends Base
{
    public function index(){
        $this->redirect('/member');
        $this->displayLayout('default',$this->render());
    }
    function listAppInstall(){
        echo $this->render();
    }
    function clickInstallChart(){
        echo $this->render();
    }
    /*function revenueCostChart(){
        echo $this->render();
    }
    function publisherCampaignChart(){
        echo $this->render();
    }*/

    public function notFound(){
        $this->displayLayout('notfound',$this->render());
    }

}