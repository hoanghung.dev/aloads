<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Controllers\Base;
use Application\Admin\Models\News;

class GetUrlNews extends Base{
    public function getUrlNews($id){
        $newsModel = new News();
        $data = $newsModel->getOne('news_id = ?',array($id),'slug,title');
        $slug = $this->toSlug($data->title);
        if($slug == null){
            $slug = $this->toSlug($data->title);
            $newsModel->update(array('slug'=>$slug),'news_id = :id',array(':id'=>$id));
        }
        return _ROOT_HOME.'/bai-viet/'.$slug.'-'.$id;
    }
}