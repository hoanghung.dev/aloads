<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 05/03/2014
 * Time: 15:21
 */
namespace Application\Admin\Helpers;

use Application\Admin\Models\Users;

class GetUser
{
    public function GetUser($userId){
        $userModel = new Users();
        $oneData = $userModel->getOne('user_id = :id',array(':id'=>$userId),'full_name');
        return isset($oneData->full_name)?$oneData->full_name:'';
    }
}


