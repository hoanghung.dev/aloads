<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;

class GetImageApp{
    public function getImageApp($image = null){
        if($image != null) return _ROOT_IMAGES.'/'.$image;
        else return _ROOT_CMS.'/images/preview.png';
    }
}