<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Models\Trackings;

class GetTrackingInstall{
    public function getTrackingInstall($id){
        $trackingsModel = new Trackings();
        $params['tracking_id'] = $id;
        $data = $trackingsModel->getInstall($params);

        return isset($data->total)?$data->total:0;
    }
}