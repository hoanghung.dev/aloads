<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 05/03/2014
 * Time: 15:21
 */
namespace Application\Admin\Helpers;

use Application\Admin\Models\UsersGroup;

class GetUserGroup
{
    public function GetUserGroup($groupId = null) {
        $userGroupModel = new UsersGroup();

        $all = $userGroupModel->getDataArr();
        $html = '';
        if(!empty($all)) foreach($all as $one) {
            $selected = '';
            if($groupId != null && $one->group_id==$groupId) $selected = 'selected="selected"';
            $html .= '<option value="'.$one->group_id.'" '.$selected.'>|-- '.$one->title.'</option>';
        }
        print $html;

    }
}


