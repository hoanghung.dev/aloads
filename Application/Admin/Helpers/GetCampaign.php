<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Models\Campaigns;

class GetCampaign{
    public function getCampaign($id,$select = '*'){
        $campaignsModel = new Campaigns();
        $data = $campaignsModel->getOne('campaign_id = :id',array(':id'=> $id),$select);
        return $data;
    }
}