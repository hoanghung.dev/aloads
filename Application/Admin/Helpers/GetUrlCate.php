<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Controllers\Base;
use Application\Admin\Models\Category;

class GetUrlCate extends Base{
    public function getUrlCate($id){
        $cateModel = new Category();
        $data = $cateModel->getOne('category_id = ?',array($id),'slug');
        if($data->slug == null){
            $slug = $this->toSlug($data->title);
            $cateModel->update(array('slug'=>$slug),'category_id = :id',array(':id'=>$id));
        }
        return _ROOT_HOME.'/the-loai/'.$data->slug.'-'.$id;
    }
}