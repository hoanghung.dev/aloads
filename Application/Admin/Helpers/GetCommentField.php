<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Models\Apps;
use Application\Admin\Models\Base;

class GetCommentField{
    public function getCommentField($table = 'app_config',$field){
        $baseModel = new Base();
        $data = $baseModel->getOne($table,$field);
        return $data->COLUMN_COMMENT;
    }
}