<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 08:08 CH
 */

namespace Application\Admin\Helpers;
use Application\Admin\Models\Permissions;
use Application\Admin\Models\UsersGroup;
use Application\Admin\Controllers\Base;

class AccessPermission extends Base{
    public function accessPermission($groupId,$moduleKey){
        if($this->_session->auth->user_id == 0){
            return true;
        }else{
            $permissionModel = new Permissions();
            $groupModel = new UsersGroup();
            $data = $permissionModel->getOne('module = :module',array(':module'=>$moduleKey));
            if(!isset($data) || empty($data)) return false;
            $permissionId = $data->permission_id;
            $dataGroup = $groupModel->getOne('group_id = :id',array(':id'=>$groupId));
            if(!empty($dataGroup->role_permission)){
                $arrPerm = explode('|',$dataGroup->role_permission);
                if(in_array($permissionId,$arrPerm)){
                    return true;
                }else{
                    return false;
                }
            }else return true;
        }

    }
}