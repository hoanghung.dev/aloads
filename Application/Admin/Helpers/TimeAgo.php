<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 07/03/2014
 * Time: 22:57
 */
namespace Application\Admin\Helpers;
class TimeAgo
{
    public function timeAgo($tm,$format ='') {
        if($format != '') {
            return date($format,strtotime($tm));
        }else{
            $cur_tm = time();
            $dif = $cur_tm-strtotime($tm);
            $pds = array('giây','phút','giờ','ngày','tuần','tháng','năm','thập kỷ');
            $lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);
            $no = '';
            for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);
            $no = floor($no);
            $x = sprintf("%d %s ",$no,$pds[$v]);
            /*if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0))
                $x .= time_ago($_tm);*/
            return $x.' trước đây';
        }
    }

    public function timeAgo2($timestamp, $format=''){

        if($format != '') {
            return date($format,strtotime($timestamp));
        }else{
            if (is_string($timestamp))
            {
                $timestamp = strtotime($timestamp);
            }
            $now = time();

            //If the difference is positive "ago" - negative "away"
            ($timestamp >= $now)?$action = 'trước':$action = 'trước';

            switch($action)
            {
                case 'away':
                    $diff = $timestamp-$now;
                    break;
                case 'ago':
                default:
                    // Determine the difference, between the time now and the timestamp
                    $diff = $now-$timestamp;
                    break;
            }

            // Set the periods of time
            $periods = array ("giây", "phút", "giờ", "ngày", "tuần", "tháng", "năm", "thập kỷ");

            // Set the number of seconds per period
            $lengths = array (1, 60, 3600, 86400, 604800, 2630880, 31570560, 315705600);

            // Go from decades backwards to seconds
            for ($val = sizeof($lengths)-1; ($val >= 0) && (($number = $diff/$lengths[$val]) <= 1); $val--);

            // Ensure the script has found a match
            if ($val < 0)
                $val = 0;

            // Determine the minor value, to recurse through
            $new_time = $now-($diff%$lengths[$val]);

            // Set the current value to be floored
            $number = floor($number);

            // If required create a plural
            //if ($number != 1)
            //    $periods[$val] .= "s";

            // Return text
            $text = sprintf("%d %s ", $number, $periods[$val]);
            return $text.$action;
        }
    }
}