<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 05/03/2014
 * Time: 15:21
 */
namespace Application\Admin\Helpers;

use Application\Admin\Models\Category;

class SelectCategory
{
    public function selectCategory($categoryId=0,$parent =0) {
        $categoryModel = new Category();
        //$arrayCate = $this->stringToArray($categoryId);
        $params['parent_id'] = $parent;
        $params['select'] = 'category_id,title';
        $all = $categoryModel->getDataArr($params);
        $html = ($parent != 0)?'<option value="'.$parent.'" > Trang chủ </option>':'<option value="0" >Trang chủ</option>';
        if(!empty($all)) foreach($all as $one) {
            $selected = '';
            if(strpos($categoryId,'|'.$one->category_id.'|') !== false) $selected = 'selected="selected"';
            $html .= '<option value="'.$one->category_id.'" '.$selected.'>|-- '.$one->title.'</option>';

            $params2['parent_id'] = $one->category_id;
            $params2['select'] = 'category_id,title';
            $all2 = $categoryModel->getDataArr($params2);
            if($all2) foreach($all2 as $one2) {
                $selected = '';
                if(strpos($categoryId,'|'.$one2->category_id.'|') !== false) $selected = 'selected="selected"';
                $html .= '<option value="'.$one2->category_id.'" '.$selected.'>|------ '.$one2->title.'</option>';
            }
        }
        print $html;

    }

}


