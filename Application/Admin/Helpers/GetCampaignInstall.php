<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Models\Campaigns;

class GetCampaignInstall{
    public function getCampaignInstall($id){
        $campaignsModel = new Campaigns();
        $params['campaign_id'] = $id;
        $data = $campaignsModel->getInstall($params);
        return isset($data->total)?$data->total:0;
    }
}