<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Models\Apps;
use Application\Admin\Models\Campaigns;

class GetColumnMeta{
    public function getColumnMeta($model,$i){
        if($model === 'app') $dataModel = new Apps();
        else $dataModel = new Campaigns();
        $data = $dataModel->getColumnMeta($i);
        return $data;
    }
}