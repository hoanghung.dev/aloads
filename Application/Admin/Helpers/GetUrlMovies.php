<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Controllers\Base;
use Application\Admin\Models\Movies;

class GetUrlMovies extends Base{
    public function getUrlMovies($id){
        $moviesModel = new Movies();
        $data = $moviesModel->getOne('movies_id = ?',array($id),'slug,title');
        $slug = $data->slug;
        if($slug == null){
            $slug = $this->toSlug($data->title);
            $moviesModel->update(array('slug'=>$slug),'movies_id = :id',array(':id'=>$id));
        }
        return _ROOT_HOME.'/xem-phim/'.$slug.'-'.$id;
    }
}