<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 08:08 CH
 */

namespace Application\Admin\Helpers;
use Application\Admin\Models\Permissions;
use Application\Admin\Controllers\Base;

class GetPermission extends Base{
    public function getPermission($permissionId){
        $permissionModel = new Permissions();
        $params['parent_id'] = $permissionId;
        $data = $permissionModel->getDataArr($params);
        return $data;
    }
}