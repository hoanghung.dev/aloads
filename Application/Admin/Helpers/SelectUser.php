<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 05/03/2014
 * Time: 15:21
 */
namespace Application\Admin\Helpers;

use Application\Admin\Models\Users;

class SelectUser
{
    public function selectUser($type = null) {
        $userModel = new Users();
        $params['select'] = 'user_id,full_name';
        if($type == 'customer') $params['group_id'] = 2;
        if($type == 'partner') $params['group_id'] = 1;
        $params['limit'] = 100;
        $all = $userModel->getDataArr($params);
        $html = '';
        if(!empty($all)) foreach($all as $one) {
            $selected = '';
            $html .= '<option value="'.$one->user_id.'" '.$selected.'>'.$one->full_name.'</option>';
        }
        print $html;

    }

}


