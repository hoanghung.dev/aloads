<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;

use Application\Admin\Models\Campaigns;

class GetListCampaign{
    public function getListCampaign(){
        $campaignModel = new Campaigns();
        $params['select'] = 'campaign_id,title';
        $params['limit'] = 100;
        $data = $campaignModel->getDataArr($params);
        return $data;
    }
}