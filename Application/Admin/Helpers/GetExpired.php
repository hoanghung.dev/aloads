<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 06/03/2014
 * Time: 11:24
 */
namespace Application\Mobile\Helpers;
use Application\Mobile\Controllers\Base;
use Application\Mobile\Models\Fanpage;
class getExpired extends Base{
    public function getExpired($pageId){
        if(!empty($pageId)){
            $fanpageModel = new Fanpage();
            $data = $fanpageModel->getOne('page_id = ?',array($pageId));
            if($data['expired_time'] != '0000-00-00 00:00:00' && $data['expired_time'] > date('Y-m-d H:i:s'))
                return $this->getDayAgo($data['expired_time']);
            elseif($data['is_expired'] == false)
                return 'Chưa kích hoạt';
            else return 'Hết hạn !';
        }
    }
    function getDayAgo($tm){
        $cur_tm = time();
        $dif = strtotime($tm)-$cur_tm;
        $pds = array('giây','phút','giờ','ngày','tuần','tháng','năm','thập kỷ');
        $lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);

        for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);
        $no = floor($no);

        $x = sprintf("%d %s ",$no,$pds[$v]);
        /*if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0))
            $x .= time_ago($_tm);*/
        return 'Còn '.$x;
    }
}
