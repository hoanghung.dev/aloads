<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 03/05/2015
 * Time: 08:43 SA
 */
namespace Application\Admin\Helpers;
use Application\Admin\Models\Modules;

class GetModules{
    public function getModules(){
        $modulesModel = new Modules();
        return $modulesModel->getDataArr();
    }
}