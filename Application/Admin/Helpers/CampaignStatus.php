<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 08:08 CH
 */

namespace Application\Admin\Helpers;
class CampaignStatus{
    public function campaignStatus($i){
        $status = '';
        switch($i){
            case 0:
                $status =  '<i style="color: #d9534f;font-style: normal;font-weight: bold;">Không hoạt động</i>';
                break;
            case 1:
                $status =  '<i style="color: #285e8e;font-style: normal;font-weight: bold;">Đang chạy</i>';
                break;
            case 2:
                $status =  '<i style="color: #f0ad4e;font-style: normal;font-weight: bold;">Tạm dừng</i>';
                break;
            case 3:
                $status =  '<i style="color: #d9534f;font-style: normal;font-weight: bold;">Kết thúc</i>';
                break;
            default:
                $status =  '<i style="color: #e6e6e6;font-style: normal;font-weight: bold;">Toàn bộ</i>';
                break;
        }
        return $status;
    }
}