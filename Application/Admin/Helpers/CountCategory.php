<?php
namespace Application\Admin\Helpers;
//use Application\Main\Models\News;
use Application\Admin\Models\Category;

class CountCategory
{
    public function countCategory($params = null)
    {
        $categoryModel = new Category();
        $data = $categoryModel->getCount($params);
        return !empty($data)?$data:0;

    }
}