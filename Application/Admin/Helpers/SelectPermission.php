<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 05/03/2014
 * Time: 15:21
 */
namespace Application\Admin\Helpers;

use Application\Admin\Models\Permissions;

class SelectPermission
{
    public function selectPermission($permissionId=0,$parent = 0) {
        $permissionModel = new Permissions();
        $params['parent_id'] = $parent;
        $params['select'] = 'permission_id,title';
        $all = $permissionModel->getDataArr($params);
        $html = '<option value="0">--Chọn module cha (Nếu có)--</option>';
        if(!empty($all)) foreach($all as $one) {
            $selected = '';
            if($one->permission_id==$permissionId) $selected = 'selected="selected"';
            $html .= '<option value="'.$one->permission_id.'" '.$selected.'>|-- '.$one->title.'</option>';

            $params2['parent_id'] = $one->permission_id;
            $params2['select'] = 'permission_id,title';
            $all2 = $permissionModel->getDataArr($params2);
            if($all2) foreach($all2 as $one2) {
                $selected = '';
                if($one2->permission_id==$permissionId) $selected = 'selected="selected"';
                $html .= '<option value="'.$one2->permission_id.'" '.$selected.'>|------ '.$one2->title.'</option>';
            }
        }
        print $html;

    }
}


