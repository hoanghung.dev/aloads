<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Controllers\Base;
use Application\Admin\Models\Category;

class GetCategory extends Base{
    public function getCategory($id,$select='*'){
        $categoryModel = new Category();
        return $categoryModel->getOne('category_id = ?',array($id),$select);
    }
    private function stringToArray($string){
        $string = str_replace('|',' ',$string);
        $string = trim($string);
        $string = explode(' ',$string);
        return $string;
    }
}