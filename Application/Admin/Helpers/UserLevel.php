<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 08:08 CH
 */

namespace Application\Admin\Helpers;
class UserLevel{
    public function userLevel($i){
        switch($i){
            case 1:
                print 'Quản lý';
                break;
            case 2:
                print 'Nội dung';
                break;
            case 3:
                print 'Cộng tác viên';
                break;
            case 4:
                print 'Đối tác';
                break;
            default:
                print 'Administrator';
                break;
        }
    }
}