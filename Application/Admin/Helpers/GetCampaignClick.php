<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;

use Application\Admin\Models\Trackings;

class GetCampaignClick{
    public function getCampaignClick($id){
        //Check xem Id có list Id con hay ko ? thì Total không thì total 1 id
        $trackingsModel = new Trackings();
        $params['campaign_id'] = $id;
        $data = $trackingsModel->getClick($params);
        return isset($data->total)?$data->total:0;
    }
    function listChild($id){
        $trackingModel = new Trackings();
        $params['parent_id'] = $id;
    }
}