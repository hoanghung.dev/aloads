<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 10:54 SA
 */

namespace Application\Mobile\Helpers;
use Application\Mobile\Controllers\Base;
use Application\Mobile\Models\News;

class GetTag extends Base{
    public function getTag($tag){
        $tag = explode(',',$tag);
        foreach($tag as $item){
            $item = trim($item);
            echo '<li><a href="/xem-them/'.$this->toSlug($item).'" title="'.$item.'">'.$item.'</a></li>';
        }

    }
}