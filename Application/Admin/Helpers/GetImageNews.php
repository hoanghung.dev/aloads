<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 26/04/2015
 * Time: 10:36 CH
 */
namespace Application\Admin\Helpers;
use Application\Admin\Models\News;

class GetImageNews{
    public function getImageNews($id){
        $newsModel = new News();
        $data = $newsModel->getOne('news_id = ?',array($id),'image');
        $image = $data->image;
        return _ROOT_IMAGES.'/'.$image;
    }
}