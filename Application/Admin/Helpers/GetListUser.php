<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;

use Application\Admin\Models\Users;

class GetListUser{
    public function getListUser(){
        $userModel = new Users();
        $params['select'] = 'user_id,full_name';
        $params['limit'] = 1000;
        $data = $userModel->getDataArr($params);
        return $data;
    }
}