<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 05/03/2014
 * Time: 15:21
 */
namespace Application\Admin\Helpers;

use Application\Admin\Models\OperationSystem;

class SelectOs
{
    public function selectOs($osId=0) {
        $osModel = new OperationSystem();
        $params['select'] = 'os_id,title';
        $all = $osModel->getDataArr($params);
        $html = '';
        if(!empty($all)) foreach($all as $one) {
            $selected = '';
            if($one->os_id==$osId) $selected = 'selected="selected"';
            $html .= '<option value="'.$one->os_id.'" '.$selected.'>|-- '.$one->title.'</option>';
        }
        print $html;

    }
}


