<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 03/05/2015
 * Time: 10:31 SA
 */
namespace Application\Admin\Helpers;
use Application\Admin\Models\Category;

class GetParentCategory{
    public function GetParentCategory($category_id=0) {
        if($category_id == 0) return 'Trang chủ';
        else {
            $category = new Category();
            $params['is_trash'] = 0;
            $params['parent_id'] = $category_id;
            $params['select'] = 'category_id,title,parent_id';
            $data = $category->getDataArr($params);
            var_dump($data);
            return $data['title'];
        }
    }
}