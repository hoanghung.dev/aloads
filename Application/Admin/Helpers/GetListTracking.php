<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;

use Application\Admin\Models\Trackings;

class GetListTracking{
    public function getListTracking(){
        $trackingModel = new Trackings();
        $params['select'] = 'tracking_id';
        $params['limit'] = 100;
        $data = $trackingModel->getDataArr($params);
        return $data;
    }
}