<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Models\Category;

class GetTitleCategory{
    public function getTitleCategory($id){
        $categoryModel = new Category();
        $arrayCate = $this->stringToArray($id);
        if(is_array($arrayCate)){
            $html = '';
            foreach($arrayCate as $categoryId){
                if($categoryId){
                    $data = $categoryModel->getOne('category_id = ?',array($categoryId),'title');
                    $html .= $data->title.',';
                }else{
                    return 'Trang chủ';
                }
            }
            return $html;
        }else{
            $categoryId = str_replace('|','',$id);
            if($categoryId){
                $data = $categoryModel->getOne('category_id = ?',array($categoryId),'title');
                return $data->title;
            }else{
                return 'Trang chủ';
            }

        }

    }
    private function stringToArray($string){
        $string = str_replace('|',' ',$string);
        $string = trim($string);
        $string = explode(' ',$string);
        return $string;
    }
}