<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;

use Application\Admin\Models\Apps;

class GetListApp{
    public function getListApp(){
        $appModel = new Apps();
        $params['select'] = 'app_id,title';
        $params['limit'] = 100;
        $data = $appModel->getDataArr($params);
        return $data;
    }
}