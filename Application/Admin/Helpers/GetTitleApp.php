<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Models\Apps;

class GetTitleApp{
    public function getTitleApp($id){
        $appModel = new Apps();
        $data = $appModel->getOne('app_id = :id',array(':id'=>$id),'title');
        return $data->title;
    }
}