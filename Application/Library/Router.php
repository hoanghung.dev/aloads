<?php
namespace Application\Library;

use Soul\Application;
use Soul\Exception;
use Soul\Mvc\Request;
use Soul\Mvc\Router as SoulRouter;

class Router extends SoulRouter
{

    public function __construct(array $config, $baseUrl = '')
    {
        $altoRouter = new AltoRouter($config, $baseUrl);
        $param = $altoRouter->match();
        if ($param == false) {
            $param['controller'] = 'index';
            $param['action'] = 'notFound';
            $param['module'] = 'Admin';
            $param['target'] = $param;
        }
        $this->setParams($param);
    }

    public function route(Request $request)
    {
        $target = $this->getParam('target');
        $this->setParams($target);
        $request->setActionName($target['action']);
        $request->setControllerName($target['controller']);
        $request->setModuleName($target['module']);
        $request->setParams($this->getParam('params'));
        $request->setParam('router', $target);     
    }

    public function httpNotFound()
    {
        $content = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'Http/404.html');
        $response = Application::getInstance()->getResponse();
        $response->setStatus(404)->body($content)->send();
        exit;
    }
}