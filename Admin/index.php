<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', -1);
$rootDir = dirname(dirname(__FILE__));
set_include_path(get_include_path() . PATH_SEPARATOR . $rootDir);
date_default_timezone_set("Asia/Ho_Chi_Minh");
setlocale(LC_MONETARY, 'vi_VN');
//echo $rootDir;
include $rootDir.'/config.php';
include $rootDir.'/Admin/router.php';
include_once $rootDir.'/Soul/Loader.php';
$loader = new \Soul\Loader();

$loader->registerNamespaces(array(
    'Soul\Mysql' => $rootDir . '\Mysql',
    'Soul\Helpers' => $rootDir ,
    'Soul\Pattern' => $rootDir . '\Pattern',
    'Application\Admin\Helpers' => $rootDir,
    'Application\Admin\Models' => $rootDir,
    'Application\Admin\Controllers' => $rootDir,
    'Application\Library' => $rootDir,
    'Soul' => $rootDir . '',
));
$loader->register();
use Soul\Application;

\Soul\Session::getInstance();


#MySQL
$mysql['host'] = DB_SERVER;
$mysql['dbname'] = DB_DATABASE;
$mysql['username'] = DB_USERNAME;
$mysql['password'] = DB_PASSWORD;
$mysql['options'] = null;

/*$mysql_second['host'] = DB_SERVER_SECOND;
$mysql_second['dbname'] = DB_DATABASE_SECOND;
$mysql_second['username'] = DB_USERNAME_SECOND;
$mysql_second['password'] = DB_PASSWORD_SECOND;
$mysql_second['options'] = null;*/

$mysqlConnection = \Soul\Mysql::factory('main', $mysql);
\Soul\Registry::set('Mysql', $mysqlConnection);

/*$mysqlConnection = \Soul\Mysql::factory('member', $mysql_second);
\Soul\Registry::set('MysqlUser', $mysqlConnection);*/


$router = new \Application\Library\Router($routerConfig);


$modules = array('Admin');

Application::getInstance()->setRouter($router)
    ->setApplicationDir($rootDir . DIRECTORY_SEPARATOR . 'Application')
    ->setModules($modules);

try {
    Application::run();
} catch (\Soul\Exception $e) {
    $e->getMessage();
}

