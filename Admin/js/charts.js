jQuery(document).ready(function() {
    "use strict";

    /***** BAR CHART *****/

    var bardata = [ ["Jan", 10], ["Feb", 23], ["Mar", 18], ["Apr", 13], ["May", 17], ["Jun", 30], ["Jul", 26], ["Aug", 16], ["Sep", 17], ["Oct", 5], ["Nov", 8], ["Dec", 15] ];

	 jQuery.plot("#barchart", [ bardata ], {
		  series: {
            lines: {
              lineWidth: 1
            },
				bars: {
					show: true,
					barWidth: 0.5,
					align: "center",
               lineWidth: 0,
               fillColor: "#428BCA"
				}
		  },
        grid: {
            borderColor: '#ddd',
            borderWidth: 1,
            labelMargin: 10
		  },
		  xaxis: {
				mode: "categories",
				tickLength: 0
		  }
	 });


    /***** PIE CHART *****/

    jQuery.plot('#piechart', piedata, {
        series: {
            pie: {
                show: true,
                radius: 1,
                label: {
                    show: true,
                    radius: 2/3,
                    formatter: labelFormatter,
                    threshold: 0.1
                }
            }
        },
        grid: {
            hoverable: true,
            clickable: true
        }
    });

    function labelFormatter(label, series) {
		return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
	}


   /***** MORRIS CHARTS *****/
   var m1 = new Morris.Line({
       // ID of the element in which to draw the chart.
       element: 'line-chart',
       // Chart data records -- each entry in this array corresponds to a point on
       // the chart.
       data: [
           { y: '2006', a: 30, b: 20 },
           { y: '2007', a: 75,  b: 65 },
           { y: '2008', a: 50,  b: 40 },
           { y: '2009', a: 75,  b: 65 },
           { y: '2010', a: 50,  b: 40 },
           { y: '2011', a: 75,  b: 65 },
           { y: '2012', a: 100, b: 90 }
       ],
       xkey: 'y',
       ykeys: ['a', 'b'],
       labels: ['Series A', 'Series B'],
       lineColors: ['#D9534F', '#428BCA'],
       lineWidth: '2px',
       hideHover: 'auto',
       resize: true
   });
    var delay = (function() {
	var timer = 0;
	return function(callback, ms) {
	    clearTimeout(timer);
	    timer = setTimeout(callback, ms);
	};
    })();

    jQuery(window).resize(function() {
	delay(function() {
	    m1.redraw();
	    //m2.redraw();
	    //m3.redraw();
	    //m4.redraw();
	    //m5.redraw();
	    //m6.redraw();
	}, 200);
    }).trigger('resize');
  
});
