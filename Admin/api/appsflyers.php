<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', -1);
session_start();
$rootDir = dirname(dirname(dirname(__FILE__)));
set_include_path(get_include_path() . PATH_SEPARATOR . $rootDir);
#Time_Zone
date_default_timezone_set("Asia/Ho_Chi_Minh");
setlocale(LC_MONETARY, 'vi_VN');
//echo $rootDir;
include $rootDir.'/config.php';
include 'functions.php';
include_once $rootDir.'/Soul/Loader.php';
$loader = new \Soul\Loader();

$loader->registerNamespaces(array(

    'Soul\Mysql' => $rootDir . '\Mysql',
    'Soul\Helpers' => $rootDir ,
    'Soul\Pattern' => $rootDir . '\Pattern',
    'Application\Admin\Helpers' => $rootDir,
    'Application\Admin\Models' => $rootDir,
    'Application\Admin\Controllers' => $rootDir,
    'Application\Library' => $rootDir,
    'Soul' => $rootDir . '',
));
$loader->register();

\Soul\Session::getInstance();

$mysql['host'] = DB_SERVER;
$mysql['dbname'] = DB_DATABASE;
$mysql['username'] = DB_USERNAME;
$mysql['password'] = DB_PASSWORD;
$mysql['options'] = null;


$mysqlConnection = \Soul\Mysql::factory('main', $mysql);
\Soul\Registry::set('Mysql', $mysqlConnection);
define("ENABLE_MEMCACHED", false);

$model = new \Application\Admin\Models\Trackings();

$startDate = date('Y-m-d');
$endDate = date('Y-m-d');
$url = 'https://hq.appsflyer.com/export/id579523206/partners_by_date_report?api_token=ca06df97-caaa-41b5-b039-aeb10c58bbef&from='.$startDate.'&to='.$endDate;
$data = csvToJson($url);
echo "<pre>";print_r($data);




