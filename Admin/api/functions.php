<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 30/12/2015
 * Time: 02:33 CH
 */
function csvToJson($url){
    $keys = array();
    $newArray = array();

    $data = csvToArray($url, ',');

// Set number of elements (minus 1 because we shift off the first row)
    $count = count($data) - 1;

//Use first row for names
    $labels = array_shift($data);

    foreach ($labels as $label) {
        $keys[] = $label;
    }

// Add Ids, just in case we want them later
    $keys[] = 'id';

    for ($i = 0; $i < $count; $i++) {
        $data[$i][] = $i;
    }

// Bring it all together
    for ($j = 0; $j < $count; $j++) {
        $d = array_combine($keys, $data[$j]);
        $newArray[$j] = $d;
    }
    return json_decode(json_encode($newArray), FALSE);
}
function csvToArray($file, $delimiter) {
    $arr = array();
    if (($handle = fopen($file, 'r')) !== FALSE) {
        $i = 0;
        while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) {
            for ($j = 0; $j < count($lineArray); $j++) {
                $arr[$i][$j] = $lineArray[$j];
            }
            $i++;
        }
        fclose($handle);
    }
    return $arr;
}