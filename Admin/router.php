<?php
$routerConfig[] = array('GET', '/', array('controller' => 'index', 'action' => 'index', 'module' => 'admin'), 'Home');
$routerConfig[] = array('GET', '/404.html', array('controller' => 'index', 'action' => 'notFound', 'module' => 'admin'), 'index notFound');
$routerConfig[] = array('GET', '/test', array('controller' => 'test', 'action' => 'index', 'module' => 'admin'), 'test');
$routerConfig[] = array('GET', '/search/[*:q]', array('controller' => 'search', 'action' => 'index', 'module' => 'admin'), 'Home search');
$routerConfig[] = array('GET', '/search-phim/[*:q]', array('controller' => 'search', 'action' => 'searchMovies', 'module' => 'admin'), 'Home search phim');
$routerConfig[] = array('GET', '/get-data-movies', array('controller' => 'search', 'action' => 'getDataMovies', 'module' => 'admin'), 'get getDataMovies');

$routerConfig[] = array('GET', '/import', array('controller' => 'importData', 'action' => 'index', 'module' => 'admin'), 'importData');

$routerConfig[] = array('GET|POST', '/slider', array('controller' => 'setting', 'action' => 'slider', 'module' => 'admin'), 'setting slider');
$routerConfig[] = array('GET|POST', '/box-home', array('controller' => 'setting', 'action' => 'boxHome', 'module' => 'admin'), 'setting boxHome');
$routerConfig[] = array('GET|POST', '/setting/actInsertSlider', array('controller' => 'setting', 'action' => 'actInsertSlider', 'module' => 'admin'), 'actInsertSlider');
$routerConfig[] = array('GET|POST', '/setting/actUpdateSlider', array('controller' => 'setting', 'action' => 'actUpdateSlider', 'module' => 'admin'), 'actUpdateSlider');
$routerConfig[] = array('GET|POST', '/page', array('controller' => 'setting', 'action' => 'page', 'module' => 'admin'), 'setting page');
$routerConfig[] = array('GET|POST', '/page/add', array('controller' => 'setting', 'action' => 'add', 'module' => 'admin'), 'setting page add');
$routerConfig[] = array('GET|POST', '/page/edit', array('controller' => 'setting', 'action' => 'edit', 'module' => 'admin'), 'setting page edit');
$routerConfig[] = array('GET|POST', '/setting/actInsert', array('controller' => 'setting', 'action' => 'actInsert', 'module' => 'admin'), 'setting actInsert');
$routerConfig[] = array('GET|POST', '/setting/actUpdate', array('controller' => 'setting', 'action' => 'actUpdate', 'module' => 'admin'), 'setting actUpdate');
$routerConfig[] = array('GET|POST', '/setting/actDelete', array('controller' => 'setting', 'action' => 'actDelete', 'module' => 'admin'), 'setting actDelete');




$routerConfig[] = array('GET|POST', '/login', array('controller' => 'user', 'action' => 'login', 'module' => 'admin'), 'Login');
$routerConfig[] = array('GET', '/logout', array('controller' => 'user', 'action' => 'logout', 'module' => 'admin'), 'Logout');
$routerConfig[] = array('GET', '/unset-message', array('controller' => 'block', 'action' => 'unsetMessage', 'module' => 'admin'), 'unset Message');


$routerConfig[] = array('GET', '/permission', array('controller' => 'permission', 'action' => 'index', 'module' => 'admin'), 'permission dashboard');
$routerConfig[] = array('GET|POST', '/permission/add', array('controller' => 'permission', 'action' => 'add', 'module' => 'admin'), 'Permission add');
$routerConfig[] = array('GET|POST', '/permission/edit/[i:id]', array('controller' => 'permission', 'action' => 'edit', 'module' => 'admin'), 'permission edit');
$routerConfig[] = array('GET|POST', '/permission/actUpdate', array('controller' => 'permission', 'action' => 'actUpdate', 'module' => 'admin'), 'Permission actUpdate');
$routerConfig[] = array('GET', '/permission-detail', array('controller' => 'permission', 'action' => 'permissionDetail', 'module' => 'admin'), 'permission group dashboard');

$routerConfig[] = array('GET', '/permission-roles', array('controller' => 'permission', 'action' => 'permissionRoles', 'module' => 'admin'), 'permission Roles');
$routerConfig[] = array('GET|POST', '/permission-add-roles', array('controller' => 'permission', 'action' => 'addRoles', 'module' => 'admin'), 'addRoles');
$routerConfig[] = array('GET|POST', '/permission-edit-roles/[i:id]', array('controller' => 'permission', 'action' => 'editRoles', 'module' => 'admin'), 'editRoles');
$routerConfig[] = array('GET|POST', '/permission/actTrashRoles', array('controller' => 'permission', 'action' => 'actTrashRoles', 'module' => 'admin'), 'actTrashRoles');
$routerConfig[] = array('GET|POST', '/permission/actUnTrashRoles', array('controller' => 'permission', 'action' => 'actUnTrash', 'module' => 'admin'), 'actUnTrashRoles');
$routerConfig[] = array('GET|POST', '/permission/actDeleteRoles', array('controller' => 'permission', 'action' => 'actDeleteRoles', 'module' => 'admin'), 'actDeleteRoles');


$routerConfig[] = array('GET', '/user', array('controller' => 'user', 'action' => 'index', 'module' => 'admin'), 'User dashboard');
$routerConfig[] = array('GET', '/user-customer', array('controller' => 'user', 'action' => 'customer', 'module' => 'admin'), 'User customer dashboard');
$routerConfig[] = array('GET', '/user-partner', array('controller' => 'user', 'action' => 'partner', 'module' => 'admin'), 'User partner dashboard');
$routerConfig[] = array('GET', '/user-add-maps', array('controller' => 'user', 'action' => 'addressMaps', 'module' => 'admin'), 'User addres maps dashboard');
$routerConfig[] = array('GET', '/user-referral', array('controller' => 'user', 'action' => 'referral', 'module' => 'admin'), 'User referral dashboard');
$routerConfig[] = array('GET', '/user/profile', array('controller' => 'user', 'action' => 'profile', 'module' => 'admin'), 'profile');
$routerConfig[] = array('GET', '/user-setting', array('controller' => 'user', 'action' => 'setting', 'module' => 'admin'), 'setting');
$routerConfig[] = array('GET|POST', '/user-add', array('controller' => 'user', 'action' => 'add', 'module' => 'admin'), 'addUser');
$routerConfig[] = array('GET|POST', '/user-edit/[i:id]', array('controller' => 'user', 'action' => 'edit', 'module' => 'admin'), 'editUser');
$routerConfig[] = array('GET|POST', '/user/actDelete', array('controller' => 'user', 'action' => 'actDelete', 'module' => 'admin'), 'actDelete');
$routerConfig[] = array('GET', '/user/permissionJson', array('controller' => 'user', 'action' => 'permissionJson', 'module' => 'admin'), 'permissionJson');


$routerConfig[] = array('GET', '/member', array('controller' => 'member', 'action' => 'index', 'module' => 'admin'), 'member dashboard');
$routerConfig[] = array('GET|POST', '/member-add', array('controller' => 'member', 'action' => 'add', 'module' => 'admin'), 'addmember');
$routerConfig[] = array('GET|POST', '/member-edit/[i:id]', array('controller' => 'member', 'action' => 'edit', 'module' => 'admin'), 'editmember');
$routerConfig[] = array('GET|POST', '/member/actDelete', array('controller' => 'member', 'action' => 'actDelete', 'module' => 'admin'), 'actDeletemember');
$routerConfig[] = array('GET', '/member-export', array('controller' => 'member', 'action' => 'exportExcel', 'module' => 'admin'), 'exportExcel');


$routerConfig[] = array('GET', '/mail-status-app', array('controller' => 'app', 'action' => 'mailstatusapp', 'module' => 'admin'), 'app mail');
$routerConfig[] = array('GET|POST', '/app', array('controller' => 'app', 'action' => 'index', 'module' => 'admin'), 'app dashboard');
$routerConfig[] = array('GET|POST', '/app-api', array('controller' => 'app', 'action' => 'api', 'module' => 'admin'), 'app api');
$routerConfig[] = array('GET', '/list-app/[*:keywords]', array('controller' => 'app', 'action' => 'listAppAjax', 'module' => 'admin'), 'list app ajax dashboard');
$routerConfig[] = array('GET|POST', '/app-add', array('controller' => 'app', 'action' => 'add', 'module' => 'admin'), 'app add');
$routerConfig[] = array('GET|POST', '/app-edit/[i:id]', array('controller' => 'app', 'action' => 'edit', 'module' => 'admin'), 'app edit');
$routerConfig[] = array('GET|POST', '/app/actTrash', array('controller' => 'app', 'action' => 'actTrash', 'module' => 'admin'), 'app actTrash');
$routerConfig[] = array('GET|POST', '/app/actUnTrash', array('controller' => 'app', 'action' => 'actUnTrash', 'module' => 'admin'), 'app actUnTrash');
$routerConfig[] = array('GET|POST', '/app/actDelete', array('controller' => 'app', 'action' => 'actDelete', 'module' => 'admin'), 'app actDelete');



$routerConfig[] = array('GET|POST', '/campaign', array('controller' => 'campaign', 'action' => 'index', 'module' => 'admin'), 'campaign dashboard');
$routerConfig[] = array('GET|POST', '/campaign-add', array('controller' => 'campaign', 'action' => 'add', 'module' => 'admin'), 'campaign add');
$routerConfig[] = array('GET|POST', '/campaign-edit/[i:id]', array('controller' => 'campaign', 'action' => 'edit', 'module' => 'admin'), 'campaign edit');
$routerConfig[] = array('GET|POST', '/campaign-price-add/[i:id]', array('controller' => 'campaign', 'action' => 'addPrice', 'module' => 'admin'), 'campaign addPrice');
$routerConfig[] = array('GET|POST', '/campaign/actDelete', array('controller' => 'campaign', 'action' => 'actDelete', 'module' => 'admin'), 'campaign actDelete');
$routerConfig[] = array('GET|POST', '/campaign/actPlay', array('controller' => 'campaign', 'action' => 'actPlay', 'module' => 'admin'), 'campaign actPlay');
$routerConfig[] = array('GET|POST', '/campaign/actPause', array('controller' => 'campaign', 'action' => 'actPause', 'module' => 'admin'), 'campaign actPause');
$routerConfig[] = array('GET|POST', '/campaign/actPriceDelete', array('controller' => 'campaign', 'action' => 'actPriceDelete', 'module' => 'admin'), 'campaign actPriceDelete');

$routerConfig[] = array('GET', '/tracking', array('controller' => 'tracking', 'action' => 'index', 'module' => 'admin'), 'tracking dashboard');
$routerConfig[] = array('GET', '/apps-[i:id]/[*:hash]', array('controller' => 'tracking', 'action' => 'trackLink', 'module' => 'admin'), 'trackLink dashboard');
$routerConfig[] = array('GET|POST', '/tracking-add', array('controller' => 'tracking', 'action' => 'add', 'module' => 'admin'), 'tracking add');
$routerConfig[] = array('GET|POST', '/tracking-edit/[i:id]', array('controller' => 'tracking', 'action' => 'edit', 'module' => 'admin'), 'tracking edit');
$routerConfig[] = array('GET|POST', '/tracking/actDelete', array('controller' => 'tracking', 'action' => 'actDelete', 'module' => 'admin'), 'tracking actDelete');



$routerConfig[] = array('GET', '/customer-care', array('controller' => 'customerCare', 'action' => 'index', 'module' => 'admin'), 'customer care');
$routerConfig[] = array('GET', '/customer-care/subscriber-detail', array('controller' => 'customerCare', 'action' => 'subscriberDetail', 'module' => 'admin'), 'customer care subscriber-detail');
$routerConfig[] = array('GET', '/customer-care/subscriber-history', array('controller' => 'customerCare', 'action' => 'subscriberHistory', 'module' => 'admin'), 'customer care subscriber-history');
$routerConfig[] = array('GET', '/customer-care/momt-history', array('controller' => 'customerCare', 'action' => 'MOMTHistory', 'module' => 'admin'), 'customer care momt-history');
$routerConfig[] = array('GET', '/customer-care/send-movies', array('controller' => 'customerCare', 'action' => 'sendMovies', 'module' => 'admin'), 'customer care send-movies');
$routerConfig[] = array('GET', '/customer-care/send-link-movies-hot', array('controller' => 'customerCare', 'action' => 'sendLinkHot', 'module' => 'admin'), 'customer care send-link-movies-hot');
$routerConfig[] = array('GET', '/customer-care/register-movies-hot', array('controller' => 'customerCare', 'action' => 'registerMoviesHot', 'module' => 'admin'), 'customer care register-movies-hot');




$routerConfig[] = array('GET', '/report/score', array('controller' => 'reports', 'action' => 'statisticsScore', 'module' => 'admin'), 'report Score');
$routerConfig[] = array('GET', '/report/movies', array('controller' => 'reports', 'action' => 'statisticsMovies', 'module' => 'admin'), 'report movies');
$routerConfig[] = array('GET', '/report/revenue', array('controller' => 'reports', 'action' => 'statisticsRevenue', 'module' => 'admin'), 'report revenue');
$routerConfig[] = array('GET', '/report/category-movies', array('controller' => 'reports', 'action' => 'statisticsCategoryMovies', 'module' => 'admin'), 'report category-movies');
$routerConfig[] = array('GET', '/report/change-package', array('controller' => 'reports', 'action' => 'statisticsChangePackage', 'module' => 'admin'), 'report change-package');
$routerConfig[] = array('GET', '/report/cancel', array('controller' => 'reports', 'action' => 'statisticsCancel', 'module' => 'admin'), 'report cancel');
$routerConfig[] = array('GET', '/report/viewed-not-registered', array('controller' => 'reports', 'action' => 'statisticsViewedNotRegistered', 'module' => 'admin'), 'report viewed-not-registered');
$routerConfig[] = array('GET', '/report/downloaded-movies', array('controller' => 'reports', 'action' => 'statisticsDownloadedMovies', 'module' => 'admin'), 'report downloaded-movies');
$routerConfig[] = array('GET', '/report/registered-package', array('controller' => 'reports', 'action' => 'statisticsRegisteredPackage', 'module' => 'admin'), 'report statisticsRegisteredPackage');
$routerConfig[] = array('GET', '/report/cp', array('controller' => 'reports', 'action' => 'statisticsCP', 'module' => 'admin'), 'report cp');
$routerConfig[] = array('GET', '/report/revenue-total', array('controller' => 'reports', 'action' => 'statisticsRevenueTotal', 'module' => 'admin'), 'report revenue total');


$routerConfig[] = array('GET', '/message', array('controller' => 'message', 'action' => 'index', 'module' => 'admin'), 'message dashboard');
$routerConfig[] = array('GET', '/message_approval', array('controller' => 'message', 'action' => 'approval', 'module' => 'admin'), 'message approval dashboard');
$routerConfig[] = array('GET|POST', '/message/add', array('controller' => 'message', 'action' => 'add', 'module' => 'admin'), 'message add');
$routerConfig[] = array('GET|POST', '/message/edit/[i:id]', array('controller' => 'message', 'action' => 'edit', 'module' => 'admin'), 'message edit');
$routerConfig[] = array('GET|POST', '/message/actTrash', array('controller' => 'message', 'action' => 'actTrash', 'module' => 'admin'), 'message actTrash');
$routerConfig[] = array('GET|POST', '/message/actApproval', array('controller' => 'message', 'action' => 'actApproval', 'module' => 'admin'), 'message actApproval');
$routerConfig[] = array('GET|POST', '/message/actUnTrash', array('controller' => 'message', 'action' => 'actUnTrash', 'module' => 'admin'), 'message actUnTrash');
$routerConfig[] = array('GET|POST', '/message/actDelete', array('controller' => 'message', 'action' => 'actDelete', 'module' => 'admin'), 'message actDelete');


$routerConfig[] = array('GET', '/imei', array('controller' => 'imei', 'action' => 'index', 'module' => 'admin'), 'imei dashboard');
$routerConfig[] = array('GET|POST', '/imei-add', array('controller' => 'imei', 'action' => 'add', 'module' => 'admin'), 'addimei');
$routerConfig[] = array('GET|POST', '/imei-edit/[i:id]', array('controller' => 'imei', 'action' => 'edit', 'module' => 'admin'), 'editimei');
$routerConfig[] = array('GET|POST', '/imei/actDelete', array('controller' => 'imei', 'action' => 'actDelete', 'module' => 'admin'), 'actDeleteimei');

